import React, {Component} from 'react';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import reducers from './reducers';
import ReduxThunk from 'redux-thunk';
import SplashScreen from 'react-native-splash-screen';
import AsyncStorage from '@react-native-community/async-storage';
import {changeLng} from './Config';
import Router from './Router';
import {Actions} from 'react-native-router-flux';
import firebase from 'react-native-firebase';
import {Root, Header} from 'native-base';
import {Alert} from 'react-native';
// second version
import OneSignal from 'react-native-onesignal'; // Import package from node modules

class App extends Component {
    constructor(properties) {
        super(properties);
        //Remove this method to stop OneSignal Debugging
        //OneSignal.setLogLevel(6, 0);

        // Replace 'YOUR_ONESIGNAL_APP_ID' with your OneSignal App ID.
        console.log("App start");
        OneSignal.init("f52ea7fe-a24e-4945-97e3-eadf5f56aaca", {kOSSettingsKeyAutoPrompt : false, kOSSettingsKeyInAppLaunchURL: false, kOSSettingsKeyInFocusDisplayOption:2});
        OneSignal.inFocusDisplaying(2); // Controls what should happen if a notification is received while the app is open. 2 means that the notification will go directly to the device's notification center.

        // The promptForPushNotifications function code will show the iOS push notification prompt. We recommend removing the following code and instead using an In-App Message to prompt for notification permission (See step below)
        OneSignal.promptForPushNotificationsWithUserResponse(myiOSPromptCallback);

        OneSignal.addEventListener('received', this.onReceived);
        OneSignal.addEventListener('opened', this.onOpened);
        OneSignal.addEventListener('ids', this.onIds);
    }
  state = {lang: false};

  componentWillMount = async () => {
    try {
      let lang = await AsyncStorage.getItem('language');
      let user = await AsyncStorage.getItem('user');
      if (lang) {
        // alert(1)
        changeLng(lang);

        if (user) {
          // console.log(user);

          this.setState({lang: true});
          Actions.reset('lunch');
          // alert('user succeess')
        } else {
          // console.log('else');
          this.setState({lang: true});
        }
      } else {
        // console.log('language');
        changeLng('ar');
        this.setState({lang: true});
        Actions.reset('language');
      }

      // I18n.locale = 'ar';
    } catch (error) {
      // console.log(error);
    }
    /*const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
    } else {
      await firebase
        .messaging()
        .requestPermission()
        .then(accept => {})
        .catch(error => {});
    }

    this.notificationDisplayedListener = firebase
      .notifications()
      .onNotificationDisplayed((notification: Notification) => {});

    this.notificationListener = firebase
      .notifications()
      .onNotification((notification: Notification) => {
        let notificationData = notification.data; //.notification,
        var click;
        console.log("----------- notification -----------------");
        console.log(notificationData);
        console.log("----------------------------");
        if (notificationData.type == 2) {
          click = () =>
            Actions.push('orderPage', {orderID: notificationData.id, title: '#' + notificationData.id});
        } else if (notificationData.type == 3) {
          click = () =>
            Actions.push('orderPage', {orderID: notificationData.id, title: '#' + notificationData.id});
        } else if (notificationData.type == 4) {
          click = () =>
            Actions.push('orderPage', {orderID: notificationData.id, title: '#' + notificationData.id});
        } else if (notificationData.type == 5) {
          click = () =>
            Actions.push('orderPage', {orderID: notificationData.id, title: '#' + notificationData.id});
        } else if (notificationData.type == 6) {
          click = () =>
            Actions.push('orderPage', {orderID: notificationData.id, title: '#' + notificationData.id});
        } else if (notificationData.type == 7) {
          click = () =>
            Actions.push('productsPage', {title: notificationData.title, brandId: notificationData.id})
        } else if (notificationData.type == 8) {
          click = () =>
            Actions.push('videosPage');
        }
        // console.log(notificationData);
        Alert.alert(
          notificationData.title,
          notificationData.body,
          [
            // {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
            {
              text: 'cancel',
              // onPress: () => console.log('Cancel Pressed'),
              style: 'cancel',
            },
            {text: 'ok', onPress: click},
          ],
          {cancelable: false},
        );
      });
    //App in Foreground and background
    this.notificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened((notificationOpen: NotificationOpen) => {
        // Get the action triggered by the notification being opened
        const action = notificationOpen.action;
        // Get information about the notification that was opened
        const notification: Notification = notificationOpen.notification.data;
        if (notification.type == 2) {
          Actions.push('orderPage', {orderID: notification.id, title: '#' + notification.id});
        } else if (notification.type == 3) {
          Actions.push('orderPage', {orderID: notification.id, title: '#' + notification.id});
        } else if (notification.type == 4) {
          Actions.push('orderPage', {orderID: notification.id, title: '#' + notification.id});
        } else if (notification.type == 5) {
          Actions.push('orderPage', {orderID: notification.id, title: '#' + notification.id});
        } else if (notification.type == 6) {
          Actions.push('orderPage', {orderID: notification.id, title: '#' + notification.id});
        } else if (notification.type == 7) {
            Actions.push('productsPage', {title: notification.title, brandId: notification.id})
        } else if (notification.type == 8) {
            Actions.push('videosPage');
        }
      });
    //App closed
    const notificationOpen: NotificationOpen = await firebase
      .notifications()
      .getInitialNotification();
    if (notificationOpen) {
        // App was opened by a notification
        // Get the action triggered by the notification being opened
        const action = notificationOpen.action;
        // Get information about the notification that was opened
        const notification: Notification = notificationOpen.notification.data;

        if (notification.type == 2) {
            Actions.push('orderPage', {orderID: notification.id, title: '#' + notification.id});
        } else if (notification.type == 3) {
            Actions.push('orderPage', {orderID: notification.id, title: '#' + notification.id});
        } else if (notification.type == 4) {
            Actions.push('orderPage', {orderID: notification.id, title: '#' + notification.id});
        } else if (notification.type == 5) {
            Actions.push('orderPage', {orderID: notification.id, title: '#' + notification.id});
        } else if (notification.type == 6) {
            Actions.push('orderPage', {orderID: notification.id, title: '#' + notification.id});
        } else if (notification.type == 7) {
            Actions.push('productsPage', {title: notification.title, brandId: notification.id})
        } else if (notification.type == 8) {
            Actions.push('videosPage');
        }
    }*/

    SplashScreen.hide();
  };

  componentWillUnmount() {
      OneSignal.removeEventListener('received', this.onReceived);
      OneSignal.removeEventListener('opened', this.onOpened);
      OneSignal.removeEventListener('ids', this.onIds);
        //this.notificationDisplayedListener();
    //this.notificationListener();
  }
    onIds(device) {
        console.log('Device info: ', device);
        AsyncStorage.setItem('oneSignalProviderId', device.userId + '');
    }
    onReceived(notification) {
        console.log("Notification received: ", notification);
    }

    onOpened(openResult) {
        const notification = openResult.notification.payload.additionalData;
        if (notification.type == 2) {
            Actions.push('orderPage', {orderID: notification.id, title: '#' + notification.id});
        } else if (notification.type == 3) {
            Actions.push('orderPage', {orderID: notification.id, title: '#' + notification.id});
        } else if (notification.type == 4) {
            Actions.push('orderPage', {orderID: notification.id, title: '#' + notification.id});
        } else if (notification.type == 5) {
            Actions.push('orderPage', {orderID: notification.id, title: '#' + notification.id});
        } else if (notification.type == 6) {
            Actions.push('orderPage', {orderID: notification.id, title: '#' + notification.id});
        } else if (notification.type == 7) {
            Actions.push('productsPage', {title: notification.title, brandId: notification.id})
        } else if (notification.type == 8) {
            Actions.push('videosPage');
        }
        console.log('Message: ', openResult.notification.payload.body);
        console.log('Data: ', openResult.notification.payload.additionalData);
        console.log('isActive: ', openResult.notification.isAppInFocus);
        console.log('openResult: ', openResult);
    }


  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
    return (
      <Root>
        <Header style={{display: 'none'}} androidStatusBarColor="#000" />
        <Provider store={store}>{this.state.lang ? <Router /> : null}</Provider>
      </Root>
    );
  }
}
export default App;
function myiOSPromptCallback(permission){
    // do something with permission value
}