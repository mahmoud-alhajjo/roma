// checkLang()
// import i18n from './i18n';
// // alert(i18n.t('lang'))
// export const baseUrl = 'https://aljawad.sa.com/app/' + i18n.t('lang') + '/api/'
export const base = 'https://sh.alyomhost.net/adasatik/upload/'
import RNRestart from 'react-native-restart';
// export const headers = {
//   'Accept': 'application/json',
//   // 'content-type': 'multipart/form-data',
//   'client': 'e45ef7c646375b4a14bc976e5416715e4f1ae554',
//   'secret': '2c70d4d132a42456e7c9fdd307d139abf82ff2ac',
// }
// import { NetInfo, AsyncStorage, Alert } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import I18n, { getLanguages } from 'react-native-i18n';
import { I18nManager } from 'react-native'

// Enable fallbacks if you want `en-US`
// and `en-GB` to fallback to `en`
I18n.fallbacks = true;

I18n.translations = {
    'en': require('./translations/en.json'),
    'ar': require('./translations/ar.json'),
};
let languageSession = AsyncStorage.getItem('language')
export let language
export let currencyRate;
// console.log(typeof languageSession, languageSession);

if (languageSession != null && 'string' == typeof languageSession) {
    // alert(1)
    language = 'en'
    I18nManager.forceRTL(false);
    I18n.local = 'en'
} else {
    // alert(2)
    language = 'ar'
    I18nManager.forceRTL(true);
    I18n.local = 'ar'
}



// var words=require('langs/ar.js')

// export let base = 'http://t8.someotherhost.com/inventory/public/'
// export let base = 'http://afnan.t2.someotherhost.com/'

// export let baseUrl = 'https://romaopticals.in/' + I18n.local + '/mobile/'
export let baseUrl = 'https://romaopticals.com/' + I18n.local + '/mobile_dev/'
// export let baseUrl = base + 'api/v1/'
export const baseUploadUrl =  'https://romaopticals.in/upload/'
// export const commonheaders = {
//     'Accept': 'application/json',
// }
// export const headers={...commonheaders,...{"Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"}}
export const headers = {
    'Accept': 'application/json',
    // 'content-type': 'multipart/form-data',
    'client': 'fb28d9c9fb43490c1d28384f6fcbf9dde6ceeab8',
    'secret': 'eaeba67bcdffcbca250a5d0c1b3c7ee6e63c8855',
}
export function changeLng(lang, flag) {
    AsyncStorage.setItem('language', lang)
    I18n.local = lang
    // baseUrl = 'https://romaopticals.in/' + lang + '/mobile/'
    baseUrl = 'https://romaopticals.com/' + lang + '/mobile_dev/'
    if (lang == 'ar') {
        I18nManager.forceRTL(true);
    } else if (lang == 'en') {
        I18nManager.forceRTL(false);
    }

    I18n.local = lang
    if (flag) {
        RNRestart.Restart()
    }

}

export function L(key) {
    let word = I18n.t(key, { locale: I18n.local, default : key });
    // let word = I18n.t(key, { locale: 'ar' })
    return word
}