import React from 'react';
import {
  Scene,
  Router,
  Actions,
  Stack,
  Tabs,
  Lightbox,
} from 'react-native-router-flux';
import { Button, Icon, Text } from 'native-base';
import {
  /*TouchableNativeFeedback, */
  BackHandler
} from 'react-native';
import { AsyncStorage } from '@react-native-community/async-storage';
import styles, {
  verticalScale,
  scale,
  buttonStyle,
  buttonTextStyle,
  textPageContent,
  errorTextStyle,
  moderateScale,
  headerStyle,
  primaryColor,
  secondaryColor,
  tabText,
  tabactive,
  tabBarLabel,
  tabBarStyle,
} from './components/assets/styles/Style';

import StackViewStyleInterpolator from 'react-navigation-stack/dist/views/StackView/StackViewStyleInterpolator';

import LoginForm from './components/LoginForm';
import Language from './components/Language';
import Lunch from './components/Lunch';
import Register from './components/Register';
import ForgetPassword from './components/ForgetPassword';

import Intro from './components/Intro';
import Home from './components/Home';
import Profile from './components/Profile';
import Cart from './components/Cart';
import Wishlist from './components/Wishlist';
import CatsPage from './components/CatsPage';
import ProductsPage from './components/ProductsPage';
import Product from './components/Product';
import About from './components/About';
import Contact from './components/Contact';
import Notificaions from './components/Notificaions';
import MyAddresses from './components/MyAddresses';
import SearchResults from './components/SearchResults';
import EditProfile from './components/EditProfile';
import MyOrders from './components/MyOrders';
import OrderPage from './components/OrderPage';
import Payment from './components/Payment';
import Filter from './components/Filter';
import RatePage from './components/RatePage';
import Pay from './components/Pay';
import Videos from './components/Videos';
import OffersPage from './components/OffersPage'
import { L } from './Config';

class TabIcon extends React.Component {
  render() {
    return (
      <Icon
        style={{
          color: this.props.focused ? '#495057' : '#999',
          fontSize: moderateScale(8),
        }}
        name={this.props.name}
        type={this.props.type}></Icon>
    );
  }
}

const RouterComponent = () => {
  return (
    <Router>
      <Stack
        key="root"
        transitionConfig={() => ({
          screenInterpolator: props => {
            const { scene } = props;
            switch (scene.route.routeName) {
              case 'language':
                return StackViewStyleInterpolator.forHorizontal(props);
            }
          },
        })}>
        <Scene key="language" component={Language} hideNavBar={true} />
        <Stack component={Lunch} hideNavBar key="lunch" />
        <Stack
          key="auth"
          hideNavBar={true}
          // initial
          transitionConfig={() => ({
            screenInterpolator: props => {
              const { scene } = props;
              switch (scene.route.routeName) {
                case 'login':
                  return StackViewStyleInterpolator.forHorizontal(props);
                case 'register':
                  return StackViewStyleInterpolator.forHorizontal(props);
                case 'forgot':
                  return StackViewStyleInterpolator.forHorizontal(props);
                default:
                  return StackViewStyleInterpolator.forInitial;
              }
            },
          })}>
          <Scene key="login" component={LoginForm} />
          <Scene
            key="register"
            component={Register}

            navigationBarStyle={[
              styles.routernavigationBarStyle,
              { backgroundColor: '#fff', elevation: 0 },
            ]}
            backButtonTextStyle={[
              styles.routerTitleStyle,
              { color: primaryColor },
            ]}
            renderLeftButton={
              <Button
                onPress={() => Actions.pop()}
                transparent
              /*background={TouchableNativeFeedback.Ripple(secondaryColor)}*/
              // style={{ marginTop: verticalScale(1) }}
              >
                <Icon
                  name="keyboard-backspace"
                  type="MaterialIcons"
                  style={{
                    color: primaryColor,
                    fontSize: 25,
                    transform: [{ scaleX: L('scaleLang') }],
                  }}
                />
              </Button>
            }
          />
          <Scene key="forgot" component={ForgetPassword} hideNavBar />
        </Stack>
        <Stack
          initial
          key="intro"
          component={Intro}
          hideNavBar
          transitionConfig={() => ({
            screenInterpolator: props => {
              const { scene } = props;
              switch (scene.route.routeName) {
                case 'intro':
                  return StackViewStyleInterpolator.forHorizontal(props);
                default:
                  return StackViewStyleInterpolator.forInitial;
              }
            },
          })}
        />
        <Stack
          key="homeStack"
          hideNavBar
          transitionConfig={() => ({
            screenInterpolator: props => {
              const { scene } = props;
              switch (scene.route.routeName) {
                case 'home':
                  return StackViewStyleInterpolator.forHorizontal(props);
                case 'wishlist':
                  return StackViewStyleInterpolator.forHorizontal(props);
                case 'cart':
                  return StackViewStyleInterpolator.forHorizontal(props);
                case 'profile':
                  return StackViewStyleInterpolator.forHorizontal(props);
                case 'catsPage':
                  return StackViewStyleInterpolator.forHorizontal(props);
                case 'productsPage':
                  return StackViewStyleInterpolator.forHorizontal(props);
                case 'categoryPage':
                  return StackViewStyleInterpolator.forHorizontal(props);
                case 'product':
                  return StackViewStyleInterpolator.forHorizontal(props);
                case 'about':
                  return StackViewStyleInterpolator.forHorizontal(props);
                case 'contact':
                  return StackViewStyleInterpolator.forHorizontal(props);
                case 'notifications':
                  return StackViewStyleInterpolator.forHorizontal(props);
                case 'address':
                  return StackViewStyleInterpolator.forHorizontal(props);
                case 'searchResults':
                  return StackViewStyleInterpolator.forHorizontal(props);
                case 'editProfile':
                  return StackViewStyleInterpolator.forHorizontal(props);
                case 'myOrders':
                  return StackViewStyleInterpolator.forHorizontal(props);
                case 'orderPage':
                  return StackViewStyleInterpolator.forHorizontal(props);
                case 'payment':
                  return StackViewStyleInterpolator.forHorizontal(props);
                case 'filter':
                  return StackViewStyleInterpolator.forHorizontal(props);
                case 'ratePage':
                  return StackViewStyleInterpolator.forHorizontal(props);
                case 'pay':
                  return StackViewStyleInterpolator.forHorizontal(props);
                case 'offersPage':
                  return StackViewStyleInterpolator.forHorizontal(props);
                default:
                  return StackViewStyleInterpolator.forInitial;
              }
            },
          })}>
          <Tabs
            // showLabel={false}
            lazy={true}
            labelStyle={styles.tabText}
            tabBarStyle={styles.tabBarStyle}
            activeTintColor="#495057"
            inactiveTintColor="#1b1b1b"
            tabStyle={styles.tabactive}>
            <Scene

              key="profile"
              component={Profile}
              hideNavBar
              tabBarLabel={L('myAccount')}
              icon={TabIcon}
              name="user"
              type="Feather"
            />
            <Scene
              key="videos"
              component={Videos}
              hideNavBar
              tabBarLabel={L('videos')}
              icon={TabIcon}
              name="video"
              type="Feather"
            />
            <Scene
              key="cart"
              component={Cart}
              hideNavBar
              tabBarLabel={L('cart')}
              icon={TabIcon}
              name="shopping-cart"
              type="Feather"
            />
            <Scene
              key="wishlist"
              component={Wishlist}
              hideNavBar
              tabBarLabel={L('wishlist')}
              icon={TabIcon}
              name="heart"
              type="Feather"
            />
            <Scene
              initial
              key="home"
              component={Home}
              hideNavBar
              tabBarLabel={L('home')}
              icon={TabIcon}
              name="home"
              type="Feather"
              onBack={() => BackHandler.exitApp()}
            />



          </Tabs>
          <Scene key="catsPage" component={CatsPage} hideNavBar />
          <Scene key="productsPage" component={ProductsPage} hideNavBar />
          {/*<Scene key="categoryPage" component={CategoryPage} hideNavBar />*/}
          <Scene key="product" component={Product} hideNavBar />
          <Scene key="about" component={About} hideNavBar />
          <Scene key="contact" component={Contact} hideNavBar />
          <Scene key="notifications" component={Notificaions} hideNavBar />
          <Scene key="address" component={MyAddresses} hideNavBar />
          <Scene key="searchResults" component={SearchResults} hideNavBar />
          <Scene key="editProfile" component={EditProfile} hideNavBar />
          <Scene key="myOrders" component={MyOrders} hideNavBar />
          <Scene key="orderPage" component={OrderPage} hideNavBar />
          <Scene key="payment" component={Payment} hideNavBar />
          <Scene key="filter" component={Filter} hideNavBar />
          <Scene key="ratePage" component={RatePage} hideNavBar />
          <Scene key="pay" component={Pay} hideNavBar />
          <Scene key="videosPage" component={Videos} hideNavBar />
          <Scene key="offersPage" component={OffersPage} hideNavBar />
        </Stack>
      </Stack>
    </Router>
  );
};

export default RouterComponent;
