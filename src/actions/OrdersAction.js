import {
  STATE_ADD_TO_CART,
  SAVE_ORDER,
  SAVE_ORDER_FAIL,
  SAVE_ORDER_SUCCESS,
  GET_SIZES,
  GET_SIZES_FAIL,
  GET_SIZES_SUCCESS,
  GET_ORDERS,
  GET_ORDERS_FAIL,
  GET_ORDERS_SUCCESS,
  GET_ORDER,
  GET_ORDER_FAIL,
  GET_ORDER_SUCCESS,
  UPDATE_CART,
  UPDATE_CART_SUCCESS,
  CHECK_COUPON,
  CHECK_COUPON_FAIL,
  CHECK_COUPON_SUCCESS,
  CLEAR_CART
} from './types';
import {Alert} from 'react-native';
import axios from 'axios';
import {Actions} from 'react-native-router-flux';
import {headers, baseUrl, L, changeLng, language, base} from '../Config';
import AsyncStorage from '@react-native-community/async-storage';
import {showMessageChanged} from './AuthActions';
import {DroidKufi, moderateScale, primaryColor} from "../components/assets/styles/Style";
import {Toast} from "native-base";

_storeCart = async cart => {
  // console.log(cart);
  try {
    await AsyncStorage.setItem('cart', JSON.stringify(cart));
  } catch (error) {
    // console.log(error);
  }
};
export const updateCartQty = (id, option, cart, selectedColor) => {
  return dispatch => {
    dispatch({type: UPDATE_CART});
    if (option == 'plus') {
      for (var i = 0; i < cart.length; i++) {
        if (cart[i].id == id) {
          cart[i].quantity = Number(1) + Number(cart[i].quantity);
        } else {
          cart[i] = cart[i];
        }
      }
    } else if (option == 'minus') {
      for (var i = 0; i < cart.length; i++) {
        if (cart[i].id == id) {
          if (cart[i].quantity == 1) {
            cart.splice(i, 1);
          } else if (cart[i].quantity > 1) {
            cart[i].quantity = Number(cart[i].quantity) - Number(1);
          }
        } else {
          cart[i] = cart[i];
        }
      }
    } else if (option == 'all') {
      // console.log(option);
      for (var i = 0; i < cart.length; i++) {
        if (cart[i].id == id) {
          cart.splice(i, 1);
        }
      }
      if (!cart.length) {
        Actions.pop();
      }
    } else if (option == 'delete') {
        for (var i = 0; i < cart.length; i++) {
            if (cart[i].id == id) {
              cart.splice(i, 1);
            } else {
                cart[i] = cart[i];
            }
        }
    } else if (option == 'changeColor') {
      for (var i = 0; i < cart.length; i++) {
        if (cart[i].id == id) {
          cart[i].selected_color = selectedColor;
        } else {
          cart[i] = cart[i];
        }
      }
    }
    updateCartQtySuccess(dispatch, cart);
  };
};
const updateCartQtySuccess = (dispatch, data) => {
  // console.log(providerId);
  dispatch({
    type: UPDATE_CART_SUCCESS,
    payload: data,
  });
  if (!data.length) {
    Actions.pop();
  }
};
export function addToCart(shoppingCartItems, total) {
  //console.log('AddToCart',shoppingCartItems.length,total)
  return {
    type: STATE_ADD_TO_CART,
    payload: {shoppingCartItems: shoppingCartItems, total: total},
  };
}
export const getSizes = () => {
  // console.log(baseUrl + 'pages/' + id,headers);

  return dispatch => {
    dispatch({type: GET_SIZES});
    axios
      .get(baseUrl + 'getSizes/', {
        headers: headers,
      })
      .then(function(data) {
        // console.log(data);
        // console.log(data);
        if (data.data.success) {
          // console.log('success',data);
          getSizesSuccess(dispatch, data.data);
        } else {
          // console.log('faild',data);
          getSizesFail(dispatch, data.data.error);
        }
      })
      .catch(function(error) {
        // console.log(error);

        // handle error
        getSizesFail(dispatch, L('connectionError'));
      });
  };
};
const getSizesFail = (dispatch, error) => {
  dispatch({
    type: GET_SIZES_FAIL,
    payload: error,
  });
  // console.log(error)
};

const getSizesSuccess = (dispatch, data) => {
  // const page = data.data;
  // console.log(page);
  dispatch({
    type: GET_SIZES_SUCCESS,
    payload: data.data,
  });
  // console.log(data)
};
export const getOrders = user_id => {
  return dispatch => {
    dispatch({type: GET_ORDERS});
    axios
      .get(baseUrl + 'getOrders/' + user_id, {
        headers: headers,
      })
      .then(function(data) {
        // console.log(data);
        // console.log(data);
        if (data.data.success) {
          // console.log('success',data.data);
          getOrdersSuccess(dispatch, data.data);
        } else {
          // console.log('faild',data);
          getOrdersFail(dispatch, data.data.error);
        }
      })
      .catch(function(error) {
        // console.log(error);

        // handle error
        getOrdersFail(dispatch, L('connectionError'));
      });
  };
};
const getOrdersFail = (dispatch, error) => {
  dispatch({
    type: GET_ORDERS_FAIL,
    payload: error,
  });
};
const getOrdersSuccess = (dispatch, data) => {
  // console.log(data);
  dispatch({
    type: GET_ORDERS_SUCCESS,
    payload: data.data,
  });
  // console.log(data)
};
export const getOrder = id => {
  return dispatch => {
    dispatch({type: GET_ORDER});
    axios
      .get(baseUrl + 'getOrder/' + id, {
        headers: headers,
      })
      .then(function(data) {
        // console.log(data);
        // console.log(data);
        if (data.data.success) {
          // console.log('success',data.data);
          getOrderSuccess(dispatch, data.data);
        } else {
          // console.log('faild',data);
          getOrderFail(dispatch, data.data.error);
        }
      })
      .catch(function(error) {
        // console.log(error);

        // handle error
        getOrdersFail(dispatch, L('connectionError'));
      });
  };
};
const getOrderFail = (dispatch, error) => {
  dispatch({
    type: GET_ORDER_FAIL,
    payload: error,
  });
};
const getOrderSuccess = (dispatch, data) => {
  // console.log(data);
  dispatch({
    type: GET_ORDER_SUCCESS,
    payload: data.data,
  });
  // console.log(data)
};
export const saveOrder = userData => {
  let cart = userData.cart
  cart = cart.map((item) => {
    if(item.is_gift || item.is_offer) {
      return { ...item, price: 0, points: 0}
    } else {
      return item
    }
  })
  if(userData.payId === 9) {
    const filterCart = cart.filter(item => item.points > 0 || (item.price === 0 && item.points === 0))
    cart = filterCart.map(item => {return { ...item, price: item.points }})
  }
  return dispatch => {
    dispatch({type: SAVE_ORDER});
    axios
      .post(
        baseUrl + 'saveOrder',
        {
          address_id: userData.addressId,
          cart: cart,
          user_id: userData.user_id,
          pay_with_points: userData.pay_with_points,
          pay: userData.payId,
          receipt_photo: userData.receipt_photo,
          coupon: userData.coupon,
          currency_id:   userData.currency_id
        },
        {headers: headers},
      )
      .then(function(data) {
        // console.log(data);
        if (data.data.success) {
          clearCart()
          saveOrderSuccess(dispatch, data.data);
          if (data.data.data.pay == 2) {
            Actions.push('orderPage', {
              orderID: data.data.data.id,
              paymentDone: true
            });
            Actions.push('pay', {orderID: data.data.data.id});
          } else {
              /*Toast.show({
                  text: 'تم الطلب بنجاح',
                  ButtonText: L('dismiss'),
                  duration: 3000,
                  style: {backgroundColor: primaryColor},
                  textStyle: {fontFamily: DroidKufi, fontSize: moderateScale(4.5)},
                  ButtonTextStyle: {fontFamily: DroidKufi, fontSize: moderateScale(4.5)},
              });*/
            Actions.push('orderPage', {
              orderID: data.data.data.id,
              paymentDone: true
            });
              // Alert.alert(
              //     '',
              //     'تم الطلب بنجاح',
              //     [
              //         {
              //             text: 'عرض التفاصيل',
              //             // onPress: () => console.log('Cancel Pressed'),
              //             style: 'cancel',
              //         },
              //     ],
              //     {cancelable: false},
              // );
          }
          // Actions.pop();
        } else {
          saveOrderFail(dispatch, data.data.error);
        }
      })
      .catch(function(error) {
        // handle error
        // console.log(error);

        saveOrderFail(dispatch, L('connectionError'));
      });
  };
};
const saveOrderSuccess = (dispatch, data) => {
  // console.log(error);
  // console.log(error);
  dispatch({
    type: SAVE_ORDER_SUCCESS,
    payload: data.data,
  });
};
const saveOrderFail = (dispatch, error) => {
  // console.log(error);
  // console.log(error);
  dispatch({
    type: SAVE_ORDER_FAIL,
    payload: error,
  });
};
export const checkCoupon = coupon => {
  return dispatch => {
    dispatch({type: CHECK_COUPON});
    axios
      .post(
        baseUrl + 'checkCoupon',
        {
          coupon: coupon,
        },
        {headers: headers},
      )
      .then(function(data) {
        // console.log(data);
        if (data.data.success) {
          //   Actions.pop()
          checkCouponSuccess(dispatch, data.data);
          // Actions.pop();
          // Actions.push('orderPage', {orderID: data.data.data.id});
        } else {
          checkCouponFail(dispatch, data.data.error);
        }
      })
      .catch(function(error) {
        // handle error
        // console.log(error);

        checkCouponFail(dispatch, L('connectionError'));
      });
  };
};
const checkCouponSuccess = (dispatch, data) => {
  // console.log(error);
  // console.log(error);
  dispatch({
    type: CHECK_COUPON_SUCCESS,
    payload: data.data,
  });
};
const checkCouponFail = (dispatch, error) => {
  // console.log(error);
  // console.log(error);
  dispatch({
    type: CHECK_COUPON_FAIL,
    payload: error,
  });
};
export const clearCart = (text) => {
  return dispatch => dispatch({
    type: CLEAR_CART,
    payload: text
  });
};