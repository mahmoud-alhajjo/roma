import {
    CONTACT_FORM,
    CONTACT_FORM_FAIL,
    CONTACT_FORM_SUCCESS,
    PAGES_SUCCESS,
    PAGES_FAIL,
    GET_PAGE,
    GET_HOMESCREEN,
    GET_HOMESCREEN_FAIL,
    GET_HOMESCREEN_SUCCESS,
    GET_NOTIFICATIONS,
    GET_NOTIFICATIONS_FAIL,
    GET_NOTIFICATIONS_SUCCESS,
    CHANGE_ITEM,
    GET_SETTINGS,
    GET_SETTINGS_FAIL,
    GET_SETTINGS_SUCCESS,
    GET_ADDRESSES,
    GET_ADDRESSES_SUCCESS,
    GET_ADDRESSES_FAIL,
    ADD_ADDRESS,
    ADD_ADDRESS_FAIL,
    ADD_ADDRESS_SUCCESS,
    SEARCH,
    SEARCH_FAIL,
    SEARCH_SUCCESS,
    ADD_COMMENT,
    ADD_COMMENT_FAIL,
    ADD_COMMENT_SUCCESS,
    GET_COMMENTS,
    GET_COMMENTS_FAIL,
    GET_COMMENTS_SUCCESS,
    DELETE_ADDRESSES,
    DELETE_ADDRESSES_FAIL,
    DELETE_ADDRESSES_SUCCESS,
    FILTER,
    FILTER_FAIL,
    FILTER_SUCCESS,
    GET_FILTER_OPTIONS,
    GET_FILTER_OPTIONS_FAIL,
    GET_FILTER_OPTIONS_SUCCESS,
    GET_CURRENCIRES_SUCCESS,
    GET_CURRENCIRES_FAIL,
    GET_CURRENCIRES,
    GET_VIDEOS,
    GET_VIDEOS_FAIL, GET_VIDEOS_SUCCESS,
} from './types';
import {Alert} from 'react-native';
import axios from 'axios';
import {Actions} from 'react-native-router-flux';
import {headers, baseUrl, L, changeLng, language, base} from '../Config';
import AsyncStorage from '@react-native-community/async-storage';
import {showMessageChanged} from './AuthActions';

_storeData = async user => {
  // console.log(user);
  try {
    await AsyncStorage.setItem('user', JSON.stringify(user));
  } catch (error) {
    // console.log(error);
  }
};
export const changeItem = response => {
  return {
    type: CHANGE_ITEM,
    payload: response,
  };
};
export const contactForm = ({sendContact}) => {
  return dispatch => {
    dispatch({type: CONTACT_FORM});
    // console.log(baseUrl, email, password);
    axios({
      method: 'post',
      url: baseUrl + 'contactUs',
      data: {
        email: sendContact.email,
        name: sendContact.name,
        phone: sendContact.phone,
        message: sendContact.message,
      },
      headers: headers,
    })
      .then(data => checkMessageResponse(dispatch, data))
      .catch(function(error) {
        // handle error
        contactFormFail(dispatch, L('connectionError'));
      });
  };
};
const checkMessageResponse = (dispatch, data) => {
  // console.log(data);
  if (data.data.success) {
    contactFormSuccess(dispatch, data.data);
    Actions.pop();
  } else {
    contactFormFail(dispatch, data.data.error);
  }
};
const contactFormFail = (dispatch, error) => {
  // console.log(error);
  dispatch({
    type: CONTACT_FORM_FAIL,
    payload: error,
  });
};

const contactFormSuccess = (dispatch, result) => {
  // console.log(result.data);
  dispatch({
    type: CONTACT_FORM_SUCCESS,
    payload: result.data,
  });
};
export const pages = () => {
  // console.log(baseUrl + 'pages/' + id,headers);

  return dispatch => {
    dispatch({type: GET_PAGE});
    axios
      .get(baseUrl + 'pages/', {
        headers: headers,
      })
      .then(function(data) {
        // console.log(data);
        // console.log(data);
        if (data.data.success) {
          // console.log('success',data);
          pagesSuccess(dispatch, data.data);
        } else {
          // console.log('faild',data);
          pagesFail(dispatch, data.data.error);
        }
      })
      .catch(function(error) {
        // console.log(error);

        // handle error
        pagesFail(dispatch, L('connectionError'));
      });
  };
};
const pagesFail = (dispatch, error) => {
  dispatch({
    type: PAGES_FAIL,
    payload: error,
  });
  // console.log(error)
};

const pagesSuccess = (dispatch, data) => {
  // const page = data.data;
  // console.log(page);
  dispatch({
    type: PAGES_SUCCESS,
    payload: data.data,
  });
  // console.log(data)
};

export const getHomescreen = () => {
  return dispatch => {
    dispatch({type: GET_HOMESCREEN});
    axios
      .get(baseUrl + 'homeScreen/', {
        headers: headers,
      })
      .then(function(data) {
        // console.log(data);
        // console.log(data);
        if (data.data.success) {
          // console.log('success',data.data);
          getHomescreenSuccess(dispatch, data.data);
        } else {
          // console.log('faild',data);
          getHomescreenFail(dispatch, data.data.error);
        }
      })
      .catch(function(error) {
        // console.log(error);

        // handle error
        getHomescreenFail(dispatch, L('connectionError'));
      });
  };
};
const getHomescreenFail = (dispatch, error) => {
  dispatch({
    type: GET_HOMESCREEN_FAIL,
    payload: error,
  });
};
const getHomescreenSuccess = (dispatch, data) => {
  // console.log(data);
  dispatch({
    type: GET_HOMESCREEN_SUCCESS,
    payload: data.data,
  });
  // console.log(data)
};
export const getNotifications = user_id => {
  return dispatch => {
    dispatch({type: GET_NOTIFICATIONS});
    axios
      .get(baseUrl + 'notifications/' + user_id, {
        headers: headers,
      })
      .then(function(data) {
        // console.log(data);
        // console.log(data);
        if (data.data.success) {
          // console.log('success',data.data);
          getNotificationsSuccess(dispatch, data.data);
        } else {
          // console.log('faild',data);
          getNotificationsFail(dispatch, data.data.error);
        }
      })
      .catch(function(error) {
        // console.log(error);
        // handle error
        getNotificationsFail(dispatch, L('connectionError'));
      });
  };
};
const getNotificationsFail = (dispatch, error) => {
  dispatch({
    type: GET_NOTIFICATIONS_FAIL,
    payload: error,
  });
};
const getNotificationsSuccess = (dispatch, data) => {
  // const page = data.data;
  // console.log(page);
  dispatch({
    type: GET_NOTIFICATIONS_SUCCESS,
    payload: data.data,
  });
  // console.log(data)
};
export const getSettings = () => {
  // console.log(baseUrl + 'pages/' + id,headers);

  return dispatch => {
    dispatch({type: GET_SETTINGS});
    axios
      .get(baseUrl + 'getSettings/', {
        headers: headers,
      })
      .then(function(data) {
        // console.log(data);
        // console.log(data);
        if (data.data.success) {
          // console.log('success',data);
          getSettingsSuccess(dispatch, data.data);
        } else {
          // console.log('faild',data);
          getSettingsFail(dispatch, data.data.error);
        }
      })
      .catch(function(error) {
        // console.log(error);

        // handle error
        getSettingsFail(dispatch, L('connectionError'));
      });
  };
};
const getSettingsFail = (dispatch, error) => {
  dispatch({
    type: GET_SETTINGS_FAIL,
    payload: error,
  });
  // console.log(error)
};

const getSettingsSuccess = (dispatch, data) => {
  // const page = data.data;
  // console.log(page);
  dispatch({
    type: GET_SETTINGS_SUCCESS,
    payload: data.data,
  });
  // console.log(data)
};
export const getAddress = client_id => {
  // console.log(baseUrl + 'pages/' + id,headers);

  return dispatch => {
    dispatch({type: GET_ADDRESSES});
    axios
      .get(baseUrl + 'getAddress/' + client_id, {
        headers: headers,
      })
      .then(function(data) {
        // console.log(data);
        // console.log(data);
        if (data.data.success) {
          // console.log('success',data);
          getAddressSuccess(dispatch, data.data);
        } else {
          // console.log('faild',data);
          getAddressFail(dispatch, data.data.error);
        }
      })
      .catch(function(error) {
        // console.log(error);

        // handle error
        getAddressFail(dispatch, L('connectionError'));
      });
  };
};
const getAddressFail = (dispatch, error) => {
  dispatch({
    type: GET_ADDRESSES_FAIL,
    payload: error,
  });
  // console.log(error)
};

const getAddressSuccess = (dispatch, data) => {
  // const page = data.data;
  // console.log(page);
  dispatch({
    type: GET_ADDRESSES_SUCCESS,
    payload: data.data,
  });
  // console.log(data)
};
export const deleteAddress = (client_id, address_id) => {
  // console.log(client_id, address_id);

  return dispatch => {
    dispatch({type: DELETE_ADDRESSES});
    axios
      .get(baseUrl + 'deleteAddress/' + client_id + '/' + address_id, {
        headers: headers,
      })
      .then(function(data) {
        // console.log(data);
        // console.log(data);
        if (data.data.success) {
          // console.log('success', data);
          deleteAddressSuccess(dispatch, data.data);
        } else {
          // console.log('faild',data);
          deleteAddressFail(dispatch, data.data.error);
        }
      })
      .catch(function(error) {
        // console.log(error);

        // handle error
        deleteAddressFail(dispatch, L('connectionError'));
      });
  };
};
const deleteAddressFail = (dispatch, error) => {
  dispatch({
    type: DELETE_ADDRESSES_FAIL,
    payload: error,
  });
  // console.log(error)
};

const deleteAddressSuccess = (dispatch, data) => {
  // const page = data.data;
  // console.log(page);
  dispatch({
    type: DELETE_ADDRESSES_SUCCESS,
    payload: data.data,
  });
  // console.log(data)
};
export const addAddress = ({addAddress}) => {
  // console.log(baseUrl + 'pages/' + id,headers);

  return dispatch => {
    dispatch({type: ADD_ADDRESS});
    axios({
      method: 'post',
      url: baseUrl + 'addAddress',
      data: {
        client_id: addAddress.client_id,
        address: addAddress.address,
          country_address: addAddress.countryAddress,
          city_address: addAddress.cityAddress,
          district_address: addAddress.districtAddress,
          street_address: addAddress.streetAddress,
          building_address: addAddress.buildingAddress,
      },
      headers: headers,
    })
      .then(function(data) {
        // console.log(data);
        // console.log(data);
        if (data.data.success) {
          // console.log('success', data);
          addAddressSuccess(dispatch, data.data);
        } else {
          // console.log('faild', data);
          addAddressFail(dispatch, data.data.error);
        }
      })
      .catch(function(error) {
        // console.log(error);

        // handle error
        addAddressFail(dispatch, L('connectionError'));
      });
  };
};
const addAddressFail = (dispatch, error) => {
  dispatch({
    type: ADD_ADDRESS_FAIL,
    payload: error,
  });
  // console.log(error)
};

const addAddressSuccess = (dispatch, data) => {
  // const page = data.data;
  // console.log(page);
  dispatch({
    type: ADD_ADDRESS_SUCCESS,
    payload: data.data,
  });
  // console.log(data)
};
export const search = postSearch => {
  // console.log(baseUrl + 'pages/' + id,headers);

  return dispatch => {
    dispatch({type: SEARCH});
    axios({
      method: 'post',
      url: baseUrl + 'search',
      data: {
        search: postSearch.search,
      },
      headers: headers,
    })
      .then(function(data) {
        // console.log(data);
        // console.log(data);
        if (data.data.success) {
          // console.log('success', data);
          searchSuccess(dispatch, data.data);
        } else {
          // console.log('faild', data);
          searchFail(dispatch, data.data.error);
        }
      })
      .catch(function(error) {
        // console.log(error);

        // handle error
        searchFail(dispatch, L('connectionError'));
      });
  };
};
const searchFail = (dispatch, error) => {
  dispatch({
    type: SEARCH_FAIL,
    payload: error,
  });
  // console.log(error)
};

const searchSuccess = (dispatch, data) => {
  // const page = data.data;
  // console.log(page);
  dispatch({
    type: SEARCH_SUCCESS,
    payload: data.data,
  });
  // console.log(data)
};
export const filter = postFilter => {
  // console.log(postFilter);

  return dispatch => {
    dispatch({type: FILTER});
    axios({
      method: 'post',
      url: baseUrl + 'filter',
      data: {
        category_id: postFilter.category_id,
        trademarks: postFilter.trademarks,
        duration: postFilter.duration,
        colors: postFilter.colors,
      },
      headers: headers,
    })
      .then(function(data) {
        // console.log(data);
        // console.log(data);
        if (data.data.success) {
          // console.log('success', data);
          filterSuccess(dispatch, data.data);
        } else {
          // console.log('faild', data);
          filterFail(dispatch, data.data.error);
        }
      })
      .catch(function(error) {
        // console.log(error);

        // handle error
        filterFail(dispatch, L('connectionError'));
      });
  };
};
const filterFail = (dispatch, error) => {
  dispatch({
    type: FILTER_FAIL,
    payload: error,
  });
  // console.log(error)
};

const filterSuccess = (dispatch, data) => {
  // const page = data.data;
  // console.log(page);
  dispatch({
    type: FILTER_SUCCESS,
    payload: data.data,
  });
  // console.log(data)
};
export const getFilterOptions = () => {
  // console.log(baseUrl + 'pages/' + id,headers);

  return dispatch => {
    dispatch({type: GET_FILTER_OPTIONS});
    axios
      .get(baseUrl + 'getFilterOptions', {
        // params: {
        //     product_id: product_id
        // },
        headers: headers,
      })
      .then(function(data) {
        // console.log(data);
        // console.log(data);
        if (data.data.success) {
          // console.log('success',data);
          getFilterOptionsSuccess(dispatch, data.data);
        } else {
          // console.log('faild',data);
          getFilterOptionsFail(dispatch, data.data.error);
        }
      })
      .catch(function(error) {
        // console.log(error);

        // handle error
        getFilterOptionsFail(dispatch, L('connectionError'));
      });
  };
};
const getFilterOptionsFail = (dispatch, error) => {
  dispatch({
    type: GET_FILTER_OPTIONS_FAIL,
    payload: error,
  });
  // console.log(error)
};

const getFilterOptionsSuccess = (dispatch, data) => {
  // const page = data.data;
  // console.log(page);
  dispatch({
    type: GET_FILTER_OPTIONS_SUCCESS,
    payload: data.data,
  });
  // console.log(data)
};
export const addComment = ({addComment}) => {
  // console.log(baseUrl + 'pages/' + id,headers);

  return dispatch => {
    dispatch({type: ADD_COMMENT});
    axios({
      method: 'post',
      url: baseUrl + 'addComment',
      data: {
        user_id: addComment.user_id,
        product_id: addComment.product_id,
        comment: addComment.comment,
        rating: addComment.rating,
      },
      headers: headers,
    })
      .then(function(data) {
        // console.log(data);
        // console.log(data);
        if (data.data.success) {
          // console.log('success', data);
          addCommentSuccess(dispatch, data.data);
        } else {
          // console.log('faild', data);
          addCommentFail(dispatch, data.data.error);
        }
      })
      .catch(function(error) {
        // console.log(error);

        // handle error
        addCommentFail(dispatch, L('connectionError'));
      });
  };
};
const addCommentFail = (dispatch, error) => {
  dispatch({
    type: ADD_COMMENT_FAIL,
    payload: error,
  });
  // console.log(error)
};

const addCommentSuccess = (dispatch, data) => {
  // const page = data.data;
  // console.log(page);
  dispatch({
    type: ADD_COMMENT_SUCCESS,
    payload: data.data,
  });
  // console.log(data)
};
export const getComments = product_id => {
  // console.log(baseUrl + 'pages/' + id,headers);

  return dispatch => {
    dispatch({type: GET_COMMENTS});
    axios
      .get(baseUrl + 'getComments', {
        params: {
          product_id: product_id,
        },
        headers: headers,
      })
      .then(function(data) {
        // console.log(data);
        // console.log(data);
        if (data.data.success) {
          // console.log('success',data);
          getCommentsSuccess(dispatch, data.data);
        } else {
          // console.log('faild',data);
          getCommentsFail(dispatch, data.data.error);
        }
      })
      .catch(function(error) {
        // console.log(error);

        // handle error
        getCommentsFail(dispatch, L('connectionError'));
      });
  };
};
const getCommentsFail = (dispatch, error) => {
  dispatch({
    type: GET_COMMENTS_FAIL,
    payload: error,
  });
  // console.log(error)
};

const getCommentsSuccess = (dispatch, data) => {
  // const page = data.data;
  // console.log(page);
  dispatch({
    type: GET_COMMENTS_SUCCESS,
    payload: data.data,
  });
  // console.log(data)
};
export const getCurrencies = (id) => {
    return dispatch => {
        dispatch({type: GET_CURRENCIRES});
        axios
            .get(baseUrl + 'getCurrencies/' + (id ? id : ''), {
                headers: headers,
            })
            .then(function(data) {

                if (data.data) {
                    getCurrencySuccess(dispatch, data.data);
                } else {
                    getCurrencyFail(dispatch, data.data.error);
                }
            })
            .catch(function(error) {
                getCurrencyFail(dispatch, L('connectionError'));
            });
    };
};
const getCurrencyFail = (dispatch, error) => {
    dispatch({
        type: GET_CURRENCIRES_FAIL,
        payload: error,
    });
};
const getCurrencySuccess = (dispatch, data) => {

  if (global.currency == null) {
      AsyncStorage.getItem('selectedCurrencyId').then(res => {
          console.log(res);
          if (res != null) {
              global.currency = data.data.filter(item => item.id == res);
              if (global.currency.length > 0) {
                  global.currency = global.currency[0];
              }
          } else {
              global.currency = data.data[0];
          }
      });
  }
    dispatch({
        type: GET_CURRENCIRES_SUCCESS,
        payload: data.data,
    });
    // console.log(data)
};

export const getVideos = (id) => {
    return dispatch => {
        dispatch({type: GET_VIDEOS});
        axios
            .get(baseUrl + 'getVideos/', {
                headers: headers,
            })
            .then(function(data) {

                if (data.data) {
                    console.log(data.data);
                    getVideosSuccess(dispatch, data.data);
                } else {
                    getVideosFail(dispatch, data.data.error);
                }
            })
            .catch(function(error) {
                console.log(error);
                getVideosFail(dispatch, L('connectionError'));
            });
    };
};
const getVideosFail = (dispatch, error) => {
    dispatch({
        type: GET_VIDEOS_FAIL,
        payload: error,
    });
};
const getVideosSuccess = (dispatch, data) => {
    dispatch({
        type: GET_VIDEOS_SUCCESS,
        payload: data.data,
    });
};

export function getPrice(price) {
  return +(price * (global.currency? global.currency.rate: 1)).toFixed(2);
}