import {
    GET_MAIN_CAT,
    GET_MAIN_CAT_FAIL,
    GET_MAIN_CAT_SUCCESS,
    GET_CATS_BY_PARENT,
    GET_CATS_BY_PARENT_FAIL,
    GET_CATS_BY_PARENT_SUCCESS,
    GET_PRODUCTS_BY_CATID,
    GET_PRODUCTS_BY_CATID_FAIL,
    GET_PRODUCTS_BY_CATID_SUCCESS,
    GET_PRODUCTS_BY_BRANDID,
    GET_PRODUCTS_BY_BRANDID_FAIL,
    GET_PRODUCTS_BY_BRANDID_SUCCESS,
    GET_PRODUCT,
    GET_PRODUCT_FAIL,
    GET_PRODUCT_SUCCESS,
    COUNT_CHANGED,
    ADD_TO_WISHLIST,
    ADD_TO_WISHLIST_FAIL,
    ADD_TO_WISHLIST_SUCCESS,
    DELETE_WISHLIST,
    DELETE_WISHLIST_FAIL,
    DELETE_WISHLIST_SUCCESS,
    GET_WISHLIST,
    GET_WISHLIST_FAIL,
    GET_WISHLIST_SUCCESS,
    GET_DIS_PRODUCTS,
    GET_DIS_PRODUCTS_FAIL,
    GET_DIS_PRODUCTS_SUCCESS,
    GET_BRAND_PRODUCTS,
    GET_BRAND_PRODUCTS_SUCCESS,
    GET_BRAND_PRODUCTS_FAIL,
    GET_SLIDERS_BY_BRAND,
    GET_SLIDERS_BY_BRAND_FAIL,
    GET_SLIDERS_BY_BRAND_SUCCESS,
    GET_SLIDERS_BY_CATEGORY, GET_SLIDERS_BY_CATEGORY_FAIL, GET_SLIDERS_BY_CATEGORY_SUCCESS,
    GET_WHEEL_ITEMS_FORTUNE, GET_WHEEL_ITEMS_FORTUNE_FAIL, GET_WHEEL_ITEMS_FORTUNE_SUCCESS,
    GIFT_TO_USER, GIFT_TO_USER_FAIL, GIFT_TO_USER_SUCCESS,
  } from './types';
import {Alert} from 'react-native';
import axios from 'axios';
import {Actions} from 'react-native-router-flux';
import {headers, baseUrl, L, changeLng, language, base} from '../Config';
import AsyncStorage from '@react-native-community/async-storage';
import {showMessageChanged} from './AuthActions';

_storeData = async user => {
  // console.log(user);
  try {
    await AsyncStorage.setItem('user', JSON.stringify(user));
  } catch (error) {
    // console.log(error);
  }
};
export const countOrder = text => {
  // console.log(text);
  return {
    type: COUNT_CHANGED,
    payload: text,
  };
};
export const getMainCat = () => {
  return dispatch => {
    dispatch({type: GET_MAIN_CAT});
    axios
      .get(baseUrl + 'getMainCat/', {
        headers: headers,
      })
      .then(function(data) {
        // console.log(data);
        // console.log(data);
        if (data.data.success) {
          // console.log('success',data.data);
          getMainCatSuccess(dispatch, data.data);
        } else {
          // console.log('faild',data);
          getMainCatFail(dispatch, data.data.error);
        }
      })
      .catch(function(error) {
        // console.log(error);

        // handle error
        getMainCatFail(dispatch, L('connectionError'));
      });
  };
};
const getMainCatFail = (dispatch, error) => {
  dispatch({
    type: GET_MAIN_CAT_FAIL,
    payload: error,
  });
};
const getMainCatSuccess = (dispatch, data) => {
  // console.log(data);
  dispatch({
    type: GET_MAIN_CAT_SUCCESS,
    payload: data.data,
  });
  // console.log(data)
};
export const getCatsByParent = parent_id => {
  return dispatch => {
    dispatch({type: GET_CATS_BY_PARENT});
    axios
      .get(baseUrl + 'getCatsByParent/' + parent_id, {
        headers: headers,
      })
      .then(function(data) {
        // console.log(data);
        // console.log(data);
        if (data.data.success) {
          // console.log('success',data.data);
          getCatsByParentSuccess(dispatch, data.data);
        } else {
          // console.log('faild',data);
          getCatsByParentFail(dispatch, data.data.error);
        }
      })
      .catch(function(error) {
        // console.log(error);

        // handle error
        getCatsByParentFail(dispatch, L('connectionError'));
      });
  };
};
export const getCatsByBrand = brand_id => {
    return dispatch => {
        dispatch({type: GET_CATS_BY_PARENT});
        axios
            .get(baseUrl + 'getCatsByBrand/' + brand_id, {
                headers: headers,
            })
            .then(function(data) {
                // console.log(data);
                // console.log(data);
                if (data.data.success) {
                    // console.log('success',data.data);
                    getCatsByParentSuccess(dispatch, data.data);
                } else {
                    // console.log('faild',data);
                    getCatsByParentFail(dispatch, data.data.error);
                }
            })
            .catch(function(error) {
                // console.log(error);

                // handle error
                getCatsByParentFail(dispatch, L('connectionError'));
            });
    };
};
const getCatsByParentFail = (dispatch, error) => {
  dispatch({
    type: GET_CATS_BY_PARENT_FAIL,
    payload: error,
  });
};
const getCatsByParentSuccess = (dispatch, data) => {
  // console.log(data);
  dispatch({
    type: GET_CATS_BY_PARENT_SUCCESS,
    payload: data.data,
  });
  // console.log(data)
};

export const getProductsByCatId = category_id => {
  return dispatch => {
    dispatch({type: GET_PRODUCTS_BY_CATID});
    axios
      .get(baseUrl + 'getProductsByCatId/' + category_id, {
        headers: headers,
      })
      .then(function(data) {
        // console.log(data);
        // console.log(data);
        if (data.data.success) {
          // console.log('success',data.data);
          getProductsByCatIdSuccess(dispatch, data.data);
        } else {
          // console.log('faild',data);
          getProductsByCatIdFail(dispatch, data.data.error);
        }
      })
      .catch(function(error) {
        // console.log(error);

        // handle error
        getProductsByCatIdFail(dispatch, L('connectionError'));
      });
  };
};
const getProductsByCatIdFail = (dispatch, error) => {
  dispatch({
    type: GET_PRODUCTS_BY_CATID_FAIL,
    payload: error,
  });
};
const getProductsByCatIdSuccess = (dispatch, data) => {
  // console.log(data);
  dispatch({
    type: GET_PRODUCTS_BY_CATID_SUCCESS,
    payload: data.data,
  });
  // console.log(data)
};
export const getProductsByBrandId = brand_id => {
  return dispatch => {
    dispatch({type: GET_PRODUCTS_BY_BRANDID});
    axios
      .get(baseUrl + 'getProductsByBrandId/' + brand_id, {
        headers: headers,
      })
      .then(function(data) {
        // console.log(data);
        // console.log(data);
        if (data.data.success) {
          // console.log('success',data.data);
          getProductsByBrandIdSuccess(dispatch, data.data);
        } else {
          // console.log('faild',data);
          getProductsByBrandIdFail(dispatch, data.data.error);
        }
      })
      .catch(function(error) {
        // console.log(error);

        // handle error
        getProductsByBrandIdFail(dispatch, L('connectionError'));
      });
  };
};
const getProductsByBrandIdFail = (dispatch, error) => {
  dispatch({
    type: GET_PRODUCTS_BY_BRANDID_FAIL,
    payload: error,
  });
};
const getProductsByBrandIdSuccess = (dispatch, data) => {
  // console.log(data);
  dispatch({
    type: GET_PRODUCTS_BY_BRANDID_SUCCESS,
    payload: data.data,
  });
  // console.log(data)
};
export const getProduct = id => {
  return dispatch => {
    dispatch({type: GET_PRODUCT});
    axios
      .get(baseUrl + 'getProduct/' + id, {
        headers: headers,
      })
      .then(function(data) {
        // console.log(data);
        // console.log(data);
        if (data.data.success) {
          // console.log('success',data.data);
          getProductSuccess(dispatch, data.data);
        } else {
          // console.log('faild',data);
          getProductFail(dispatch, data.data.error);
        }
      })
      .catch(function(error) {
        // console.log(error);

        // handle error
        getProductFail(dispatch, L('connectionError'));
      });
  };
};
const getProductFail = (dispatch, error) => {
  dispatch({
    type: GET_PRODUCT_FAIL,
    payload: error,
  });
};
const getProductSuccess = (dispatch, data) => {
  // console.log(data);
  dispatch({
    type: GET_PRODUCT_SUCCESS,
    payload: data.data,
  });
  // console.log(data)
};
export const getWhishlist = client_id => {
  // console.log(baseUrl + 'getWhishlist/' + id,headers);

  return dispatch => {
    dispatch({type: GET_WISHLIST});
    axios
      .get(baseUrl + 'getWhishlist/' + client_id, {
        headers: headers,
      })
      .then(function(data) {
        // console.log(data);
        // console.log(data);
        if (data.data.success) {
          // console.log('success',data);
          getWhishlistSuccess(dispatch, data.data);
        } else {
          // console.log('faild',data);
          getWhishlistFail(dispatch, data.data.error);
        }
      })
      .catch(function(error) {
        // console.log(error);

        // handle error
        getWhishlistFail(dispatch, L('connectionError'));
      });
  };
};
const getWhishlistFail = (dispatch, error) => {
  dispatch({
    type: GET_WISHLIST_FAIL,
    payload: error,
  });
  // console.log(error)
};

const getWhishlistSuccess = (dispatch, data) => {
  // const page = data.data;
  // console.log(page);
  dispatch({
    type: GET_WISHLIST_SUCCESS,
    payload: data.data,
  });
  // console.log(data)
};
export const addToWhishlist = ({postFavourite}) => {
  // console.log({ postFavourite });

  return dispatch => {
    dispatch({type: ADD_TO_WISHLIST});
    axios({
      method: 'post',
      url: baseUrl + 'addToWhishlist',
      data: {
        client_id: postFavourite.client_id,
        product_id: postFavourite.product_id,
      },
      headers: headers,
    })
      .then(function(data) {
        if (data.data.success) {
          // console.log('success',data);
          addToWhishlistSuccess(dispatch, data.data);
        } else {
          // console.log('faild',data);
          addToWhishlistFail(dispatch, data.data.error);
        }
      })
      .catch(function(error) {
        // handle error
        addToWhishlistFail(dispatch, L('connectionError'));
      });
  };
};
const addToWhishlistFail = (dispatch, error) => {
  // console.log(error);
  dispatch({
    type: ADD_TO_WISHLIST_FAIL,
    payload: error,
  });
};
const addToWhishlistSuccess = (dispatch, data) => {
  // console.log(data.data);
  dispatch({
    type: ADD_TO_WISHLIST_SUCCESS,
    payload: data.data,
  });
};
export const deleteWhishlist = ({deleteFavourite}) => {
  // console.log({ postReport });

  return dispatch => {
    dispatch({type: DELETE_WISHLIST});
    axios({
      method: 'post',
      url: baseUrl + 'deleteWhishlist',
      data: {
        client_id: deleteFavourite.client_id,
        product_id: deleteFavourite.product_id,
      },
      headers: headers,
    })
      .then(function(data) {
        if (data.data.success) {
          // console.log('success',data);
          deleteWhishlistSuccess(dispatch, data.data);
        } else {
          // console.log('faild',data);
          deleteWhishlisttFail(dispatch, data.data.error);
        }
      })
      .catch(function(error) {
        // handle error
        deleteWhishlisttFail(dispatch, L('connectionError'));
      });
  };
};
const deleteWhishlisttFail = (dispatch, error) => {
  // console.log(error);
  dispatch({
    type: DELETE_WISHLIST_FAIL,
    payload: error,
  });
};
const deleteWhishlistSuccess = (dispatch, data) => {
  // console.log(data.data);
  dispatch({
    type: DELETE_WISHLIST_SUCCESS,
    payload: data.data,
  });
};
export const getDiscountedProducts = () => {
  // console.log(baseUrl + 'getWhishlist/' + id,headers);

  return dispatch => {
    dispatch({type: GET_DIS_PRODUCTS});
    axios
      .get(baseUrl + 'discountedProducts/', {
        headers: headers,
      })
      .then(function(data) {
        // console.log(data);
        // console.log(data);
        if (data.data.success) {
          // console.log('success',data);
          getDiscountedProductsSuccess(dispatch, data.data);
        } else {
          // console.log('faild',data);
          getDiscountedProductsFail(dispatch, data.data.error);
        }
      })
      .catch(function(error) {
        // console.log(error);

        // handle error
        getDiscountedProductsFail(dispatch, L('connectionError'));
      });
  };
};
const getDiscountedProductsFail = (dispatch, error) => {
  dispatch({
    type: GET_DIS_PRODUCTS_FAIL,
    payload: error,
  });
  // console.log(error)
};
const getDiscountedProductsSuccess = (dispatch, data) => {
  // console.log(data.data);
  dispatch({
    type: GET_DIS_PRODUCTS_SUCCESS,
    payload: data.data,
  });
};
export const productsByCatsBrand = (category_id, brand_id) => {
  // console.log(baseUrl + 'getWhishlist/' + id,headers);

  return dispatch => {
    dispatch({type: GET_BRAND_PRODUCTS});
    axios
      .get(baseUrl + 'productsByCatsBrand/' + category_id + '/' + brand_id, {
        headers: headers,
      })
      .then(function(data) {
        // console.log(data);
        // console.log(data);
        if (data.data.success) {
          // console.log('success',data);
          productsByCatsBrandSuccess(dispatch, data.data);
        } else {
          // console.log('faild',data);
          productsByCatsBrandFail(dispatch, data.data.error);
        }
      })
      .catch(function(error) {
        // console.log(error);

        // handle error
        productsByCatsBrandFail(dispatch, L('connectionError'));
      });
  };
};
const productsByCatsBrandFail = (dispatch, error) => {
  dispatch({
    type: GET_BRAND_PRODUCTS_FAIL,
    payload: error,
  });
  // console.log(error)
};
const productsByCatsBrandSuccess = (dispatch, data) => {
  // console.log(data.data);
  dispatch({
    type: GET_BRAND_PRODUCTS_SUCCESS,
    payload: data.data,
  });
};
export const getSlidersByBrand = brand_id => {
    return dispatch => {
        dispatch({type: GET_SLIDERS_BY_BRAND});
        axios
            .get(baseUrl + 'getBrandSlider/' + brand_id, {
                headers: headers,
            })
            .then(function(data) {
                // console.log(data);
                //console.log(data);
                if (data.data.success) {
                    getSlidersByBrandSuccess(dispatch, data.data);
                } else {
                    // console.log('faild',data);
                    getSlidersByBrandFail(dispatch, data.data.error);
                }
            })
            .catch(function(error) {
                // console.log(error);

                // handle error
                getSlidersByBrandFail(dispatch, L('connectionError'));
            });
    };
};
const getSlidersByBrandFail = (dispatch, error) => {
    dispatch({
        type: GET_SLIDERS_BY_BRAND_FAIL,
        payload: error,
    });
};
const getSlidersByBrandSuccess = (dispatch, data) => {
    // console.log(data);
    dispatch({
        type: GET_SLIDERS_BY_BRAND_SUCCESS,
        payload: data.data,
    });
    // console.log(data)
};

export const getSlidersByCategory = category_id => {
    return dispatch => {
        dispatch({type: GET_SLIDERS_BY_CATEGORY});
        axios
            .get(baseUrl + 'getCategorySlider/' + category_id, {
                headers: headers,
            })
            .then(function(data) {
                // console.log(data);
                //console.log(data);
                if (data.data.success) {
                    getSlidersByCategorySuccess(dispatch, data.data);
                } else {
                    // console.log('faild',data);
                    getSlidersByCategoryFail(dispatch, data.data.error);
                }
            })
            .catch(function(error) {
                // console.log(error);

                // handle error
                getSlidersByCategoryFail(dispatch, L('connectionError'));
            });
    };
};
const getSlidersByCategoryFail = (dispatch, error) => {
    dispatch({
        type: GET_SLIDERS_BY_CATEGORY_FAIL,
        payload: error,
    });
};
const getSlidersByCategorySuccess = (dispatch, data) => {
    // console.log(data);
    dispatch({
        type: GET_SLIDERS_BY_CATEGORY_SUCCESS,
        payload: data.data,
    });
    // console.log(data)
};

export const getWheelItems = () => {
  return dispatch => {
      dispatch({type: GET_WHEEL_ITEMS_FORTUNE});
      axios
          .get(baseUrl + 'getWheelItems', {
              headers: headers,
          })
          .then(function(data) {
              // console.log(data);
              //console.log(data);
              if (data.data.success) {
                  // console.log('success',data.data);
                  getWheelItemsSuccess(dispatch, data.data);
              } else {
                  // console.log('faild',data);
                  getWheelItemsFail(dispatch, data.data.error);
              }
          })
          .catch(function(error) {
              // console.log(error);

              // handle error
              getWheelItemsFail(dispatch, L('connectionError'));
          });
  };
};
const getWheelItemsFail = (dispatch, error) => {
  dispatch({
      type: GET_WHEEL_ITEMS_FORTUNE_FAIL,
      payload: error,
  });
};
const getWheelItemsSuccess = (dispatch, data) => {
  // console.log(data);
  dispatch({
      type: GET_WHEEL_ITEMS_FORTUNE_SUCCESS,
      payload: data.data,
  });
  // console.log(data)
};

export const giftToUser = (userID, wheelItemId) => {
  return dispatch => {
    dispatch({type: GIFT_TO_USER});
    axios({
      method: 'post',
      url: baseUrl + 'giveGiftToUser',
      data: {
        user_id: userID,
        wheel_item_id: wheelItemId,
      },
      headers: headers,
    })
      .then(function(data) {
        if (data.data.success) {
          console.log('giftToUserSuccess',data);
          giftToUserSuccess(dispatch, data.data);
        } else {
          // console.log('giftToUserFail',data);
          giftToUserFail(dispatch, data.data.error);
        }
      })
      .catch(function(error) {
        // handle error
        giftToUserFail(dispatch, L('connectionError'));
      });
  };
};
const giftToUserFail = (dispatch, error) => {
  // console.log('error',error);
  dispatch({
    type: GIFT_TO_USER_FAIL,
    payload: error,
  });
};
const giftToUserSuccess = (dispatch, data) => {
  // console.log(data.data);
  dispatch({
    type: GIFT_TO_USER_SUCCESS,
    payload: data.data,
  });
};
