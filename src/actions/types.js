//AUTH
export const EMAIL_USER_CHANGED = 'email_user_changed';
export const PASSWORD_CHANGED = 'password_changed';
export const CONFIRM_PASSWORD_CHANGED = 'confirm_password_changed';
export const USERNAME_CHANGED = 'username_changed';
export const PHONE_CHANGED = 'phone_changed';
export const LOGIN_USER_SUCCESS = 'login_user_sucess';
export const LOGIN_SOCAIL_USER_SUCCESS = 'login_social_user_sucess';
export const LOGIN_USER_FAIL = 'login_user_fail';
export const LOGIN_SOCIAL_USER_FAIL = 'login_social_user_fail';
export const LOGIN_USER = 'login_user';
export const LOGIN_SOCIAL_USER = 'login_social_user';
export const CHANGE_MESSAGE = 'show_message_changed';
export const CLEAR_MESSAGE = 'clear_message';
export const ENTER_NUMBER = 'enter_number';
export const GET_USER = 'get_user';
export const GET_USER_SUCCESS = 'get_user_success';
export const GET_USER_FAIL = 'get_user_fail';
export const EDIT_USER = 'edit_user';
export const EDIT_USER_SUCCESS = 'edit_user_success';
export const EDIT_USER_FAIL = 'edit_user_fail';
export const FORGET_EMAIL_CHANGED = 'forget_email_changed';
export const FORGET_USER = 'forget_user';
export const FORGET_USER_FAIL = 'forget_user_fail';
export const FORGET_USER_SUCCESS = 'forget_user_success';
export const REGISTER_USER = 'register_user';
export const REGISTER_USER_SUCCESS = 'register_user_success';
export const REGISTER_USER_FAIL = 'register_user_fail';
export const UPLOAD_PHOTO = 'upload_photo';
export const UPLOAD_PHOTO_SUCCESS = 'upload_photo_success';
export const UPLOAD_PHOTO_FAIL = 'upload_photo_fail';
export const CLEAR_USER = 'clear_user';
export const CLEAR_UPLOAD_PHOTO_STATUS = 'clear_upload_photo_status';



//OTHERS
export const CONTACT_FORM = 'contact_form';
export const CONTACT_FORM_SUCCESS = 'contact_form_success';
export const CONTACT_FORM_FAIL = 'contact_form_fail';
export const PAGES_FAIL = 'pages_fail';
export const PAGES_SUCCESS = 'pages_success';
export const GET_PAGE = 'get_page';
export const GET_HOMESCREEN = 'get_homeScreen';
export const GET_HOMESCREEN_FAIL = 'get_homeScreen_fail';
export const GET_HOMESCREEN_SUCCESS = 'get_homeScreen_success';
export const GET_SETTINGS = 'get_settings';
export const GET_SETTINGS_FAIL = 'get_settings_fail';
export const GET_SETTINGS_SUCCESS = 'get_settings_success';
export const GET_NOTIFICATIONS = 'get_notifications';
export const GET_NOTIFICATIONS_FAIL = 'get_notifications_fail';
export const GET_NOTIFICATIONS_SUCCESS = 'get_notifications_success';
export const GET_ADDRESSES = 'get_addresses';
export const GET_ADDRESSES_FAIL = 'get_addresses_fail';
export const GET_ADDRESSES_SUCCESS = 'get_addresses_success';
export const DELETE_ADDRESSES = 'delete_addresses';
export const DELETE_ADDRESSES_FAIL = 'delete_addresses_fail';
export const DELETE_ADDRESSES_SUCCESS = 'delete_addresses_success';
export const ADD_ADDRESS = 'add_address';
export const ADD_ADDRESS_FAIL = 'add_address_fail';
export const ADD_ADDRESS_SUCCESS = 'add_address_success';
export const SEARCH = 'search';
export const SEARCH_FAIL = 'search_fail';
export const SEARCH_SUCCESS = 'search_success';
export const FILTER = 'filter';
export const FILTER_FAIL = 'filter_fail';
export const FILTER_SUCCESS = 'filter_success';
export const ADD_COMMENT = 'add_comment';
export const ADD_COMMENT_FAIL = 'add_comment_fail';
export const ADD_COMMENT_SUCCESS = 'add_comment_success';
export const GET_COMMENTS = 'get_comments';
export const GET_COMMENTS_SUCCESS = 'get_comments_success';
export const GET_COMMENTS_FAIL = 'get_comments_fail';
export const GET_FILTER_OPTIONS = 'get_filter_options';
export const GET_FILTER_OPTIONS_SUCCESS = 'get_filter_options_success';
export const GET_FILTER_OPTIONS_FAIL = 'get_filter_options_fail';

//PRODUCTS
export const COUNT_CHANGED = 'count_changed';
export const GET_MAIN_CAT = 'get_main_cat';
export const GET_MAIN_CAT_FAIL = 'get_main_cat_fail';
export const GET_MAIN_CAT_SUCCESS = 'get_main_cat_success';
export const GET_CATS_BY_PARENT = 'get_cats_by_parent';
export const GET_CATS_BY_PARENT_FAIL = 'get_cats_by_parent_fail';
export const GET_CATS_BY_PARENT_SUCCESS = 'get_cats_by_parent_success';
export const GET_PRODUCTS_BY_CATID = 'get_product_by_catid';
export const GET_PRODUCTS_BY_CATID_FAIL = 'get_product_by_catid_fail';
export const GET_PRODUCTS_BY_CATID_SUCCESS = 'get_product_by_catid_success';
export const GET_PRODUCTS_BY_BRANDID = 'get_product_by_brandid';
export const GET_PRODUCTS_BY_BRANDID_FAIL = 'get_product_by_brandid_fail';
export const GET_PRODUCTS_BY_BRANDID_SUCCESS = 'get_product_by_brandid_success';
export const GET_PRODUCT = 'get_product';
export const GET_PRODUCT_FAIL = 'get_product_fail';
export const GET_PRODUCT_SUCCESS = 'get_product_success';
export const ADD_TO_WISHLIST = 'add_to_wishlist';
export const ADD_TO_WISHLIST_FAIL = 'add_to_wishlist_fail';
export const ADD_TO_WISHLIST_SUCCESS = 'add_to_wishlist_success';
export const DELETE_WISHLIST = 'delete_wishlist';
export const DELETE_WISHLIST_FAIL = 'delete_wishlist_fail';
export const DELETE_WISHLIST_SUCCESS = 'delete_wishlist_success';
export const GET_WISHLIST = 'get_wishlist';
export const GET_WISHLIST_FAIL = 'get_wishlist_fail';
export const GET_WISHLIST_SUCCESS = 'get_wishlist_success';
export const GET_DIS_PRODUCTS = 'get_dis_products';
export const GET_DIS_PRODUCTS_FAIL = 'get_dis_products_fail';
export const GET_DIS_PRODUCTS_SUCCESS = 'get_dis_products_success';
export const GET_BRAND_PRODUCTS = 'get_brand_products';
export const GET_BRAND_PRODUCTS_FAIL = 'get_brand_products_fail';
export const GET_BRAND_PRODUCTS_SUCCESS = 'get_brand_products_success';
export const GET_WHEEL_ITEMS_FORTUNE = 'get_wheel_items_fortune';
export const GET_WHEEL_ITEMS_FORTUNE_FAIL = 'get_wheel_items_fortune_fail';
export const GET_WHEEL_ITEMS_FORTUNE_SUCCESS = 'get_wheel_items_fortune_success';
export const GIFT_TO_USER = 'gift_to_user';
export const GIFT_TO_USER_FAIL =  'gift_to_user_fail';
export const GIFT_TO_USER_SUCCESS = 'gift_to_user_success';

//ORDERS
export const STATE_ADD_TO_CART = 'state_add_to_cart';
export const GET_SIZES = 'get_sizes';
export const GET_SIZES_FAIL = 'get_sizes_fail';
export const GET_SIZES_SUCCESS = 'get_sizes_success';
export const GET_ORDERS = 'get_orders';
export const GET_ORDERS_FAIL = 'get_orders_fail';
export const GET_ORDERS_SUCCESS = 'get_orders_success';
export const GET_ORDER = 'get_order';
export const GET_ORDER_FAIL = 'get_order_fail';
export const GET_ORDER_SUCCESS = 'get_order_success';
export const SAVE_ORDER = 'save_order';
export const SAVE_ORDER_FAIL = 'save_order_fail';
export const SAVE_ORDER_SUCCESS = 'save_order_success';
export const CHECK_COUPON = 'check_coupon';
export const CHECK_COUPON_FAIL = 'check_coupon_fail';
export const CHECK_COUPON_SUCCESS = 'check_coupon_success';
export const UPDATE_CART = 'update_cart';
export const UPDATE_CART_SUCCESS = 'update_cart_success';
export const CLEAR_CART = 'clear_cart';

//Currencies
export const GET_CURRENCIRES = "get_currencies";
export const GET_CURRENCIRES_SUCCESS = "get_currencies_success";
export const GET_CURRENCIRES_FAIL = "get_currencies_fail";

// slider brands

export const GET_SLIDERS_BY_BRAND = 'get_sliders_by_brand';
export const GET_SLIDERS_BY_BRAND_FAIL = 'get_sliders_by_brand_fail';
export const GET_SLIDERS_BY_BRAND_SUCCESS = 'get_sliders_by_brand_success';

// slider category

export const GET_SLIDERS_BY_CATEGORY = 'get_sliders_by_category';
export const GET_SLIDERS_BY_CATEGORY_FAIL = 'get_sliders_by_category_fail';
export const GET_SLIDERS_BY_CATEGORY_SUCCESS = 'get_sliders_by_category_success';

// videos
export const GET_VIDEOS = "get_videos";
export const GET_VIDEOS_SUCCESS = "get_videos_success";
export const GET_VIDEOS_FAIL = "get_videos_fail";