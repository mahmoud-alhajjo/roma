import React, { Component } from 'react';
import {
  Image,
  /*TouchableNativeFeedback,*/
  TouchableOpacity,
  TouchableHighlight,
  FlatList,
  BackHandler,
  StyleSheet
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { Spinner } from './assets/common';
import { L } from '../Config';
import styles, {
  verticalScale,
  scale,
  moderateScale,
  buttonStyle,
  buttonTextStyle,
  textContainer,
  solidText,
  inputField,
  primaryColor,
  secondaryColor,
  fontBold,
  DroidKufi,
  DroidKufiBold,
  brandImage,
} from './assets/styles/Style';
import {
  Container,
  Content,
  Header,
  Item,
  Input,
  Button,
  Text,
  View,
  Icon,
  Toast,
  Accordion
} from 'native-base';
import { pages, clearMessage, showMessageChanged } from '../actions';

const itemsWheelType = ['product', 'coupon']

class About extends Component {
  state = {
    searchField: '',
    pressedButton: false,
    giftsTokenArray: []
  };
  onButtonPress = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    // then navigate
    // navigate('NewScreen');
  };

  handleBackButton = () => {
    Actions.pop();
    return true;
  };
  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }
  _renderError() {
    if (this.props.message) {
      this.props.clearMessage();
      return Toast.show({
        text: this.props.message,
        buttonText: L('dismiss'),
        duration: 3000,
        style: { backgroundColor: primaryColor },
        textStyle: { fontFamily: DroidKufi, fontSize: moderateScale(4.5) },
        buttonTextStyle: { fontFamily: DroidKufi, fontSize: moderateScale(4.5) },
      });
    }
  }
  _renderLoading() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }
  }
  _renderHeader(item, expanded, cart) {
    let baseTitle = item.product ? item.product.title : item.coupon ? item.coupon.name : null
    // debugger
    if (cart && cart.length > 0 && item.product) {
      cart.map((obj) => {
        if (obj.product_id === item.product.id && obj.is_gift) {
          baseTitle = null
        }
      })
    }
    return (
      itemsWheelType.map(itemsWheel => (itemsWheel === item.type && baseTitle) ?
        <View style={[style2.accordionHeader]}>
          <View>
            <Text style={[style2.accordionHeaderTitle]}>
              {L(itemsWheel)}
            </Text>
            {baseTitle ? <Text style={[style2.accordionHeaderSubtitle]}>
              {baseTitle}
            </Text> : null}
          </View>
          {expanded
            ? <Icon style={{ fontSize: 18, color: "#9d0208" }} type="Entypo" name="chevron-up" />
            : <Icon style={{ fontSize: 18, color: "#2a9d8f" }} type="Entypo" name="chevron-down" />}
        </View> : null)
    )
  }

  _renderContent(item, state) {
    if (item.type === 'coupon') {
      return (
        <View style={{ paddingHorizontal: scale(1) }}>
          <Text style={[style2.testGifts, style2.textCoupon]}>
            {L('codeCoupon')} : {item.coupon.number}
          </Text>
          <Text style={[style2.testGifts, style2.textCoupon]}>
            {L('numberOfUse')} : {item.coupon.numberOfUse}
          </Text>
          <Text style={[style2.testGifts, style2.textCoupon]}>
            {L('restOfUsing')} : {item.coupon.numberOfUse - item.coupon.numberOfUsed}
          </Text>
          <Text style={[style2.testGifts]}>
            {L('ValidityIsFrom')} {item.coupon.start} {L('to')} {item.coupon.end}
          </Text>
        </View >
      )
    } else if (item.type === 'product') {
      return (
        <View style={{ paddingHorizontal: scale(1) }}>
          <Text style={[style2.testGifts]}>
            {L('abilityGift')}
          </Text>
          <Button
            style={[style2.buttonProduct, { backgroundColor: state.giftsTokenArray.includes(item.product.id) ? '#b2bec3' : primaryColor }]}
            rounded
            light
            disabled={state.giftsTokenArray.includes(item.product.id)}
            onPress={() => {
              Actions.push('product', { productID: item.product.id, title: item.product.title, giftProduct: true, giftId: item.id })
              this.setState({ giftsTokenArray:[...state.giftsTokenArray, item.product.id] })
            }}
          >
            <Text style={[style2.textButtonProduct]}>{L('viewProduct')}</Text>
          </Button>
        </View >
      )
    }
  }

  render() {
    return (
      <Container>
        <Header
          androidStatusBarColor={primaryColor}
          style={styles.routernavigationBarStyle}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignSelf: 'center',
              alignItems: 'center',
              width: scale(100),
            }}>
            <Button
              onPress={() => Actions.pop()}
              transparent
              /*background={TouchableNativeFeedback.Ripple(secondaryColor)}*/
              style={styles.headerBack}>
              <Icon
                name="keyboard-backspace"
                type="MaterialIcons"
                style={{
                  color: '#fff',
                  fontSize: 25,
                  transform: [{ scaleX: L('scaleLang') }],
                }}
              />
            </Button>
            <Text numberOfLines={1} style={styles.headerText}>
              {this.props.title}
            </Text>
          </View>
        </Header>
        <Content style={{ backgroundColor: '#f9f9f9' }}>
          <View>
            <Image
              source={require('./assets/images/about-bg.png')}
              resizeMode="cover"
              style={{
                width: scale(100),
                height: verticalScale(25),
              }}
            />
          </View>
          <View style={{ padding: moderateScale(5) }}>
            {this.props.pagesElement.content ? <Text style={styles.fontRegular}>
              {this.props.pagesElement.content}
            </Text>
              :
              this.props.myPoints ?
                <View>
                  <Text style={styles.fontRegular}>
                    {L('youHavePoints')} : {this.props.pagesElement.points} {L('point')}
                  </Text>
                  <Text style={styles.fontRegular}>
                    {L('faresDeliveryPoint')} : 25 {L('point')}
                  </Text>
                  <Text style={styles.fontRegular}>
                    {L('pointswaiting')} : {this.props.pagesElement.pending_points} {L('point')}
                  </Text>
                </View>
                :
                <View>
                  <Accordion
                    dataArray={this.props.pagesElement.gifts}
                    renderHeader={(item, expanded) => this._renderHeader(item, expanded, this.props.cart)}
                    renderContent={(item) => this._renderContent(item, this.state)}
                    style={{ flex: 1, backgroundColor: '#f5f6fa' }}
                  />
                </View>
            }
          </View>
        </Content>
        {this._renderError()}
        {this._renderLoading()}
      </Container>
    );
  }
}
const style2 = StyleSheet.create({
  accordionHeader: {
    flexDirection: L('textAlignRight') === 'right' ? 'row' : 'row-reverse',
    padding: 10,
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "#f9f9f9"
  },
  accordionHeaderTitle: {
    fontFamily: DroidKufi,
    fontSize: moderateScale(6),
    color: primaryColor,
    textAlign: L('textAlignRight') === 'right' ? 'left' : 'right',
  },
  accordionHeaderSubtitle: {
    paddingHorizontal: 5,
    textAlign: L('textAlignRight') === 'right' ? 'left' : 'right',
    fontFamily: DroidKufi, fontSize: moderateScale(3.5),
    color: '#686e7f'
  },
  testGifts: {
    padding: scale(4),
    fontFamily: DroidKufi,
    fontSize: moderateScale(4.8),
    color: '#1d3557'
  },
  textCoupon: {
    borderBottomWidth: 1,
    borderBottomColor: '#ddd',
    paddingHorizontal: 0,
    marginHorizontal: scale(4)
  },
  buttonProduct: {
    width: scale(28),
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: L('textAlignRight') === 'right' ? 'flex-end' : 'flex-start',
    margin: scale(2),
    marginBottom: scale(4),
    backgroundColor: primaryColor
  },
  textButtonProduct: {
    fontFamily: DroidKufi,
    fontSize: moderateScale(4),
    color: '#fff',
    textAlign: 'center',
    alignSelf: 'center'
  }
});

const mapStateToProps = ({ productsR, ordersR }) => {
  const { cart } = ordersR;
  const { catsByParent, loading, message } = productsR;
  // console.log(catsByParent);
  return { loading, message, catsByParent, cart };
};
export default connect(mapStateToProps, {
  pages,
  clearMessage,
  showMessageChanged,
})(About);
