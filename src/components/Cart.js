import React, { Component } from 'react';
import {
  Image,
  FlatList,
  Alert,
  TouchableOpacity,
  BackHandler,
  StyleSheet
} from 'react-native';
import { connect } from 'react-redux';
import { Spinner } from './assets/common';
import { L } from '../Config';
import styles, {
  verticalScale,
  scale,
  buttonStyle,
  buttonTextStyle,
  moderateScale,
  inputField,
  primaryColor,
  fontBold,
  DroidKufi,
  DroidKufiBold,
} from './assets/styles/Style';
import AsyncStorage from '@react-native-community/async-storage';
import {
  Container,
  Content,
  Header,
  List,
  ListItem,
  Button,
  Text,
  View,
  Icon,
  Toast,
  Thumbnail,
  Right,
  Left,
  Body,
  Footer,
} from 'native-base';
import { AirbnbRating } from 'react-native-ratings';
import {
  getSizes,
  getSettings,
  updateCartQty,
  clearMessage,
  showMessageChanged,
  clearCart, getPrice,
  addToCart
} from '../actions';
import { Actions } from 'react-native-router-flux';
import ColorPalette from 'react-native-color-palette'
import { colors } from 'react-native-elements';

class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      language: 'ar',
      // cart: '',
      user: null,
      showListColor: false,
      idItemListColor: '',
      cart: (this.props.cart && this.props.cart.length > 0) ? this.props.cart : [],
      editCart: false,
    }
    this.navigationWillFocusListener = this.props.navigation.addListener('willBlur', () => {
      this.setState({ editCart: false });
    })
  }
  onButtonPress = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    // then navigate
    // navigate('NewScreen');
  };

  userNeedToCompleteHisProfile = (user) => {
    if (user.name == null || user.name.length === 0 ||
      user.email == null || user.email.length === 0 ||
      user.phone == null || user.phone.length === 0) {
      return true;
    }
    return false;
  };

  handleBackButton = () => {
    BackHandler.exitApp();
    return true;
  };
  async componentDidMount() {
    try {
      let value = await AsyncStorage.getItem('user');
      // console.log(value);
      const user = JSON.parse(value);
      if (user) {
        this.setState({ user: user });
        // this._getData();
      }
      // console.log(user);
      this._getData();
    } catch (error) {
      // console.log(error);
      // this._getData()
    }
  }
  _getData() {
    // console.log(this.state.user);
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    this.props.getSettings();
  }
  componentWillUnmount() {
    this.setState({ editCart: false })
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }
  _renderError() {
    if (this.props.updateCart) {
      this.forceUpdate();
      this.props.clearMessage();
    }
    if (this.props.message) {
      this.props.clearMessage();
      return Toast.show({
        text: this.props.message,
        buttonText: L('dismiss'),
        duration: 3000,
        style: { backgroundColor: primaryColor },
        textStyle: { fontFamily: DroidKufi, fontSize: moderateScale(4.5) },
        buttonTextStyle: { fontFamily: DroidKufi, fontSize: moderateScale(4.5) },
      });
    }
  }
  _renderLoading() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }
  }
  updateCart(item, option, selectedColor) {
    var cart = this.state.cart;
    this.props.updateCartQty(item.id, option, cart, selectedColor);
    this.setState({ editCart: true })
  }
  onClearCart() {
    this.props.clearCart();
    Actions.jump('home');
  }
  _renderItem = item => {
    item = item.item;
    // var sizes = this.props.sizes
    const selectedColorName = item.colors.filter(color => {
      return color.id == item.selected_color;
    });
    var selectedSizeR = item.quantityPerSize.filter(size => {
      return size.size_id == item.size_right;
    });
    var selectedSizeL = item.quantityPerSize.filter(size => {
      return size.size_id == item.size_left;
    });
    return (
      <View>
        <View
          style={{
            flexDirection: 'row',
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.23,
            shadowRadius: 2.62,
            elevation: 4,
            // alignItems: 'center',
            alignSelf: 'center',
            marginVertical: verticalScale(1.5),
            width: scale(90),
            minHeight: verticalScale(10),
            backgroundColor: '#fff',
            borderRadius: moderateScale(5),
            padding: scale(5),
          }}>
          {item.is_offer &&
            <View style={style2.stripNewProduct}>
              <Text style={style2.textNewProduct}>{item.quantity} {L('free')}</Text>
              <View style={style2.triangleCorner}></View>
            </View>}
          {item.is_gift &&
            <View style={style2.stripNewProduct}>
              <Text style={style2.textNewProduct}>{L('gift')}</Text>
              <View style={[style2.triangleCorner, { borderTopColor: '#f9c74f' }]}></View>
            </View>}
          <View style={{ flexDirection: 'column', alignItems: 'center' }}>
            <View
              style={{
                backgroundColor: '#fff',
                width: scale(24),
                height: verticalScale(12),
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 1,
                },
                shadowOpacity: 0.2,
                shadowRadius: 1.41,
                elevation: 2,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
                marginBottom: verticalScale(1),
              }}>
              <Thumbnail square source={{ uri: item.photos[0].thumb }} />
            </View>
            <AirbnbRating
              showRating={false}
              isDisabled
              defaultRating={item.rating}
              ratingColor="#f4b700"
              size={10}
              starContainerStyle={{ marginLeft: 0 }}
            />
          </View>
          <View
            style={{
              width: scale(54),
              // borderWidth: 1,
              // borderColor: '#0f0',
              marginLeft: 10,
            }}>
            <Text
              numberOfLines={1}
              style={[
                styles.fontRegular,
                {
                  color: primaryColor,
                  fontSize: moderateScale(6),
                  marginBottom: verticalScale(1),
                  textAlign: 'left',
                },
              ]}>
              {item.title}
            </Text>
            <View style={{ flexDirection: 'row' }}>
              <Text
                style={[
                  styles.fontRegular,
                  { fontSize: moderateScale(4.3), color: '#333' },
                ]}>
                {L('qqty')}
              </Text>
              <Text
                style={[
                  styles.fontRegular,
                  {
                    fontSize: moderateScale(4.3),
                    borderWidth: 1,
                    borderColor: '#ddd',
                    width: scale(15),
                    textAlign: 'center',
                    marginHorizontal: scale(1),
                    color: '#828282',
                    borderRadius: moderateScale(5),
                  },
                ]}>
                {item.quantity}
              </Text>

              {(item.is_offer || (item.is_gift)) ? null : <View style={{ flexDirection: 'row' }}>
                <TouchableOpacity
                  style={{ borderColor: 'rgb(237,80,64)' }}
                  onPress={() => this.updateCart(item, 'plus')}>
                  <Icon
                    name="chevron-up"
                    type="Feather"
                    style={{
                      fontSize: moderateScale(7),
                    }}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ borderColor: 'rgb(237,80,64)' }}
                  onPress={() => this.updateCart(item, 'minus')}>
                  <Icon
                    name="chevron-down"
                    type="Feather"
                    style={{
                      fontSize: moderateScale(7),
                    }}
                  />
                </TouchableOpacity>
              </View>}

              {/* <Text
              style={[
                styles.fontRegular,
                {fontSize: moderateScale(4.3), color: '#333'},
              ]}>
              {L('eyeColor')}
            </Text>
            <Text
              style={[
                styles.fontRegular,
                {
                  fontSize: moderateScale(4.3),
                  borderWidth: 1,
                  borderColor: '#ddd',
                  width: scale(15),
                  textAlign: 'center',
                  marginHorizontal: scale(1),
                  color: '#828282',
                  borderRadius: moderateScale(5),
                },
              ]}>
              {selectedColorName[0] && selectedColorName[0].name}
            </Text> */}
            </View>

            {selectedSizeR[0] && selectedSizeR[0].size != 0 ? (
              <View
                style={{ flexDirection: 'row', marginVertical: verticalScale(1) }}>
                <Text
                  style={[
                    styles.fontRegular,
                    { fontSize: moderateScale(4.3), color: '#333' },
                  ]}>
                  {L('eye')}
                </Text>
                <Text
                  style={[
                    styles.fontRegular,
                    {
                      fontSize: moderateScale(4.3),
                      color: '#828282',
                      borderWidth: 1,
                      borderColor: '#ddd',
                      width: scale(15),
                      textAlign: 'center',
                      marginHorizontal: scale(1),
                      borderRadius: moderateScale(5),
                    },
                  ]}>
                  {L('right')}
                </Text>
                <Text
                  style={[
                    styles.fontRegular,
                    {
                      fontSize: moderateScale(4.3),
                      color: '#333',
                      marginHorizontal: scale(1),
                    },
                  ]}>
                  {L('size')}
                </Text>
                <Text
                  style={[
                    styles.fontRegular,
                    {
                      fontSize: moderateScale(4.3),
                      borderWidth: 1,
                      borderColor: '#ddd',
                      width: scale(15),
                      textAlign: 'center',
                      color: '#828282',
                      borderRadius: moderateScale(5),
                    },
                  ]}>
                  {selectedSizeR[0] && selectedSizeR[0].size}
                </Text>
              </View>
            ) : null}
            {selectedSizeL[0] && selectedSizeL[0].size != 0 ? (
              <View
                style={{ flexDirection: 'row', marginVertical: verticalScale(1) }}>
                <Text
                  style={[
                    styles.fontRegular,
                    { fontSize: moderateScale(4.3), color: '#333' },
                  ]}>
                  {L('eye')}
                </Text>
                <Text
                  style={[
                    styles.fontRegular,
                    {
                      fontSize: moderateScale(4.3),
                      color: '#828282',
                      borderWidth: 1,
                      borderColor: '#ddd',
                      width: scale(15),
                      textAlign: 'center',
                      marginHorizontal: scale(1),
                      borderRadius: moderateScale(5),
                    },
                  ]}>
                  {L('left')}
                </Text>
                <Text
                  style={[
                    styles.fontRegular,
                    {
                      fontSize: moderateScale(4.3),
                      color: '#333',
                      marginHorizontal: scale(1),
                    },
                  ]}>
                  {L('size')}
                </Text>
                <Text
                  style={[
                    styles.fontRegular,
                    {
                      fontSize: moderateScale(4.3),
                      borderWidth: 1,
                      borderColor: '#ddd',
                      width: scale(15),
                      textAlign: 'center',
                      color: '#828282',
                      borderRadius: moderateScale(5),
                    },
                  ]}>
                  {selectedSizeL[0] && selectedSizeL[0].size}
                </Text>
              </View>
            ) : null}

            {(item.is_offer || (item.is_gift)) ? null : <Text
              style={[
                styles.fontRegular,
                { fontSize: moderateScale(8), color: '#bd3030' },
              ]}>

              {getPrice((item.newPrice +
                (item.Rsize_additional_price ? Number(item.Rsize_additional_price) : 0)
                + (item.Lsize_additional_price ? Number(item.Lsize_additional_price) : 0)) * item.quantity)} {L(global.currency.code)}
            </Text>}
            <View style={{ flexDirection: 'row-reverse', alignItems: 'center' }}>
              {(item.offer && item.price === 0) ? null : <TouchableOpacity
                style={{ marginLeft: scale(4), borderColor: 'rgb(237,80,64)', width: scale(8), height: verticalScale(3) }}
                onPress={() => this.updateCart(item, 'delete')}>
                <Icon
                  name="trash"
                  type="Feather"
                  style={{
                    fontSize: moderateScale(7),
                  }}
                />
              </TouchableOpacity>}
              {(item.colors_with_photos.length > 0) ? <TouchableOpacity
                style={{ borderColor: 'rgb(237,80,64)', width: scale(12), height: verticalScale(3) }}
                onPress={() => {
                  this.setState({ showListColor: this.state.idItemListColor === item.id ? !this.state.showListColor : true, idItemListColor: item.id })
                }}>
                <Icon
                  name="color-lens"
                  type="MaterialIcons"
                  style={{
                    fontSize: moderateScale(7.5),
                  }}
                />
              </TouchableOpacity> : null}
            </View>
            {(item.colors_with_photos.length > 0 && this.state.showListColor && this.state.idItemListColor === item.id) ?
              <ColorPalette
                onChange={color => this.updateCart(item, 'changeColor', color)}
                defaultColor={item.selected_color}
                colors={item.colors_with_photos.map(color => color.color)}
                title={""}
                icon={<Text>✔</Text>}
              /> : null
            }
          </View>
        </View>
        {((item.category.offer && (item.category.offer.condition > 0 && item.chosenOfferItemnumber > 0)) ||
          (item.brand && (item.brand.offer && (item.brand.offer.condition > 0 && item.chosenOfferItemnumber > 0)))) && <Button
            style={[styles.buttonStyle, { width: scale(55), backgroundColor: '#7d7d7d', marginBottom: verticalScale(1.2) }]}
            onPress={() => {
              if (item.brand && item.brand.offer) {
                if (item.brand.categoriesNumber > 0) {
                  Actions.push('catsPage', { title: item.brand.name, brandId: item.brand.id, discount: true, chosenOfferItem: true })
                } else {
                  Actions.push('productsPage', { title: item.brand.name, brandId: item.brand.id, discount: true, chosenOfferItem: true })
                }
              } else if (item.category && item.category.offer) {
                Actions.push('productsPage', { title: item.category.title, catId: item.category.id, discount: true, chosenOfferItem: true })
              }
            }}
          >
            <Text style={styles.buttonTextStyle}>
              {item.category.offer ?
                `${L('offerChoose')} ${item.chosenOfferItemnumber} ${L('productFrom')} ${item.category.title}` :
                `${L('offerChoose')} ${item.chosenOfferItemnumber} ${L('productFrom')} ${item.brand.name}`
              }
            </Text>
          </Button>}
      </View>
    );
  };
  UNSAFE_componentWillReceiveProps(nextProps, nextState) {
    if (nextProps.cart.length > 0) {
      this.setState(() => { console.log('editCart1', this.state.editCart) },
        () => {
          console.log('editCart2', this.state.editCart)
          this.state.editCart ? this.setState({
            cart: addOffersEdit(nextProps.cart.filter((item) => item.price > 0))
          }) :
            this.setState({
              cart: addOffers(nextProps.cart.filter((item) => item.price > 0))
            })
        })
    } else {
      this.setState({ cart: [] })
    }
  }

  handleCheckout(finalCart) {
    if (this.state.user == null) {
      Alert.alert(
        L('alert'),
        L('mustLogin'),
        [
          // { text: 'Ask me later', onPress: () => console.log('Ask me later pressed') },
          {
            text: L('dontRegister'),
            // onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          { text: L('registerNow'), onPress: () => Actions.reset('auth') },
        ],
        { cancelable: false },
      );
    } else if (this.userNeedToCompleteHisProfile(this.state.user)) {
      Alert.alert(
        L('alert'),
        L('mustContinueProfile'),
        [
          // { text: 'Ask me later', onPress: () => console.log('Ask me later pressed') },
          {
            text: L('dontCompleteProfile'),
            // onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          { text: L('completeProfile'), onPress: () => Actions.push('editProfile') },
        ],
        { cancelable: false },
      );
    } else {
      this.props.addToCart(finalCart);
      Actions.push('payment', { title: L('checkout') });
    }
  }
  render() {
    var cart = this.state.cart;
    var delivery = this.props.delivery;
    let finalCart = cart;
    if (global.currency && global.currency.delivery) {
      delivery = global.currency.delivery;
    }
    var vat = this.props.vat;
    if (global.currency && global.currency.vat) {
      vat = global.currency.vat;
    }
    var vatCalc = Number((vat * delivery) / 100);
    // console.log(vatCalc);
    if (cart.length) {
      total = 0;
      totalWithDelivery = 0;
      for (var i = 0; i < cart.length; i++) {
        if (!cart[i].is_gift && !cart[i].is_offer) {
          var total = Number(total) +
            (Number(cart[i].quantity) *
              (Number(cart[i].newPrice) +
                (cart[i].Rsize_additional_price ? Number(cart[i].Rsize_additional_price) : 0)
                + (cart[i].Lsize_additional_price ? Number(cart[i].Lsize_additional_price) : 0)));
        }
        /*if (cart[i].size_right == 70 && cart[i].size_left) {
          var total =
            Number(total) + Number(cart[i].quantity) * Number(cart[i].newPrice);
        } else {
          var total =
            Number(total) +
            Number(cart[i].quantity) * Number(cart[i].newPrice) +
            Number(30);
        }*/
        var totalWithDelivery =
          Number(total) + Number(delivery) + Number(vatCalc);
      }
    }
    return (
      <Container>
        <Header
          androidStatusBarColor={primaryColor}
          style={styles.routernavigationBarStyle}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              alignSelf: 'center',
              alignItems: 'center',
              width: scale(100),
            }}>
            <Text style={[styles.fontRegular, { color: '#fff', fontSize: 14 }]}>
              {L('cart')}
            </Text>
          </View>
        </Header>
        <Content padder style={{ height: verticalScale(90) }}>
          {/* <List> */}
          {this.props.cart && this.props.cart.length > 0 ? (
            <Text
              style={[
                styles.fontRegular,
                {
                  backgroundColor: 'rgba(140, 74, 164, 0.5)',
                  padding: scale(2),
                  textAlign: 'center',
                  borderRadius: scale(2),
                  color: '#FFF',
                },
              ]}>
              {this.props.cartMessage}
            </Text>
          ) : null}
          {cart && cart.length > 0 ? (
            <FlatList
              data={finalCart}
              renderItem={this._renderItem}
              horizontal={false}
              // numColumns={2}
              keyExtractor={(item, index) => index.toString()}
              extraData={this.state}
              // columnWrapperStyle={{ justifyContent: 'space-between' }}
              initialNumToRender={4}
            />
          ) : (
              <View
                style={{
                  alignSelf: 'center',
                  justifyContent: 'center',
                  height: verticalScale(50),
                }}>
                <View
                  style={{
                    backgroundColor: '#f9f9f9',
                    alignSelf: 'center',
                    width: scale(60),
                    height: verticalScale(32),
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: moderateScale(100),
                    borderWidth: 0.3,
                    borderColor: primaryColor,
                  }}>
                  <Icon
                    name="shopping-cart"
                    type="Feather"
                    style={{
                      fontSize: moderateScale(35),
                      color: '#ddd',
                    }}
                  />
                </View>
                <Text
                  style={[
                    styles.fontRegular,
                    { textAlign: 'center', marginTop: verticalScale(3) },
                  ]}>
                  {L('cart')} {L('empty')}
                </Text>
              </View>
            )}
          {/* </List> */}
        </Content>
        {this.props.cart.length > 0 ? (
          <Footer
            style={{
              paddingTop: verticalScale(1),
              minHeight: verticalScale(34),
              alignSelf: 'center',
              alignItems: 'center',
              // marginBottom: verticalScale(11),
              flexDirection: 'column',
              backgroundColor: '#fff',
              borderTopWidth: 1,
              borderTopColor: '#f9f9f9',
              width: scale(100),
              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.23,
              shadowRadius: 2.62,
              elevation: 4,
            }}>
            <View
              style={{
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.23,
                shadowRadius: 2.62,
                elevation: 4,
                backgroundColor: '#fff',
                width: scale(90),
                flexDirection: 'column',
                paddingHorizontal: scale(5),
                borderRadius: moderateScale(5),
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  borderBottomColor: '#ddd',
                  borderBottomWidth: 1,
                  paddingVertical: verticalScale(1.3),
                }}>
                <Text style={styles.fontRegular}>{L('sum')}</Text>
                <Text style={styles.fontRegular}>
                  {getPrice(total)} {L(global.currency.code)}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  borderBottomColor: '#ddd',
                  borderBottomWidth: 1,
                  paddingVertical: verticalScale(1.3),
                }}>
                <Text style={styles.fontRegular}>{L('delivery')}</Text>
                <Text style={styles.fontRegular}>
                  {getPrice(delivery)} {L(global.currency.code)}
                </Text>
              </View>
              {/* <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  borderBottomColor: '#ddd',
                  borderBottomWidth: 1,
                  paddingVertical: verticalScale(1.3),
                }}>
                <Text style={styles.fontRegular}>{L('VAT')}</Text>
                <Text style={styles.fontRegular}>{this.props.vat} %</Text>
              </View> */}
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingVertical: verticalScale(1.3),
                }}>
                <Text style={[styles.fontRegular, { color: primaryColor }]}>
                  {L('total')}
                </Text>
                <Text style={[styles.fontRegular, { color: primaryColor }]}>
                  {' '}
                  {getPrice(totalWithDelivery)} {L(global.currency.code)}
                </Text>
              </View>
            </View>
            <View
              style={{
                marginVertical: verticalScale(1),
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: scale(90),
              }}>
              <Button
                style={[styles.buttonStyle, { width: scale(43) }]}
                onPress={() =>
                  Alert.alert(
                    L('clearCart'),
                    L('clearCartMsg'),
                    [
                      { text: L('no'), style: 'cancel' },
                      { text: L('yes'), onPress: () => this.onClearCart() },
                    ],
                    { cancelable: false },
                  )
                }>
                <Text style={styles.buttonTextStyle}>{L('clearCart')}</Text>
              </Button>
              <Button
                style={[styles.buttonStyle, { width: scale(43) }]}
                onPress={() => this.handleCheckout(finalCart)}>
                <Text style={styles.buttonTextStyle}>
                  {L('continuePayment')}
                </Text>
              </Button>
            </View>
          </Footer>
        ) : null}
        {this._renderError()}
        {this._renderLoading()}
      </Container>
    );
  }
}

const style2 = StyleSheet.create({
  stripNewProduct: {
    position: 'absolute',
    top: 0,
    right: 0,
    width: 80,
    height: 80
  },
  triangleCorner: {
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderLeftWidth: 80,
    borderTopWidth: 80,
    borderLeftColor: 'transparent',
    borderTopColor: '#c44569',
    borderTopRightRadius: moderateScale(5),
  },
  textNewProduct: {
    position: 'absolute',
    top: 20,
    right: -5,
    width: 60,
    transform: [{ rotate: '315deg' }],
    color: '#ffffff',
    fontFamily: DroidKufi,
    fontSize: moderateScale(4.5),
    zIndex: 100,
    textAlign: 'center',
  }
});


const mapStateToProps = ({ ordersR, others }) => {
  // const { loading, message } = auth;
  const { cart, loading, message, sizes, updateCart } = ordersR;
  const { delivery, vat, cartMessage } = others;
  // console.log(cart);
  return {
    loading,
    message,
    cart,
    sizes,
    delivery,
    updateCart,
    vat,
    cartMessage,
  };
};
export default connect(mapStateToProps, {
  addToCart,
  getSizes,
  getSettings,
  updateCartQty,
  clearMessage,
  showMessageChanged,
  clearCart,
})(Cart);

// Helpers
const addOffers = (cart = []) => {
  // console.log('cart', cart)
  const itemsMapForBrand = {};
  const itemsMapForCategory = {};
  const itemWithoutCategoryOffer = cart.filter(item => (!item.category.offer && (item.brand && !item.brand.offer)) || (!item.category.offer && item.brand_id === 0))
  cart.map((item, index) => {
    if (item.brand && item.brand.offer && item.brand.offer.condition > 0) {
      if (item.brand.id in itemsMapForBrand) {
        let items = itemsMapForBrand[item.brand.id];
        items.push(item);
      } else {
        itemsMapForBrand[item.brand.id] = [item];
      }
    }
    if (item.category.offer && item.category.offer.condition > 0) {
      if (item.category.id in itemsMapForCategory) {
        let items = itemsMapForCategory[item.category.id];
        items.push(item);
      } else {
        itemsMapForCategory[item.category.id] = [item];
      }
    }
  })
  // console.log('itemsMapForCategory', itemsMapForCategory)
  // console.log('itemsMapForBrand', itemsMapForBrand)
  // console.log('itemWithoutCategoryOffer', itemWithoutCategoryOffer)

  return [
    ...itemWithoutCategoryOffer,
    ...extractOfferAndDivideByType(itemsMapForBrand, 'brand'),
    ...extractOfferAndDivideByType(itemsMapForCategory, 'category')
  ];
}

function extractOfferAndDivideByType(itemsMap, type) {
  let cartAddedOffer = []
  if (Object.keys(itemsMap).length !== 0) {
    Object.keys(itemsMap).map((key) => {
      let items = [...itemsMap[key]];
      let quantity = 0;

      items.map((item) => {
        if (item.is_gift) {
          quantity -= item.quantity;
        }
        quantity += item.quantity;
      });

      // console.log('items', items);
      const itemsBrand = calculateOfferItems(items, quantity, type);
      // console.log('itemBrands', itemsBrand);
      cartAddedOffer.push(...itemsBrand)
    })
  }
  return cartAddedOffer
}

const calculateOfferItems = (brandItems, quantity, type) => {
  const brandOffer = type === 'category' ? brandItems[0].category.offer : brandItems[0].brand.offer;
  const { offerCount, restCount, quantity: newQuantity } = getOfferFromQuantity(quantity, brandOffer);
  let brandOfferItems = [];

  let restOfferCount = offerCount;
  let restQuantity = newQuantity;

  if (newQuantity >= brandOffer.condition) {
    for (let i = brandItems.length - 1; i >= 0; i--) {
      const brandItem = brandItems[i];
      if(brandItem.is_gift) {
        brandOfferItems.push({
          ...brandItem,
        });
        continue
      }
      const itemQuantity = brandItem.quantity;


      if (itemQuantity === restOfferCount) { // First Case
        // console.log('First Case')

        if (restOfferCount > 0) {
          brandOfferItems.push({
            ...brandItem,
            is_offer: true,
          });
        }
        restOfferCount = 0;
      } else if (itemQuantity > restOfferCount) { // Secound Case
        // console.log('Secound Case')

        // Add offer items
        if (restOfferCount > 0) {
          brandOfferItems.push({
            ...brandItem,
            is_offer: true,
            id: new Date().getTime(),
            quantity: restOfferCount,
          });
        }

        // Add non-offer items
        if (0 === offerCount) {
          brandOfferItems.push({
            ...brandItem,
            is_offer: false,
            quantity: itemQuantity - restOfferCount,
          });
        } else {
          brandOfferItems.push({
            ...brandItem,
            is_offer: false,
            quantity: itemQuantity - restOfferCount,
          });
        }
        restQuantity = restQuantity - itemQuantity - restOfferCount;
        restOfferCount = 0;
      } else if (itemQuantity < offerCount) { // Third Case
        // console.log('Third Case')

        // Add offer items
        if (restOfferCount > 0) {
          brandOfferItems.push({
            ...brandItem,
            is_offer: true,
            id: new Date().getTime(),
          });
        }
        restOfferCount = restOfferCount - itemQuantity;
      }
    }
  } else {
    brandOfferItems.push(...brandItems.filter(item => !item.is_offer));
  }

  // Add rest button to last item
  brandOfferItems[0].chosenOfferItemnumber = restCount;

  return brandOfferItems.reverse();
}

const getOfferFromQuantity = (quantity, offer) => {
  const { condition, result } = offer;
  let loopCount = result > 1 ? quantity - 1 : quantity;
  let newQuantity = quantity;
  let restCount = 0;
  let offerCount = 0;
  let count = 0;
  while (loopCount > 0) {

    if (count === condition) {
      newQuantity -= result;

      offerCount += result;

      count = 0;
      if (newQuantity < offerCount) {
        newQuantity += result;

        offerCount -= result;

        count++;
      }
    } else {
      count++;

    }
    loopCount--;
  }

  if (newQuantity >= condition && (Math.floor(newQuantity / condition) * result) >= offerCount) {
    restCount = (Math.floor(newQuantity / condition) * result) - offerCount;
  }

  if (offerCount > (Math.floor(newQuantity / condition) * result)) {
    offerCount = offerCount - (Math.floor(newQuantity / condition) * result)
    newQuantity = newQuantity + offerCount
    restCount = (Math.floor(newQuantity / condition) * result) - offerCount;
  }

  return {
    offerCount,
    restCount,
    quantity: newQuantity,
  }
}

const getOfferItemNumber = (quantity, offer) => Math.floor(quantity / offer.condition) * offer.result;


const addOffersEdit = (cart = []) => {
  // console.log('cart', cart)
  const itemsMapForBrand = {};
  const itemsMapForCategory = {};
  const itemWithoutCategoryOffer = cart.filter(item => (!item.category.offer && (item.brand && !item.brand.offer)) || (!item.category.offer && item.brand_id === 0))
  cart.map((item, index) => {
    if (item.brand && item.brand.offer && item.brand.offer.condition > 0) {
      if (item.brand.id in itemsMapForBrand) {
        let items = itemsMapForBrand[item.brand.id];
        items.push(item);
      } else {
        itemsMapForBrand[item.brand.id] = [item];
      }
    }
    if (item.category.offer && item.category.offer.condition > 0) {
      if (item.category.id in itemsMapForCategory) {
        let items = itemsMapForCategory[item.category.id];
        items.push(item);
      } else {
        itemsMapForCategory[item.category.id] = [item];
      }
    }
  })
  // console.log('itemsMapForCategory', itemsMapForCategory)
  // console.log('itemsMapForBrand', itemsMapForBrand)
  // console.log('itemWithoutCategoryOffer', itemWithoutCategoryOffer)

  return [
    ...itemWithoutCategoryOffer,
    ...extractOfferAndDivideByType_Edit(itemsMapForBrand, 'brand'),
    ...extractOfferAndDivideByType_Edit(itemsMapForCategory, 'category')
  ];
}

const extractOfferAndDivideByType_Edit = (itemsMap, type) => {
  let cartAddedOffer = []
  if (Object.keys(itemsMap).length !== 0) {
    Object.keys(itemsMap).map((key) => {
      let items = itemsMap[key];
      const offer = type === 'category' ? items[0].category.offer : items[0].brand.offer
      let quantity = 0;
      let chosenOfferItemnumber = 0
      items.map((item) => {
        if (item.is_offer) {
          chosenOfferItemnumber += item.quantity
          quantity -= item.quantity;
        }
        if (item.is_gift) {
          quantity -= item.quantity;
        }
        quantity += item.quantity;
      });

      let typeOfferItems = items.filter((item) => item.is_offer)
      let quantityOfferItems = 0;
      typeOfferItems.map((item) => {
        quantityOfferItems += item.quantity;
      });

      if (quantityOfferItems > (Math.floor(quantity / offer.condition) * offer.result)) {
        let numerOfItemDeleted = quantityOfferItems - Math.floor(quantity / offer.condition) * offer.result
        // console.log('numerOfItemDeleted', numerOfItemDeleted)
        for (let i = 0; i < numerOfItemDeleted; i++) {
          typeOfferItems[typeOfferItems.length - 1].quantity -= numerOfItemDeleted
          if (typeOfferItems[typeOfferItems.length - 1].quantity === 0) {
            typeOfferItems.pop()
          }
        }
      }

      let typeItemsWithoutOffer = type === 'category' ?
        items.filter((item) => !item.is_offer && item.category.id === items[0].category.id)
        : items.filter((item) => !item.is_offer && item.brand && item.brand.id === items[0].brand.id)

      if (quantity >= offer.condition) {
        if (typeOfferItems.length > 0) {
          typeOfferItems = typeOfferItems.map((item, index) => {
            if (index === typeOfferItems.length - 1) {
              return { ...item, chosenOfferItemnumber: (Math.floor(quantity / offer.condition) * offer.result) - chosenOfferItemnumber }
            } else {
              return item
            }
          })
        } else if (typeItemsWithoutOffer.length > 0) {
          typeItemsWithoutOffer = typeItemsWithoutOffer.map((item, index) => {
            if (index === typeItemsWithoutOffer.length - 1) {
              return { ...item, chosenOfferItemnumber: (Math.floor(quantity / offer.condition) * offer.result) - chosenOfferItemnumber }
            } else {
              return item
            }
          })
        }
      } else {
        typeItemsWithoutOffer = typeItemsWithoutOffer.map((item, index) => {
          if (index === typeItemsWithoutOffer.length - 1) {
            return { ...item, chosenOfferItemnumber: 0 }
          } else {
            return item
          }
        })
      }

      cartAddedOffer.push(...typeItemsWithoutOffer, ...typeOfferItems)
    })
  }
  return cartAddedOffer
}
