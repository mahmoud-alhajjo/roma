import React, {Component} from 'react';
import {
    Image,
    /*TouchableNativeFeedback,*/
    TouchableWithoutFeedback,
    FlatList,
    BackHandler, Linking, TouchableOpacity, Dimensions
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {Spinner} from './assets/common';
import {L} from '../Config';
import styles, {
  verticalScale,
  scale,
  moderateScale,
  buttonStyle,
  buttonTextStyle,
  textContainer,
  solidText,
  inputField,
  primaryColor,
  secondaryColor,
  fontBold,
  DroidKufi,
  DroidKufiBold,
  brandImage,
} from './assets/styles/Style';
import {
  Container,
  Content,
  Header,
  Item,
  Input,
  Button,
  Text,
  View,
  Icon,
  Toast,
} from 'native-base';
import {AirbnbRating} from 'react-native-ratings';
import FastImage from 'react-native-fast-image';
import {
    getMainCat
} from '../actions';
import Carousel from "react-native-snap-carousel/src/carousel/Carousel";
const {width: screenWidth} = Dimensions.get('window');

class CategoryPage extends Component {
  state = {
    searchField: '',
  };
  onButtonPress = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    // then navigate
    // navigate('NewScreen');
  };

  handleBackButton = () => {
    Actions.pop();
    return true;
  };
    componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
      this.props.getMainCat();
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }
  _renderError() {
    if (this.props.message) {
      this.props.clearMessage();
      return Toast.show({
        text: this.props.message,
        buttonText: L('dismiss'),
        duration: 3000,
        style: {backgroundColor: primaryColor},
        textStyle: {fontFamily: DroidKufi, fontSize: moderateScale(4.5)},
        buttonTextStyle: {fontFamily: DroidKufi, fontSize: moderateScale(4.5)},
      });
    }
  }
  _renderLoading() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }
  }

    _renderCategory({item}) {
        console.log("brand is : ---------------------------------------------");
        console.log(this.props.brandId);
        return (
            <TouchableOpacity
                onPress={() =>
                    Actions.push('productsPage', {title: item.name, catId: item.id,  brandId: this.props.brandId})
                }>
                <View style={styles.categoryImageContainer}>
                    <FastImage
                        source={{uri: item.photo, priority: FastImage.priority.normal}}
                        style={styles.brandImage}
                        resizeMode={FastImage.resizeMode.contain}
                    />
                    <View style={styles.categoryDetails}>
                        <Text
                            numberOfLines={1}
                            style={[styles.fontRegular, {color: primaryColor}]}>
                            {item.title}
                        </Text>
                    </View>
                </View>

            </TouchableOpacity>
        );
    }

  render() {
    return (
      <Container>
          <Header
              androidStatusBarColor={primaryColor}
              style={styles.routernavigationBarStyle}>
              <View
                  style={{
                      flexDirection: 'row',
                      justifyContent: 'flex-start',
                      alignSelf: 'center',
                      alignItems: 'center',
                      width: scale(100),
                  }}>
                  <Button
                      onPress={() => Actions.pop()}
                      transparent
                      /*background={TouchableNativeFeedback.Ripple(secondaryColor)}*/
                      style={styles.headerBack}>
                      <Icon
                          name="keyboard-backspace"
                          type="MaterialIcons"
                          style={{
                              color: '#fff',
                              fontSize: 25,
                              transform: [{scaleX: L('scaleLang')}],
                          }}
                      />
                  </Button>
                  <Text numberOfLines={1} style={styles.headerText}>
                      {this.props.title}
                  </Text>
                  <Button
                      onPress={() => Actions.jump('cart', {loading: true})}
                      transparent
                      /*background={TouchableNativeFeedback.Ripple(secondaryColor)}*/
                      style={[styles.headerBack]}>
                      <View
                          style={{
                              position: 'absolute',
                              height: 15,
                              width: 15,
                              borderRadius: 15,
                              backgroundColor: '#fff',
                              left: 8,
                              top: 5,
                              alignItems: 'center',
                              justifyContent: 'center',
                              zIndex: 2000,
                          }}>
                          <Text
                              style={{
                                  color: primaryColor,
                                  fontFamily: DroidKufi,
                                  fontSize: moderateScale(5),
                              }}>
                              {this.props.qty}
                          </Text>
                      </View>
                      <Icon
                          name="shopping-cart"
                          type="Feather"
                          style={{
                              fontSize: moderateScale(8),
                              color: '#fff',
                          }}
                      />
                      {/* <Icon name='keyboard-backspace' type='MaterialIcons'
            style={styles.backButtonIcon}
        /> */}
                  </Button>
              </View>
          </Header>
          <Content padder>
              <FlatList
                  data={this.props.mainCat}
                  renderItem={(item, brandId) => this._renderCategory(item, this.props.brandId)}
                  horizontal={false}
                  numColumns={2}
                  keyExtractor={(item, index) => index.toString()}
                  extraData={this.state}
                  columnWrapperStyle={{justifyContent: 'space-between'}}
                  initialNumToRender={4}
              />
          </Content>
        {this._renderError()}
        {this._renderLoading()}
      </Container>
    );
  }
}
const mapStateToProps = ({others, productsR}) => {
    const {mainCat} = productsR;
    const {loading,
        message,} = others;
  return {
    loading,
    message,
      mainCat
  };
};
export default connect(mapStateToProps, {
  getMainCat
})(CategoryPage);
