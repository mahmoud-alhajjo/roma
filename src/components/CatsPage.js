import React, {Component} from 'react';
import {
    Image,
    /*TouchableNativeFeedback,*/
    TouchableOpacity,
    FlatList,
    BackHandler, Dimensions, StyleSheet,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {Spinner} from './assets/common';
import {L} from '../Config';
import FastImage from 'react-native-fast-image';
import styles, {
  verticalScale,
  scale,
  moderateScale,
  buttonStyle,
  buttonTextStyle,
  textContainer,
  solidText,
  inputField,
  primaryColor,
  secondaryColor,
  fontBold,
  DroidKufi,
  DroidKufiBold,
  brandImage,
} from './assets/styles/Style';
import {
  Container,
  Content,
  Header,
  Item,
  Input,
  Button,
  Text,
  View,
  Icon,
  Toast,
} from 'native-base';
import {getCatsByBrand, clearMessage, showMessageChanged, getSlidersByBrand, getCatsByParent} from '../actions';
import Carousel from 'react-native-snap-carousel';

const {width: screenWidth} = Dimensions.get('window');
const styles2 = StyleSheet.create({
    item: {
        width: "100%",
        height: screenWidth - 150,
        marginTop: verticalScale(0),
    },
    imageContainer: {
        flex: 1,
        marginBottom: Platform.select({ios: 0, android: 1}), // Prevent a random Android rendering issue
        backgroundColor: 'white',
        borderRadius: moderateScale(2),
        marginHorizontal: scale(2.5),
    },
    image: {
        ...StyleSheet.absoluteFillObject,
        resizeMode: 'cover',
        /*borderRadius: moderateScale(6),*/
        // overflow: 'hidden',
        alignSelf: 'center',
        //marginHorizontal: scale(0),
    },
});
class CatsPage extends Component {
  state = {
    searchField: '',
  };
  onButtonPress = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    // then navigate
    // navigate('NewScreen');
  };

  handleBackButton = () => {
    Actions.pop();
    return true;
  };
  componentDidMount() {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
      this.props.getSlidersByBrand(this.props.brandId);
      if (this.props.catId && this.props.catId !== 2) {
          this.props.getCatsByParent(this.props.catId);
      } else {
          this.props.getCatsByBrand(this.props.brandId);
      }
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }
  _renderError() {
    if (this.props.message) {
      this.props.clearMessage();
      return Toast.show({
        text: this.props.message,
        buttonText: L('dismiss'),
        duration: 3000,
        style: {backgroundColor: primaryColor},
        textStyle: {fontFamily: DroidKufi, fontSize: moderateScale(4.5)},
        buttonTextStyle: {fontFamily: DroidKufi, fontSize: moderateScale(4.5)},
      });
    }
  }
  _renderLoading() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }
  }
  _renderCats({item}, props) {
    return (
      <TouchableOpacity
        onPress={() =>
          Actions.push('productsPage', {title: item.title, catId: item.id, discount: props.discount ? true : false,  chosenOfferItem: props.chosenOfferItem ? true : false})
        }>
        <View style={styles.brandImageContainer}>
          <FastImage
            source={{uri: item.photo, priority: FastImage.priority.normal}}
            style={styles.brandImage}
            resizeMode={FastImage.resizeMode.contain}
          />
        </View>
      </TouchableOpacity>
    );
  }
  _renderBrands = item => {
    item = item.item;
    // console.log(item);
    return (
      <TouchableOpacity
        onPress={() => {
            if (item.categoriesNumber > 0) {
                Actions.push('catsPage', {title: item.name, brandId: item.id})
            } else {
                Actions.push('productsPage', {title: item.name, brandId: item.id})
            }
        }
          /*Actions.push('productsPage', {
            title: item.name,
            brandId: item.id,
            catId: this.props.catId,
          })*/
        }>
        <View style={styles.brandImageContainer}>
          <FastImage
            source={{uri: item.photo, priority: FastImage.priority.normal}}
            style={styles.brandImage}
            resizeMode={FastImage.resizeMode.contain}
          />
        </View>
      </TouchableOpacity>
    );
  };
    _renderItem = ({item, index}) => {
        return (
            <View style={styles2.item} key={index}>
                <FastImage
                    source={{uri: item.photo, priority: FastImage.priority.normal}}
                    containerStyle={styles2.imageContainer}
                    style={styles2.image}
                />
            </View>
        );
    };
  render() {
    return (
      <Container>
        <Header
          androidStatusBarColor={primaryColor}
          style={styles.routernavigationBarStyle}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignSelf: 'center',
              alignItems: 'center',
              width: scale(100),
            }}>
            <Button
              onPress={() => Actions.pop()}
              transparent
              /*background={TouchableNativeFeedback.Ripple(secondaryColor)}*/
              style={styles.headerBack}>
              <Icon
                name="keyboard-backspace"
                type="MaterialIcons"
                style={{
                  color: '#fff',
                  fontSize: 25,
                  transform: [{scaleX: L('scaleLang')}],
                }}
              />
            </Button>
            <Text numberOfLines={1} style={styles.headerText}>
              {this.props.title}
            </Text>
          </View>
        </Header>
          <Content >
              <Carousel slideStyle={{height: screenWidth - 180}}
                        sliderWidth={screenWidth}
                        sliderHeight={screenWidth }
                        itemWidth={screenWidth - 0}
                        data={this.props.brandSlider}
                        renderItem={this._renderItem}
                        hasParallaxImages={false}
                        activeSlideAlignment="center"
                        autoplay={true}
                        loop={true}
                        inactiveSlideScale={1}
              />
        <Content padder>
          {this.props.catId !== 2 ? (
            <FlatList
              data={this.props.catsByParent}
              renderItem={item => this._renderCats(item, this.props)}
              horizontal={false}
              numColumns={2}
              keyExtractor={(item, index) => index.toString()}
              extraData={this.state}
              columnWrapperStyle={{justifyContent: 'space-between'}}
              initialNumToRender={4}
            />
          ) : (
            <FlatList
              data={this.props.brands}
              renderItem={this._renderBrands}
              horizontal={false}
              numColumns={2}
              keyExtractor={(item, index) => index.toString()}
              extraData={this.state}
              columnWrapperStyle={{justifyContent: 'space-between'}}
              initialNumToRender={4}
            />
          )}
        </Content>
          </Content>
        {this._renderError()}
        {this._renderLoading()}
      </Container>
    );
  }
}
const mapStateToProps = ({productsR}) => {
  const {catsByParent,brandSlider, loading, message} = productsR;
  // console.log(catsByParent);
  return {loading, message, catsByParent, brandSlider};
};
export default connect(mapStateToProps, {
    getCatsByBrand,
    getCatsByParent,
    getSlidersByBrand,
  clearMessage,
  showMessageChanged,
})(CatsPage);
