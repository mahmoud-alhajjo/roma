import React, {Component} from 'react';
import {
  Image,
  /*TouchableNativeFeedback,*/
  TouchableWithoutFeedback,
  TouchableOpacity,
  Modal,
  Linking,
  BackHandler
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {Spinner} from './assets/common';
import {L} from '../Config';
import AsyncStorage from '@react-native-community/async-storage';
import styles, {
  verticalScale,
  scale,
  moderateScale,
  buttonStyle,
  buttonTextStyle,
  textContainer,
  solidText,
  inputField,
  primaryColor,
  secondaryColor,
  fontBold,
  DroidKufi,
  DroidKufiBold,
  brandImage,
  modalContainerlStyle,
  drawerHeadText,
  modalStyle,
  textAreaField,
  noBorder,
} from './assets/styles/Style';
import {
  Container,
  Content,
  Header,
  Item,
  Input,
  Button,
  Text,
  View,
  Icon,
  Toast,
  Textarea,
} from 'native-base';
import {getSettings, contactForm} from '../actions';

class Contact extends Component {
  state = {
    searchField: '',
    modalVisible: false,
    message: '',
    user: null,
    phone: 0,
    email: '',
    name: '',
  };
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }
  closeModal() {
    this.setState({
      modalVisible: false,
    });
  }
  onButtonPress = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    // then navigate
    // navigate('NewScreen');
  };

  handleBackButton = () => {
    Actions.pop();
    return true;
  };
  async componentDidMount() {
    try {
      let value = await AsyncStorage.getItem('user');
      // console.log(value);
      const user = JSON.parse(value);
      if (user) {
        this.setState({user: user});
        this._getData();
      }
      // console.log(user);
    } catch (error) {
      // console.log(error);
      // this._getData()
    }
  }
  _getData() {
    this.props.getSettings();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }
  _renderError() {
    if (this.props.message) {
      this.props.clearMessage();
      return Toast.show({
        text: this.props.message,
        buttonText: L('dismiss'),
        duration: 3000,
        style: {backgroundColor: primaryColor},
        textStyle: {fontFamily: DroidKufi, fontSize: moderateScale(4.5)},
        buttonTextStyle: {fontFamily: DroidKufi, fontSize: moderateScale(4.5)},
      });
    }
  }
  _renderLoading() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }
  }
  onSubmitContact() {
    const sendContact = {
      name: this.state.name,
      email: this.state.email,
      phone: this.state.phone,
      message: this.state.message,
    };
    this.props.contactForm({sendContact});
    this.setModalVisible(false);
  }
  render() {
    return (
      <Container>
        <Header
          androidStatusBarColor={primaryColor}
          style={styles.routernavigationBarStyle}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignSelf: 'center',
              alignItems: 'center',
              width: scale(100),
            }}>
            <Button
              onPress={() => Actions.pop()}
              transparent
              /*background={TouchableNativeFeedback.Ripple(secondaryColor)}*/
              style={styles.headerBack}>
              <Icon
                name="keyboard-backspace"
                type="MaterialIcons"
                style={{
                  color: '#fff',
                  fontSize: 25,
                  transform: [{scaleX: L('scaleLang')}],
                }}
              />
            </Button>
            <Text numberOfLines={1} style={styles.headerText}>
              {this.props.title}
            </Text>
          </View>
        </Header>
        <Content
          style={{backgroundColor: '#f5f5f5', paddingTop: verticalScale(4)}}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              borderBottomWidth: 1,
              borderBottomColor: '#ccc',
            }}>
            {/* <Image
                            source={require('./assets/images/contact.png')}
                            resizeMode="contain"
                            style={{
                                width: scale(100),
                                height: verticalScale(35),
                                alignSelf: 'center'
                            }}
                        /> */}
            <TouchableOpacity
              onPress={() => Linking.openURL(`tel:${this.props.adminPhone}`)}>
              <View
                style={{
                  width: scale(30),
                  borderRightWidth: 1,
                  borderRightColor: '#ccc',
                }}>
                <Image
                  source={require('./assets/images/phone.png')}
                  resizeMode="contain"
                  style={{
                    width: scale(15),
                    height: verticalScale(8),
                    alignSelf: 'center',
                    marginBottom: 10,
                  }}
                />
                <Text style={[styles.fontRegular, {textAlign: 'center'}]}>
                  {this.props.adminPhone}
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() =>
                Linking.openURL(`mailto:${this.props.admin_email}`)
              }>
              <View
                style={{
                  width: scale(30),
                  borderRightWidth: 1,
                  borderRightColor: '#ccc',
                  alignSelf: 'center',
                }}>
                <Image
                  source={require('./assets/images/mail.png')}
                  resizeMode="contain"
                  style={{
                    width: scale(15),
                    height: verticalScale(8),
                    alignSelf: 'center',
                    marginBottom: 10,
                    marginRight: 15,
                  }}
                />
                <Text style={[styles.fontRegular, {textAlign: 'center'}]}>
                  {this.props.admin_email}
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View
                style={{
                  width: scale(30),
                  borderRightWidth: 1,
                  borderRightColor: '#ccc',
                  alignSelf: 'center',
                }}>
                <Image
                  source={require('./assets/images/place.png')}
                  resizeMode="contain"
                  style={{
                    width: scale(15),
                    height: verticalScale(8),
                    alignSelf: 'center',
                    marginBottom: 10,
                  }}
                />
                <Text
                  style={[
                    styles.fontRegular,
                    {textAlign: 'center', fontSize: moderateScale(4)},
                  ]}>
                  {this.props.address_ar}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          <View
            style={{
              width: scale(85),
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              alignSelf: 'center',
              marginTop: verticalScale(3),
            }}>
            <Button
              transparent
              onPress={() =>
                Linking.openURL(`${this.props.settings.facebook}`)
              }>
              <View
                style={{
                  backgroundColor: '#fff',
                  width: scale(15),
                  height: verticalScale(8),
                  justifyContent: 'center',
                  borderRadius: scale(100),
                  //   marginRight: scale(3),
                }}>
                <Icon
                  name="facebook"
                  type="FontAwesome"
                  style={{textAlign: 'center', color: primaryColor}}
                />
              </View>
            </Button>
            <Button
              transparent
              onPress={() => Linking.openURL(`${this.props.settings.twitter}`)}>
              <View
                style={{
                  backgroundColor: '#fff',
                  width: scale(15),
                  height: verticalScale(8),
                  justifyContent: 'center',
                  borderRadius: scale(100),
                  //   marginRight: scale(3),
                }}>
                <Icon
                  name="twitter"
                  type="FontAwesome"
                  style={{textAlign: 'center', color: primaryColor}}
                />
              </View>
            </Button>
            <Button
              transparent
              onPress={() => Linking.openURL(`${this.props.settings.insta}`)}>
              <View
                style={{
                  backgroundColor: '#fff',
                  width: scale(15),
                  height: verticalScale(8),
                  justifyContent: 'center',
                  borderRadius: scale(100),
                  //   marginRight: scale(3),
                }}>
                <Icon
                  name="instagram"
                  type="FontAwesome"
                  style={{textAlign: 'center', color: primaryColor}}
                />
              </View>
            </Button>
            <Button
              transparent
              onPress={() => Linking.openURL(`${this.props.settings.insta}`)}>
              <View
                style={{
                  backgroundColor: '#fff',
                  width: scale(15),
                  height: verticalScale(8),
                  justifyContent: 'center',
                  borderRadius: scale(100),
                  //   marginRight: scale(3),
                }}>
                <Icon
                  name="youtube-play"
                  type="FontAwesome"
                  style={{textAlign: 'center', color: primaryColor}}
                />
              </View>
            </Button>
          </View>
          <View
            style={{alignSelf: 'center', paddingVertical: verticalScale(2),marginTop:verticalScale(2)}}>
            <Item
              style={[
                styles.inputAuthContainer,
                {
                  borderRadius: moderateScale(5),
                  width: scale(90),
                  paddingHorizontal: scale(2),
                },
              ]}>
              <Input
                onChangeText={text => this.setState({name: text})}
                placeholder={L('username')}
                value={this.state.name}
                style={styles.inputField}
                placeholderTextColor="#a2a2a2"
              />
            </Item>
            <Item
              style={[
                styles.inputAuthContainer,
                {
                  borderRadius: moderateScale(5),
                  width: scale(90),
                  paddingHorizontal: scale(2),
                },
              ]}>
              <Input
                onChangeText={text => this.setState({email: text})}
                placeholder={L('email')}
                value={this.state.email}
                style={styles.inputField}
                placeholderTextColor="#a2a2a2"
              />
            </Item>
            <Item
              style={[
                styles.inputAuthContainer,
                {
                  borderRadius: moderateScale(5),
                  width: scale(90),
                  paddingHorizontal: scale(2),
                },
              ]}>
              <Input
                onChangeText={text => this.setState({phone: text})}
                placeholder={L('phone')}
                value={this.state.phone}
                style={styles.inputField}
                placeholderTextColor="#a2a2a2"
                keyboardType={"numeric"}
              />
            </Item>
            <Item
              style={[
                styles.inputAuthContainer,
                {
                  borderRadius: moderateScale(5),
                  width: scale(90),
                  paddingHorizontal: scale(2),
                },
              ]}>
              <Textarea
                rowSpan={5}
                onChangeText={text => this.setState({message: text})}
                placeholder={L('writeMessage')}
                value={this.state.message}
                style={styles.textAreaField}
                placeholderTextColor="#a2a2a2"
              />
            </Item>
            <Item regular style={styles.noBorder}>
              <Button
                block
                style={[
                  styles.buttonStyle2,
                  {
                    width: scale(90),
                    alignSelf: 'center',
                    marginBottom: verticalScale(4),
                  },
                ]}
                onPress={() => this.onSubmitContact()}>
                <Text style={styles.buttonTextStyle}>{L('send')}</Text>
              </Button>
            </Item>
          </View>
        </Content>
        <View>
          <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.modalVisible}
            onRequestClose={this.closeModal.bind(this)}
            hardwareAccelerated={true}
            presentationStyle="overFullScreen">
            <TouchableWithoutFeedback
              onPress={() => {
                this.setModalVisible(!this.state.modalVisible);
              }}>
              <View style={styles.modalContainerlStyle}>
                <View
                  style={[
                    styles.modalStyle,
                    {
                      width: scale(90),
                      padding: scale(5),
                      justifyContent: 'center',
                    },
                  ]}>
                  <Text
                    style={[
                      styles.fontRegular,
                      {
                        marginBottom: verticalScale(2),
                        textAlign: 'center',
                        color: primaryColor,
                      },
                    ]}>
                    {L('yourMessage')}
                  </Text>
                  <Item
                    style={[
                      styles.inputAuthContainer,
                      {
                        borderRadius: moderateScale(5),
                        marginBottom: verticalScale(0),
                        width: scale(80),
                      },
                    ]}>
                    <Textarea
                      rowSpan={5}
                      onChangeText={text => this.setState({message: text})}
                      placeholder={L('writeMessage')}
                      value={this.state.message}
                      style={styles.textAreaField}
                      placeholderTextColor="#a2a2a2"
                    />
                  </Item>
                  <Item regular style={styles.noBorder}>
                    <Button
                      block
                      style={[styles.buttonStyle2, {width: scale(80)}]}
                      onPress={() => this.onSubmitContact()}>
                      <Text style={styles.buttonTextStyle}>{L('send')}</Text>
                    </Button>
                  </Item>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </Modal>
        </View>
        {this._renderError()}
        {this._renderLoading()}
      </Container>
    );
  }
}
const mapStateToProps = ({others}) => {
  const {
    loading,
    message,
    adminPhone,
    admin_email,
    address_ar,
    settings,
  } = others;
  // console.log(catsByParent);
  return {loading, message, adminPhone, admin_email, address_ar, settings};
};
export default connect(mapStateToProps, {
  getSettings,
  contactForm,
})(Contact);
