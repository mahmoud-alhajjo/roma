import React, {Component} from 'react';
import {
  Image,
  TouchableWithoutFeedback,
  FlatList,
  TouchableOpacity,
  /*TouchableNativeFeedback,*/
  BackHandler
} from 'react-native';
import {connect} from 'react-redux';
import {Spinner} from './assets/common';
import {L} from '../Config';
import {AirbnbRating} from 'react-native-ratings';
import styles, {
  verticalScale,
  scale,
  buttonStyle,
  buttonStyle3,
  buttonTextStyle,
  moderateScale,
  secondaryColor,
  primaryColor,
} from './assets/styles/Style';
import {
  Container,
  Content,
  Toast,
  Text,
  View,
  Icon,
  Button,
  CardItem,
  Header,
  ListItem,
  Body,
  CheckBox,
} from 'native-base';
import {Actions} from 'react-native-router-flux';
import {
    clearMessage,
    getFilterOptions,
    showMessageChanged,
    filter, getPrice,
} from '../actions';

class Filter extends Component {
  state = {
    search: '',
    searchResult: [],
    filterId: 1,
    check: false,
    category_id: null,
    trademarks: [],
    duration: [],
    colors: [],
  };
  onButtonPress = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    // then navigate
    // navigate('NewScreen');
  };

  handleBackButton = () => {
    Actions.pop();
    return true;
  };
  componentDidMount() {
    this.props.getFilterOptions(); 
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }
  _renderError() {
    if (this.props.message) {
      this.props.clearMessage();
      return Toast.show({
        text: this.props.message,
        buttonText: L('dismiss'),
        duration: 3000,
        style: {backgroundColor: primaryColor},
        textStyle: {fontFamily: DroidKufi, fontSize: moderateScale(4.5)},
        buttonTextStyle: {fontFamily: DroidKufi, fontSize: moderateScale(4.5)},
      });
    }
  }
  renderLoading() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }
  }
  postFilter() {
    const postFilter = {
      trademarks: this.state.trademarks,
      duration: this.state.duration,
      colors: this.state.colors,
    };
    Actions.push('searchResults', {
      postFilter: postFilter,
      title: L('searchResults'),
      filterCheck: true,
    });
  }
  _renderItem({item}) {
    // console.log(item);
    return (
      <TouchableOpacity
        style={styles.productContainer}
        onPress={() =>
          Actions.push('product', {productID: item.id, title: item.title})
        }>
        <View>
          <Image
            source={{uri: item.photos[0] && item.photos[0].thumb}}
            style={styles.productImage}
          />
        </View>
        <View style={styles.productDetails}>
          <Text
            numberOfLines={1}
            style={[styles.fontRegular, {color: primaryColor}]}>
            {item.title}
          </Text>
          <AirbnbRating
            showRating={false}
            isDisabled
            defaultRating={5}
            ratingColor="#f4b700"
            size={10}
            starContainerStyle={{marginLeft: 0}}
          />
          <Text style={styles.fontRegular}>
            {getPrice(item.price)} {L(global.currency.code)}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
  inArray(needle, type) {
    const {trademarks, duration, colors} = this.state;
    var array = [];
    if (type == 1) {
      array = trademarks;
    } else if (type == 2) {
      array = duration;
    } else if (type == 3) {
      array = colors;
    }
    var haystack = array;
    var length = haystack.length;
    for (var i = 0; i < length; i++) {
      if (haystack[i] == needle) return true;
    }
    return false;
  }
  checkTrue(id, type) {
    const {trademarks, duration, colors} = this.state;
    var array = [];
    if (type == 1) {
      array = trademarks;
    } else if (type == 2) {
      array = duration;
    } else if (type == 3) {
      array = colors;
    }
    array.push(id);
    if (type == 1) {
      this.setState({trademarks: array});
    } else if (type == 2) {
      this.setState({duration: array});
    } else if (type == 3) {
      this.setState({colors: array});
    }
  }
  checkFalse(id, type) {
    const {trademarks, duration, colors} = this.state;
    var array = [];
    if (type == 1) {
      array = trademarks;
    } else if (type == 2) {
      array = duration;
    } else if (type == 3) {
      array = colors;
    }
    for (let i = 0; i < array.length; i++) {
      // console.log(trademarks[i],id);
      if (array[i] == id) {
        array.splice(i, 1);
      }
    }
    if (type == 1) {
      this.setState({trademarks: array});
    } else if (type == 2) {
      this.setState({duration: array});
    } else if (type == 3) {
      this.setState({colors: array});
    }
  }
  render() {
    const filterOptions = [
      {
        id: 1,
        title: L('lensBrands'),
      },
      {
        id: 2,
        title: L('duration'),
      },
      // {
      //   id: 3,
      //   title: L('lensColor'),
      // },
    ];
    return (
      <Container>
        <Header
          androidStatusBarColor={primaryColor}
          style={styles.routernavigationBarStyle}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignSelf: 'center',
              alignItems: 'center',
              width: scale(100),
            }}>
            <Button
              onPress={() => Actions.pop()}
              transparent
              /*background={TouchableNativeFeedback.Ripple(secondaryColor)}*/
              style={styles.headerBack}>
              <Icon
                name="keyboard-backspace"
                type="MaterialIcons"
                style={{
                  color: '#fff',
                  fontSize: 25,
                  transform: [{scaleX: L('scaleLang')}],
                }}
              />
            </Button>
            <Text numberOfLines={1} style={styles.headerText}>
              {this.props.title}
            </Text>
          </View>
        </Header>
        <Content>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View
              style={{
                width: scale(35),
                height: verticalScale(75),
                flexDirection: 'column',
                backgroundColor: primaryColor,
                marginTop: verticalScale(3),
                paddingTop: verticalScale(7),
                borderTopRightRadius: scale(15),
                // paddingLeft: scale(5)
                zIndex: 11,
                position: 'relative',
              }}>
              {filterOptions.map((item, index) => {
                return (
                  <TouchableWithoutFeedback
                    key={index}
                    onPress={() =>
                      this.setState({
                        filterId: item.id,
                      })
                    }>
                    <View
                      style={[
                        styles.buttonStyle2,
                        {
                          backgroundColor: '#fff',
                          width: scale(50),
                          borderTopRightRadius: 0,
                          borderBottomRightRadius: 0,
                          // borderWidth: 1, borderColor: this.state.filterId == item.id
                          //     ? '#fff' : '#a2a2a2',
                          height: verticalScale(6),
                          justifyContent: 'center',
                          marginBottom: verticalScale(1),
                          backgroundColor:
                            this.state.filterId == item.id
                              ? '#fff'
                              : 'transparent',
                          marginLeft: scale(15),
                          zIndex: 111,
                          shadowColor: '#000',
                          shadowOffset: {
                            width: 0,
                            height: 1,
                          },
                          shadowOpacity: 0.22,
                          shadowRadius: 2.22,
                          // backgroundColor: '#fff',
                          elevation: this.state.filterId == item.id ? 3 : 0,
                          paddingLeft: scale(6),
                        },
                      ]}>
                      <Text
                        style={[
                          styles.buttonTextStyle,
                          {
                            color:
                              this.state.filterId == item.id ? '#000' : '#fff',
                            fontSize: moderateScale(6),
                            textAlign: L('textAlignLeft'),
                          },
                        ]}>
                        {item.title}
                      </Text>
                    </View>
                  </TouchableWithoutFeedback>
                );
              })}
            </View>
            {this.state.filterId !== 0 ? (
              <View
                style={{
                  width: scale(60),
                  shadowColor: '#000',
                  shadowOffset: {
                    width: 0,
                    height: 1,
                  },
                  shadowOpacity: 0.22,
                  shadowRadius: 2.22,
                  backgroundColor: '#fff',
                  elevation: 3,
                  height: verticalScale(75),
                  flexDirection: 'column',
                  marginTop: verticalScale(3),
                  paddingTop: verticalScale(7),
                  borderTopLeftRadius: scale(15),
                  padding: scale(2),
                }}>
                <Content>
                  {this.state.filterId == 1
                    ? this.props.brands.map((item, index) => {
                        if (this.props.brands) {
                          var check = this.inArray(item.id, 1);
                          if (check == true) {
                            var favouriteClick = () =>
                              this.checkFalse(item.id, 1);
                          } else {
                            var favouriteClick = () =>
                              this.checkTrue(item.id, 1);
                          }
                        }
                        return (
                          <ListItem key={index} onPress={favouriteClick}>
                            <CheckBox
                              checked={this.inArray(item.id, 1) ? true : false}
                              onPress={favouriteClick}
                              color={primaryColor}
                            />
                            <Body>
                              <Text
                                numberOfLines={1}
                                style={styles.fontRegular}>
                                {item.name}
                              </Text>
                            </Body>
                          </ListItem>
                        );
                      })
                    : this.state.filterId == 2
                    ? this.props.durations.map((item, index) => {
                        if (this.props.durations) {
                          var check = this.inArray(item.id, 2);
                          if (check == true) {
                            var favouriteClick = () =>
                              this.checkFalse(item.id, 2);
                          } else {
                            var favouriteClick = () =>
                              this.checkTrue(item.id, 2);
                          }
                        }
                        return (
                          <ListItem key={index} onPress={favouriteClick}>
                            <CheckBox
                              checked={this.inArray(item.id, 2) ? true : false}
                              onPress={favouriteClick}
                              color={primaryColor}
                            />
                            <Body>
                              <Text
                                numberOfLines={1}
                                style={styles.fontRegular}>
                                {item.name}
                              </Text>
                            </Body>
                          </ListItem>
                        );
                      })
                    // : this.state.filterId == 3
                    // ? this.props.colors.map((item, index) => {
                    //     if (this.props.colors) {
                    //       var check = this.inArray(item.id, 3);
                    //       if (check == true) {
                    //         var favouriteClick = () =>
                    //           this.checkFalse(item.id, 3);
                    //       } else {
                    //         var favouriteClick = () =>
                    //           this.checkTrue(item.id, 3);
                    //       }
                    //     }
                    //     return (
                    //       <ListItem key={index} onPress={favouriteClick}>
                    //         <CheckBox
                    //           checked={this.inArray(item.id, 3) ? true : false}
                    //           onPress={favouriteClick}
                    //           color={primaryColor}
                    //         />
                    //         <Body>
                    //           <Text
                    //             numberOfLines={1}
                    //             style={styles.fontRegular}>
                    //             {item.name}
                    //           </Text>
                    //         </Body>
                    //       </ListItem>
                    //     );
                    //   })
                    : null}
                </Content>
              </View>
            ) : null}
          </View>
          <Button style={styles.buttonStyle2} onPress={() => this.postFilter()}>
            <Text style={styles.buttonTextStyle}>{L('applyFilter')}</Text>
          </Button>
        </Content>
        {this.renderLoading()}
        {this._renderError()}
      </Container>
    );
  }
}

const mapStateToProps = ({auth, others}) => {
  const {user} = auth;
  const {
    searchResult,
    loading,
    message,
    refresh,
    brands,
    durations,
    colors,
  } = others;
  // console.log('brands', brands, 'durations', durations, 'colors', colors);

  return {
    message,
    loading,
    user,
    searchResult,
    refresh,
    brands,
    durations,
    colors,
  };
};

export default connect(mapStateToProps, {
  clearMessage,
  showMessageChanged,
  getFilterOptions,
  filter,
})(Filter);
