import React, {Component} from 'react';
import {Image,
    /*TouchableNativeFeedback,*/
    BackHandler} from 'react-native';
import {connect} from 'react-redux';
import {Spinner} from './assets/common';
import {Actions} from 'react-native-router-flux';
import {L} from '../Config';
import styles, {
  verticalScale,
  scale,
  buttonStyle,
  buttonTextStyle,
  moderateScale,
  inputField,
  primaryColor,
  fontBold,
  DroidKufi,
  DroidKufiBold,
} from './assets/styles/Style';
import {
  Container,
  Content,
  Header,
  Form,
  Left,
  Item,
  Input,
  Button,
  Text,
  View,
  Icon,
  Toast,
  Label,
  Right,
} from 'native-base';
import {forgetUserPassword, emailForgetUserChanged} from '../actions';

class ForgetPassword extends Component {
  state = {
    language: 'ar',
  };
  onButtonPress = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    // then navigate
    // navigate('NewScreen');
  };

  handleBackButton = () => {
    Actions.pop();
    return true;
  };
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }
  onForgetPassChange(text) {
    this.props.emailForgetUserChanged(text);
  }

  onForgetPassPressed() {
    const {emailF} = this.props;
    // console.log(email);
    if (emailF == '') {
      this.onMessageChange(L('emptyField') + ' ' + L('email'));
    } else {
      this.props.forgetUserPassword(emailF);
    }
  }

  _renderError() {
    if (this.props.message) {
      this.props.clearMessage();
      return Toast.show({
        text: this.props.message,
        buttonText: L('dismiss'),
        duration: 3000,
        style: {backgroundColor: primaryColor},
        textStyle: {fontFamily: DroidKufi, fontSize: moderateScale(4.5)},
        buttonTextStyle: {fontFamily: DroidKufi, fontSize: moderateScale(4.5)},
      });
    }
  }
  _renderLoading() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }
  }
  render() {
    return (
      <Container>
        <Header
          androidStatusBarColor={primaryColor}
          style={[
            styles.routernavigationBarStyle,
            {backgroundColor: '#fff', elevation: 0},
          ]}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignSelf: 'center',
              alignItems: 'center',
              width: scale(100),
            }}>
            <Button
              onPress={() => Actions.pop()}
              transparent
              /*background={TouchableNativeFeedback.Ripple(primaryColor)}*/
              // style={{ marginTop: verticalScale(1) }}
            >
              <Icon
                name="keyboard-backspace"
                type="MaterialIcons"
                style={{
                  color: primaryColor,
                  fontSize: 25,
                  transform: [{scaleX: L('scaleLang')}],
                }}
              />
            </Button>
            {/* <Text style={[styles.fontRegular,{fontSize:14,width:scale(42)}]}>Forget</Text> */}
          </View>
        </Header>
        <Content>
          <View style={{height: verticalScale(90), alignSelf: 'center'}}>
            {/* <Image source={require('./assets/images/bg1.png')} style={styles.bgLogo} /> */}
            <Item style={styles.imageContainer}>
              <Image
                source={require('./assets/images/logo.png')}
                style={styles.lbgImage}
              />
            </Item>
            <Item
              style={[
                styles.inputAuthContainer,
                {
                  borderTopWidth: 0,
                  borderLeftWidth: 0,
                  borderRightWidth: 0,
                },
              ]}
              floatingLabel>
              <Label style={styles.inputLabel}>{L('email')}</Label>
              <Input
                onChangeText={this.onForgetPassChange.bind(this)}
                value={this.props.emailF}
                style={[styles.inputField, {textAlign: L('textAlignRight')}]}
              />
            </Item>
            <Item style={{borderBottomWidth: 0}}>
              <Button
                block
                style={[
                  styles.buttonStyle,
                  {
                    marginBottom: verticalScale(2),
                  },
                ]}
                onPress={() => this.onForgetPassPressed()}>
                <Text style={styles.buttonTextStyle}>{L('resetPass')}</Text>
              </Button>
            </Item>
          </View>
        </Content>
        {this._renderError()}
        {this._renderLoading()}
      </Container>
    );
  }
}

const mapStateToProps = ({auth}) => {
  const {loading, message, emailF} = auth;
  // console.log(user);
  return {loading, message, emailF};
};
export default connect(mapStateToProps, {
  forgetUserPassword,
  emailForgetUserChanged,
})(ForgetPassword);
