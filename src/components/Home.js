import React, { useState, useEffect, useRef } from 'react';
import {
    Alert,
    BackHandler,
    Dimensions,
    FlatList,
    Image,
    Linking,
    StyleSheet,
    TextInput,
    /*TouchableNativeFeedback,*/
    TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
// import AsyncStorage from '@react-native-community/async-storage';
import { Spinner } from './assets/common';
import { L } from '../Config';
import Carousel from 'react-native-snap-carousel';
import FastImage from 'react-native-fast-image';
import { FAB } from 'react-native-paper';
/*
import { Container as FloatingContainer, Button as FloatingButton, Link as FloatingLink} from 'react-floating-action-button'
*/
import styles, {
    brandImage,
    buttonStyle,
    buttonTextStyle,
    DroidKufi,
    DroidKufiBold,
    inputField,
    moderateScale,
    primaryColor,
    scale,
    secondaryColor,
    solidText,
    textContainer,
    verticalScale,
} from './assets/styles/Style';
import { Button, Container, Content, Header, Icon, Input, Item, Text, Toast, View, } from 'native-base';
import {
    clearMessage,
    getDiscountedProducts,
    getHomescreen,
    getMainCat, getPrice,
    getSettings,
    showMessageChanged,
} from '../actions';
// import { AirbnbRating } from "react-native-elements";

const { width: screenWidth } = Dimensions.get('window');

const Home = (props) => {
    const whatsappIcon = require('./assets/images/whatspp_icon.png');
    const categoryScroll = useRef()
    useEffect(() => {
        props.getMainCat();
        props.getHomescreen();
        props.getSettings();
        props.getDiscountedProducts();
    }, []);
    useEffect(() => {
      const root = categoryScroll.current._root;
      if(categoryScroll && categoryScroll.current && root ) {
        // console.log('categoryScroll',!!categoryScroll.current, categoryScroll.current)
        setTimeout(() => root.scrollToPosition(0, 0), 3000)
        setTimeout(() => root.scrollToEnd({ duration: 2500, animated: true }), 7000)
      }
    }, [categoryScroll.current]);
    const [state, setState] = useState({
        searchField: undefined,
        search: undefined,
        offers: false,
        activeTab: 1,
    });

    const _renderError = () => {
        if (props.message) {
            props.clearMessage();
            return Toast.show({
                text: props.message,
                buttonText: L('dismiss'),
                duration: 3000,
                style: { backgroundColor: primaryColor },
                textStyle: { fontFamily: DroidKufi, fontSize: moderateScale(4.5) },
                buttonTextStyle: { fontFamily: DroidKufi, fontSize: moderateScale(4.5) },
            });
        }
    }
    const _renderLoading = () => {
        if (props.loading) {
            return <Spinner size="large" />;
        }
    };

    const _renderItem = ({ item, index }) => {
        return (
            <View style={styles2.item} key={index}>
                <FastImage
                    source={{ uri: item.photo, priority: FastImage.priority.normal }}
                    containerStyle={styles2.imageContainer}
                    style={styles2.image}
                />
            </View>
        );
    };

    const _renderBrands = ({ item }) => {
        // console.log(item);
        return (
            <TouchableOpacity
                onPress={() => {
                    if (item.categoriesNumber > 0) {
                        Actions.push('catsPage', { title: item.name, brandId: item.id })
                    } else {
                        Actions.push('productsPage', { title: item.name, brandId: item.id })
                    }
                }
                }
            >
                <View style={styles.brandImageContainer}>
                    <FastImage
                        source={{ uri: item.photo, priority: FastImage.priority.normal }}
                        style={styles.brandImage}
                        resizeMode={FastImage.resizeMode.contain}
                    />
                </View>
            </TouchableOpacity>
        );
    };
    const _renderDiscountItem = ({ item }) => {
        return (
            <TouchableOpacity
                onPress={() => {
                    if (item.offer && item.name) {
                        if (item.categoriesNumber > 0) {
                            Actions.push('catsPage', { title: item.name, brandId: item.id, discount: true })
                        } else {
                            Actions.push('productsPage', { title: item.name, brandId: item.id, discount: true })
                        }
                    } else if (item.offer && item.title) {
                        Actions.push('productsPage', { title: item.title, catId: item.id, discount: true })
                    } else {
                        Actions.push('offersPage', { title: item.name, id: item.id, catId: item.title ? true : false})
                    }
                }
                }
            >
                <View style={styles.brandImageContainer}>
                    <FastImage
                        source={{ uri: item.photo, priority: FastImage.priority.normal }}
                        style={styles.brandImage}
                        resizeMode={FastImage.resizeMode.contain}
                    />
                    {item.offer ?
                        <View style={styles2.styleVeiwTextFreeBrand}>
                            <Text style={[styles2.styleTextFreeBrand]}>
                                BUY {item.offer.condition} GET {item.offer.result} FREE
                            </Text>
                        </View> : null
                    }
                </View>
            </TouchableOpacity>
        );
    };

    const onSubmitSearch = (search) => {
        const postSearch = {
            search: search,
        };
        Actions.push('searchResults', {
            postSearch: postSearch,
            title: L('searchResults'),
            filterCheck: false,
        });
        setState({ search: '' });
    }

    const remove_duplicates_ItemsArray = (arr) => {
        // let s = new Set(arr);
        // let it = s.values();
        // return Array.from(it);
        if (arr.length > 0) {
            return arr.filter((v, i, a) => a.findIndex(t => (t.id === v.id)) === i)
        } else {
            return []
        }
    }

    const offersBrands = (disProducts, brands, offeredCats) => {
        console.log('brands', brands)
        console.log('offeredCats', offeredCats)
        console.log('disProducts', disProducts)
        let offersCategories = []
        let offersBrands = []
        offersCategories = disProducts.map((itemBrand) => {
            if (itemBrand.brand_id === 0) {
                return itemBrand.category
            }
        }).filter(item => item != undefined)
        offersBrands = disProducts.map((itemBrand) => {
            if (itemBrand.brand_id) {
                return itemBrand.brand
            }
        }).filter(item => item != undefined)
        brands.length > 0 && brands.map(brand => {
            if (brand.offer && brand.offer.condition > 0) {
                offersBrands = [...offersBrands, brand]
            }
        })
        offeredCats.length > 0 && offeredCats.map(category => {
            if (category.offer && category.offer.condition > 0) {
                offersCategories = [...offersCategories, category]
            }
        })
        let allOffers = [...remove_duplicates_ItemsArray(offersBrands),
        ...remove_duplicates_ItemsArray(offersCategories)
        ]
        // console.log('allOffers', allOffers)
        return allOffers;
    }

    return (
        <Container>
            <Header
                androidStatusBarColor={primaryColor}
                style={styles.routernavigationBarStyle}>
                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'space-evenly',
                        alignSelf: 'center',
                        alignItems: 'center',
                        width: scale(100),
                    }}>
                    <Item
                        style={[
                            styles.inputAuthContainer,
                            {
                                width: scale(80),
                                height: verticalScale(5),
                                borderRadius: moderateScale(10),
                                backgroundColor: secondaryColor,
                                borderTopWidth: 0,
                                borderBottomWidth: 0,
                                borderLeftWidth: 0,
                                borderRightWidth: 0,
                                elevation: 0,
                                marginBottom: 0,
                                // alignSelf: 'flex-end'
                            },
                        ]}>
                        <Button transparent>
                            <Icon
                                name="search"
                                style={{
                                    color: '#fff',
                                    fontSize: moderateScale(8),
                                    textAlign: 'center',
                                    alignSelf: 'center',
                                    transform: [{ scaleX: L('scaleLang') }],
                                }}
                            />
                        </Button>
                        <Input
                            onChangeText={text => setState({ search: text })}
                            value={state.search}
                            placeholder={L('searchPlaceholder')}
                            style={[
                                styles.inputField,
                                { color: 'white', selectionColor: 'white', width: '100%', textAlign: L('textAlignRight') },
                            ]}
                            placeholderTextColor="white"
                        />
                        {state.search ?
                            <Button
                                onPress={() => onSubmitSearch(state.search)}
                                small
                                style={{
                                    borderRadius: moderateScale(5),
                                    backgroundColor: '#fff',
                                    height: verticalScale(4),
                                }}>
                                <Text style={[styles.fontRegular, { color: primaryColor }]}>
                                    {L('search')}
                                </Text>
                            </Button> : null}
                        <TouchableOpacity
                            /*background={TouchableNativeFeedback.Ripple(
                                secondaryColor,
                                true,
                            )}*/
                            onPress={() => Actions.push('filter', { title: L('filter') })}>
                            <View
                                style={{
                                    // borderWidth: 1,
                                    // borderColor: '#ff0',
                                    height: verticalScale(5),
                                    width: scale(10),
                                    borderRadius: moderateScale(100),
                                    overflow: 'hidden',
                                    // backgroundColor: '#F66',
                                    justifyContent: 'center',
                                }}>
                                <Icon
                                    name="equalizer"
                                    type="SimpleLineIcons"
                                    style={{
                                        color: '#fff',
                                        fontSize: moderateScale(8),
                                        textAlign: 'center',
                                        alignSelf: 'center',
                                        transform: [{ rotateX: '180deg' }, { rotateZ: '1.6rad' }],
                                        textAlign: 'center',
                                        alignSelf: 'center',
                                    }}
                                />
                            </View>
                        </TouchableOpacity>
                    </Item>
                    <TouchableOpacity
                        /*background={TouchableNativeFeedback.Ripple(secondaryColor, true)}*/
                        onPress={() =>
                            Actions.push('notifications', { title: L('Notifications') })
                        }>
                        <View
                            style={{
                                // borderWidth: 1,
                                // borderColor: '#ff0',
                                height: verticalScale(5),
                                width: scale(10),
                                borderRadius: moderateScale(100),
                                overflow: 'hidden',
                                // backgroundColor: '#F66',
                                justifyContent: 'center',
                            }}>
                            <Icon
                                name="bell"
                                type="Feather"
                                style={{
                                    color: '#fff',
                                    fontSize: moderateScale(8),
                                    textAlign: 'center',
                                    alignSelf: 'center',
                                }}
                            />
                        </View>
                    </TouchableOpacity>
                </View>
            </Header>

            <Content >
                <Carousel slideStyle={{ height: screenWidth - 180 }}
                    sliderWidth={screenWidth}
                    sliderHeight={screenWidth}
                    itemWidth={screenWidth - 0}
                    data={props.slider}
                    renderItem={_renderItem}
                    hasParallaxImages={false}
                    activeSlideAlignment="center"
                    autoplay={true}
                    loop={true}
                    inactiveSlideScale={1}
                />
                <Content padder >
                    <Content
                        horizontal
                        style={{ flexDirection: 'row' }}
                        ref={categoryScroll}
                        showsHorizontalScrollIndicator={false}
                    >
                        {props.mainCat.map((item, index) => {
                            return (
                                <TouchableOpacity
                                    style={{ width: scale(31), alignSelf: 'center' }}
                                    key={index}
                                    onPress={() =>
                                        Actions.jump('catsPage', {
                                            catId: item.id,
                                            title: item.title,
                                            brands: props.brands,
                                        })
                                    }>
                                    {/* <FastImage
                        source={{uri: item.photo}}
                        style={{
                          height: verticalScale(8),
                          width: scale(15),
                          borderColor: '#ddd',
                          borderWidth: 1,
                          borderRadius: moderateScale(100),
                          resizeMode: 'cover',
                          alignSelf: 'center',
                        }}
                      /> */}
                                    <Text
                                        style={[
                                            styles.fontRegular,
                                            {
                                                alignSelf: 'center', marginTop: verticalScale(0), height: verticalScale(6),
                                                marginBottom: verticalScale(1),
                                                width: scale(29),
                                                borderColor: primaryColor,
                                                borderWidth: 1,
                                                borderRadius: moderateScale(100), textAlign: 'center', textAlignVertical: 'center'
                                            },
                                        ]}>
                                        {item.title}
                                    </Text>
                                </TouchableOpacity>
                            );
                        })}
                        <TouchableOpacity
                            style={{ width: scale(31), alignSelf: 'center' }}
                            onPress={() =>
                                Actions.jump('productsPage', {
                                    catId: false,
                                    title: L('discountProducts'),
                                })
                            }>
                            {/* <FastImage
                    source={{uri: this.props.disProductPhoto}}
                    style={{
                      height: verticalScale(8),
                      width: scale(15),
                      borderColor: '#ddd',
                      borderWidth: 1,
                      borderRadius: moderateScale(100),
                      resizeMode: 'cover',
                      alignSelf: 'center',
                    }}
                  /> */}
                            <Text
                                style={[
                                    styles.fontRegular,
                                    {
                                        alignSelf: 'center', marginTop: verticalScale(0), height: verticalScale(6), marginBottom: verticalScale(1),
                                        width: scale(29),
                                        borderColor: primaryColor,
                                        borderWidth: 1,
                                        borderRadius: moderateScale(100), textAlign: 'center', textAlignVertical: 'center'
                                    },
                                ]}>
                                {L('discountProducts')}
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{ width: scale(31), alignSelf: 'center' }}
                            onPress={() =>
                                Linking.openURL(
                                    `https://api.whatsapp.com/send?phone=` +
                                    props.adminPhone,
                                )
                            }>
                            {/* <FastImage
                    source={{uri: this.props.specialOrderPhoto}}
                    style={{
                      height: verticalScale(8),
                      width: scale(15),
                      borderColor: '#ddd',
                      borderWidth: 1,
                      borderRadius: moderateScale(100),
                      resizeMode: 'cover',
                      alignSelf: 'center',
                    }}
                  /> */}
                            <Text
                                style={[
                                    styles.fontRegular,
                                    {
                                        alignSelf: 'center', marginTop: verticalScale(0), height: verticalScale(6), marginBottom: verticalScale(1),
                                        width: scale(29),
                                        borderColor: primaryColor,
                                        borderWidth: 1,
                                        borderRadius: moderateScale(100), textAlign: 'center', textAlignVertical: 'center'
                                    },
                                ]}>
                                {L('specialOrders')}
                            </Text>
                        </TouchableOpacity>

                    </Content>


                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            paddingVertical: verticalScale(2),
                        }}>
                        <TouchableOpacity style={{ flex: 1, }} onPress={() => setState({ offers: false, activeTab: 1 })}>
                            <View style={{ alignItems: 'center' }}>
                                <Text style={[styles.fontBold, state.activeTab === 1 && styles2.tabDecoration]}>{L('lensBrands')}</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ flex: 1 }} onPress={() => setState({ offers: true, activeTab: 2 })}>
                            <View style={{ alignItems: 'center' }}>
                                <Text style={[styles.fontBold, state.activeTab === 2 && styles2.tabDecoration]}>
                                    {L('offers')}
                                </Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    {!state.offers ?
                        <FlatList
                            data={props.brands}
                            renderItem={_renderBrands}
                            horizontal={false}
                            numColumns={2}
                            keyExtractor={(item, index) => index.toString()}
                            extraData={state}
                            columnWrapperStyle={{ justifyContent: 'space-between' }}
                            initialNumToRender={4}
                        />
                        : null
                    }
                    {(state.offers && offersBrands(props.disProducts, props.brands, props.offeredCats).length > 0) ?
                        <FlatList
                            data={offersBrands(props.disProducts, props.brands, props.offeredCats)}
                            renderItem={_renderDiscountItem}
                            horizontal={false}
                            numColumns={2}
                            keyExtractor={(item, index) => index.toString()}
                            extraData={state}
                            columnWrapperStyle={{ justifyContent: 'space-between' }}
                            initialNumToRender={4}
                        />
                        :
                        (state.offers && offersBrands(props.disProducts, props.brands, props.offeredCats).length === 0) ?
                            <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                                <Text style={styles2.noNot}>{L('noNot')}</Text>
                            </View>
                            : null
                    }
                </Content>
            </Content>
            <FAB
                style={styles.fab}
                icon={whatsappIcon}
                onPress={() =>
                    Linking.openURL(
                        `https://api.whatsapp.com/send?phone=` +
                        props.adminPhone,
                    )
                }
            />
            {_renderError()}
            {_renderLoading()}
        </Container>
    );
};

const styles2 = StyleSheet.create({
    item: {
        width: "100%",
        height: screenWidth - 150,
        marginTop: verticalScale(0),
    },
    imageContainer: {
        flex: 1,
        marginBottom: Platform.select({ ios: 0, android: 1 }), // Prevent a random Android rendering issue
        backgroundColor: 'white',
        borderRadius: moderateScale(2),
        marginHorizontal: scale(2.5),
    },
    image: {
        ...StyleSheet.absoluteFillObject,
        resizeMode: 'cover',
        /*borderRadius: moderateScale(6),*/
        // overflow: 'hidden',
        alignSelf: 'center',
        //marginHorizontal: scale(0),
    },
    tabDecoration: {
        borderBottomColor: '#727c86',
        borderRadius: 5,
        borderBottomWidth: 2,
        color: '#727c86'
    },
    styleVeiwTextFreeBrand: {
        // flexDirection: L('textAlignRight') === 'right' ? 'row' : 'row-reverse',
        flexDirection: 'row-reverse',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        bottom: verticalScale(2.6),
        left: scale(11.25)
    },
    styleTextFreeBrand: {
        textAlign: 'center',
        alignSelf: 'center',
        fontFamily: DroidKufiBold,
        color: '#fff',
        backgroundColor: '#7d7d7d',
        fontSize: moderateScale(3.9),
        paddingHorizontal: scale(0.6),
        // letterSpacing: moderateScale(-0.4)
    }
});
const mapStateToProps = ({ others, productsR }) => {
    const { mainCat, disProducts } = productsR;
    const {
        slider,
        brands,
        offeredCats,
        loading,
        message,
        adminPhone,
        disProductPhoto,
        specialOrderPhoto,
    } = others;
    // console.log(adminPhone);
    return {
        loading,
        message,
        slider,
        brands,
        offeredCats,
        mainCat,
        disProducts,
        adminPhone,
        disProductPhoto,
        specialOrderPhoto,
    };
};
export default connect(mapStateToProps, {
    getMainCat,
    getHomescreen,
    clearMessage,
    showMessageChanged,
    getSettings,
    getDiscountedProducts
})(Home);
