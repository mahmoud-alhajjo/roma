import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Spinner} from './assets/common';
import {Actions} from 'react-native-router-flux';
import {Container, View, Header, Button, Text} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import styles, {
  scale,
  verticalScale,
  primaryColor,
} from './assets/styles/Style';
import {getCurrencies, getSettings} from '../actions';
import FastImage from 'react-native-fast-image';
import {L} from '../Config';

class Intro extends Component {
  componentDidMount = async () => {
      this.props.getCurrencies(null);
    try {
      const value = await AsyncStorage.getItem('user');
      user = await JSON.parse(value);
      if (user) {
        // Actions.reset('homeStack');
        this._getData();
      } else {
        // alert('user fail');
        this._getData();
      }
    } catch (e) {}
  };
  _getData() {
    // console.log(this.props);
    
    // if (this.props.bannerApp == '') {
    //   Actions.reset('homeStack');
    // }
    this.props.getSettings();
  }
  onPushHome() {
    Actions.reset('homeStack');
  }
  _renderLoading() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }
  }
  render() {
    return (
      <Container>
        <Header
          androidStatusBarColor={primaryColor}
          style={{display: 'none'}}
        />
        <View>
          <FastImage
            source={{
              uri: this.props.bannerApp,
              priority: FastImage.priority.high,
            }}
            resizeMode="stretch"
            style={{width: scale(100), height: verticalScale(100)}}
          />
          <Button
            block
            style={[
              styles.buttonStyle,
              {
                // marginBottom: verticalScale(2),
                position: 'absolute',
                bottom: verticalScale(10),
                width: scale(30),
              },
            ]}
            onPress={() => this.onPushHome()}>
            <Text style={styles.buttonTextStyle}>{L('enter')}</Text>
          </Button>
        </View>
        {this._renderLoading()}
      </Container>
    );
  }
}

const mapStateToProps = ({others}) => {
  const {bannerApp, loading, currencies} = others;
  // console.log(bannerApp);
  
  return {bannerApp, loading};
};

export default connect(mapStateToProps, {getSettings, getCurrencies})(Intro);
