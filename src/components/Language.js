import React, {Component} from 'react';
import {
    Image,
    StyleSheet,
    /*TouchableNativeFeedback,*/
    BackHandler, Linking, TouchableOpacity, ScrollView,
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {editUser, clearMessage, showMessageChanged, getCurrencies} from '../actions';
import styles, {
  verticalScale,
  scale,
  buttonStyle,
  buttonTextStyle,
  textPageContent,
  moderateScale,
  primaryColor,
  secondaryColor,
} from './assets/styles/Style';
import {
  Container,
  Toast,
  Text,
  Header,
  Button,
  View,
  Spinner,
  Icon,
} from 'native-base';

import {L, changeLng, currencyRate} from '../Config';
import AsyncStorage from "@react-native-community/async-storage/lib/AsyncStorage";

class Language extends Component {
  state = {
    aciveLang: false,
    lang: 'ar',
      selectedCurrencyId: global.currency ? global.currency.id : 0
  };
  handleBackButton = () => {
    this.goBack();
    return true;
  };
  componentDidMount() {
      this.props.getCurrencies(null);
    if (this.props.langPush == true) {
      this.backHandler = BackHandler.addEventListener(
        'hardwareBackPress',
        () => {
          Actions.pop(); // works best when the goBack is async
          return true;
        },
      );
    }
    // if (!this.props.userId) {
    //   this.props.showMessageChanged(L('loginFirst'))
    // }
  }
  // componentDidUpdate(){
  //   console.log(this.state);
  // }
  _sortLanguage() {
    changeLng(this.state.lang, 1);
    // console.log(this.state.lang);

    // if (!this.props.userId) {
    //   this.props.showMessageChanged(L('loginFirst'))
    // } else {
    //   const user = { userId: this.props.userId, language: lang, updateLang: true }
    //   this.props.editUser({ user })
    //   Actions.LoginForm()

    //   // await AsyncStorage.setItem('language', lang);

    // }
  }
  componentWillUnmount() {
    if (this.props.langPush == true) {
      BackHandler.removeEventListener(
        'hardwareBackPress',
        this.handleBackButton,
      );
    }
  }
  _renderError() {
    if (this.props.message) {
      this.props.clearMessage();
      return Toast.show({
        text: this.props.message,
        buttonText: 'تم',
        duration: 3000,
        style: {backgroundColor: '#000'},
      });
    }
  }
  _renderLoading() {
    if (this.props.loading) {
      return <Spinner />;
    }
  }

  render() {
    return (
      <Container>
        <Header
          androidStatusBarColor={primaryColor}
          style={{display: 'none'}}
        />
        {this.props.langPush == true ? (
          <View
            style={{position: 'absolute', top: 5, left: 5, zIndex: 9999999}}>
            <Button
              onPress={() => Actions.pop()}
              transparent
              /*background={TouchableNativeFeedback.Ripple(secondaryColor)}*/
              style={styles.headerBack}>
              <Icon
                name="keyboard-backspace"
                type="MaterialIcons"
                style={{
                  color: primaryColor,
                  fontSize: 25,
                  transform: [{scaleX: L('scaleLang')}],
                }}
              />
            </Button>
          </View>
        ) : null}
        {/* <Image source={require('./assets/images/splash.png')}
          style={{ position: 'absolute', right: 0, left: 0, width: scale(100), height: verticalScale(100), zIndex: -1 }} /> */}
        <View
          style={{
            justifyContent: 'center',
            height: verticalScale(10),
            alignSelf: 'center',
          }}>
          <Image
            source={require('./assets/images/logo.png')}
            style={{
              width: moderateScale(90),
              height: moderateScale(80),
              resizeMode: 'contain',
              marginTop: verticalScale(0),
              // borderColor: "#00f",
              // borderWidth: 2
            }}
          />
        </View>
        <View
          style={{
            /*position: 'absolute',
            top: verticalScale(15),*/
            alignSelf: 'center',
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignSelf: 'center',
              overflow: 'hidden',
              borderRadius: moderateScale(10),
              borderWidth: 1,
              borderColor: primaryColor,
            }}>
            <Button
              transparent
              style={[
                styles.langButton,
                {
                  backgroundColor:
                    this.state.aciveLang === true
                      ? 'transparent'
                      : primaryColor,
                },
              ]}
              onPress={() =>
                this.setState({
                  aciveLang: false,
                  lang: 'ar',
                })
              }>
              <Text
                style={[
                  styles.buttonTextStyle,
                  {
                    color:
                      this.state.aciveLang === true ? primaryColor : '#fff',
                  },
                ]}>
                عربي
              </Text>
            </Button>
            <Button
              transparent
              style={[
                styles.langButton,
                {
                  backgroundColor:
                    this.state.aciveLang === false
                      ? 'transparent'
                      : primaryColor,
                },
              ]}
              onPress={() =>
                this.setState({
                  aciveLang: true,
                  lang: 'en',
                })
              }>
              <Text
                style={[
                  styles.buttonTextStyle,
                  {
                    color:
                      this.state.aciveLang === true ? '#fff' : primaryColor,
                  },
                ]}>
                English
              </Text>
            </Button>
          </View>

        </View>
          <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center'}}>
          {this.props.currencies.map((item, index) => {
              return (
                  <View style={{alignSelf: "stretch", padding: 10}} key={index}>
                      <TouchableOpacity onPress={() => {
                          AsyncStorage.setItem('selectedCurrencyId', item.id + '');
                          global.currency = item;
                          this.setState({selectedCurrencyId: item.id});
                      }}>
                          <View style={{flexDirection: 'row', marginHorizontal: 20}}>
                              <Image
                                  source={{uri: item.photo}}
                                  style={{
                                      width: moderateScale(16),
                                      height: moderateScale(16),
                                      resizeMode: 'contain',
                                  }}
                              />
                              <Text style={{
                                  justifyContent: 'flex-start',
                                  alignSelf: 'center',
                                  paddingRight: 15,
                                  paddingLeft: 15
                              }}>
                                  {item.country + ' (' + item.code + ') '}
                              </Text>
                              {this.state.selectedCurrencyId == item.id ?
                                  <View style={{flex: 1}}>
                                      <Image
                                          source={require('./assets/images/checked.png')}
                                          style={{
                                              flex: 1,
                                              alignSelf: 'flex-end',
                                              width: moderateScale(12),
                                              height: moderateScale(12),
                                              resizeMode: 'contain',
                                              marginTop: verticalScale(0),
                                          }}
                                      />
                                  </View> : null
                              }
                          </View>

                      </TouchableOpacity>
                  </View>
              )
          })}
          </ScrollView>
          <View style={{
              /*position: 'absolute',
              bottom: verticalScale(5),*/
              padding: 10,
              alignSelf: 'center',
          }}>
              <Button
                  onPress={() => this._sortLanguage()}
                  style={[
                      styles.buttonStyle2,
                      {
                          marginTop: verticalScale(2),
                      },
                  ]}>
                  <Text style={styles.buttonTextStyle}>{L('startOver')}</Text>
              </Button>
          </View>
        {this._renderLoading()}
        {this._renderError()}
      </Container>
    );
  }

    /*renderCurrencies() {
        console.log("------------------ this is from component");
        console.log(this.props.currencies);
        if (this.props.currencies) {
            return (
                this.props.currencies.map((item, index) => {
                    (
                        <View style={{alignSelf: "stretch", paddingTop: 5, paddingBottom: 5}} key={index}>
                            <TouchableOpacity onPress={() => {
                                AsyncStorage.setItem('selectedCountry', index + '');
                                global.currency = this.countries[index];
                                this.setState({selectedCountryIndex: index});
                            }}>
                                <View style={{flexDirection: 'row', marginHorizontal: 20}}>
                                    <Image
                                        source={item.photo}
                                        style={{
                                            width: moderateScale(16),
                                            height: moderateScale(16),
                                            resizeMode: 'contain',
                                        }}
                                    />
                                    <Text style={{
                                        justifyContent: 'flex-start',
                                        alignSelf: 'center',
                                        paddingRight: 15,
                                        paddingLeft: 15
                                    }}>
                                        {item.country + ' (' + item.code + ') '}
                                    </Text>
                                    {this.state.selectedCountryIndex == index ?
                                        <View style={{flex: 1}}>
                                            <Image
                                                source={require('./assets/images/checked.png')}
                                                style={{
                                                    flex: 1,
                                                    alignSelf: 'flex-end',
                                                    width: moderateScale(12),
                                                    height: moderateScale(12),
                                                    resizeMode: 'contain',
                                                    marginTop: verticalScale(0),
                                                }}
                                            />
                                        </View> : null
                                    }
                                </View>

                            </TouchableOpacity>
                        </View>
                    )
                })
            )
        } else {
            return null;
        }
    }*/
}



const mapStateToProps = ({others}) => {
  const { loading, currencies} = others;
  // console.log(result);
  return {loading, currencies};
};

export default connect(mapStateToProps, {
  editUser,
  clearMessage,
  showMessageChanged,
    getCurrencies,
})(Language);
