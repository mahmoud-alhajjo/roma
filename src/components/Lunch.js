import React, {Component} from 'react';

import {Image} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {Container, Content, Header} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import {scale, verticalScale, primaryColor} from './assets/styles/Style';

class Lunch extends Component {
  componentDidMount = async () => {
    try {
      const value = await AsyncStorage.getItem('user');
      user = await JSON.parse(value);
      if (user) {
        Actions.reset('intro');
      } else {
        // alert('user fail')
      }
    } catch (e) {}
  };
  render() {
    return (
      <Container>
        <Header
          androidStatusBarColor={primaryColor}
          style={{display: 'none'}}
        />

        <Content>
          <Image
            source={require('./assets/images/splash.png')}
            resizeMode="stretch"
            style={{width: scale(100), height: verticalScale(100)}}
          />
        </Content>
      </Container>
    );
  }
}

export default Lunch;
