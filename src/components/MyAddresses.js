import React, {Component} from 'react';
import {
  Image,
  /*TouchableNativeFeedback,*/
  TouchableWithoutFeedback,
  FlatList,
  TouchableOpacity,
  Alert,
  Modal,
  BackHandler
} from 'react-native';
import {connect} from 'react-redux';
import {Spinner} from './assets/common';
import {Actions} from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import {L} from '../Config';
import styles, {
  verticalScale,
  scale,
  buttonStyle,
  buttonTextStyle,
  moderateScale,
  inputField,
  primaryColor,
  fontBold,
  DroidKufi,
  DroidKufiBold,
  secondaryColor,
  modalContainerlStyle,
  drawerHeadText,
  modalStyle,
  textAreaField,
  noBorder,
} from './assets/styles/Style';
import {
  Container,
  Content,
  Header,
  Form,
  Left,
  Item,
  Input,
  Button,
  Text,
  View,
  Icon,
  Toast,
  Label,
  Right,
  Fab,
  Textarea,
} from 'native-base';
import {getAddress, deleteAddress, addAddress, showMessageChanged} from '../actions';

class MyAddresses extends Component {
  state = {
    language: 'ar',
    user: null,
    active: false,
    modalVisible: false,
    address: '',
      countryAddress: '',
      cityAddress: '',
      districtAddress: '',
      streetAddress: '',
      buildingAddress: '',
  };
  onButtonPress = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    // then navigate
    // navigate('NewScreen');
  };

  handleBackButton = () => {
    Actions.pop();
    return true;
  };

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }
  closeModal() {
    this.setState({
      modalVisible: false,
    });
  }
  async componentDidMount() {
    try {
      let value = await AsyncStorage.getItem('user');
      // console.log(value);
      const user = JSON.parse(value);
      if (user) {
        this.setState({user: user});
        this._getData();
      }
      // console.log(user);
    } catch (error) {
      // console.log(error);
      // this._getData()
    }
  }
  _getData() {
    this.props.getAddress(this.state.user.id);
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    // console.log(this.state.user.id);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }
    onCountryAddressChanged(text) {
        this.setState({countryAddress: text});
    }
    onCityAddressChanged(text) {
        this.setState({cityAddress: text});
    }
    onDistrictAddressChanged(text) {
        this.setState({districtAddress: text});
    }
    onStreetAddressChanged(text) {
        this.setState({streetAddress: text});
    }
    onBuildingAddressChanged(text) {
        this.setState({buildingAddress: text});
    }
    initAddressfields() {
        this.setState({countryAddress: '', cityAddress: '', districtAddress: '', streetAddress: '', buildingAddress: ''});
    }
  onAddAddress() {
      if (this.state.countryAddress == '') {
          this.props.showMessageChanged(L('emptyField') + ' ' + L('country'));
      } else if (this.state.cityAddress == '') {
          this.props.showMessageChanged(L('emptyField') + ' ' + L('city'));
      } else if (this.state.districtAddress == '') {
          this.props.showMessageChanged(L('emptyField') + ' ' + L('district'));
      } else if (this.state.streetAddress == '') {
          this.props.showMessageChanged(L('emptyField') + ' ' + L('street'));
      } else if (this.state.buildingAddress == '') {
          this.props.showMessageChanged(L('emptyField') + ' ' + L('building'));
      } else {
          var address = this.state.countryAddress + ' - ' +
              this.state.cityAddress + ' - ' +
              this.state.districtAddress + ' - ' +
              this.state.streetAddress + ' - ' +
              this.state.buildingAddress;
          const addAddress = {
              client_id: this.state.user.id,
              //address: this.state.address,
              address: address,
              countryAddress: this.state.countryAddress,
              cityAddress: this.state.cityAddress,
              districtAddress: this.state.districtAddress,
              streetAddress: this.state.streetAddress,
              buildingAddress: this.state.buildingAddress,
          };
          this.props.addAddress({addAddress});
          this.setModalVisible(false);
      }
  }
  _renderError() {
    if (this.props.message) {
      this.props.clearMessage();
      return Toast.show({
        text: this.props.message,
        buttonText: L('dismiss'),
        duration: 3000,
        style: {backgroundColor: primaryColor},
        textStyle: {fontFamily: DroidKufi, fontSize: moderateScale(4.5)},
        buttonTextStyle: {fontFamily: DroidKufi, fontSize: moderateScale(4.5)},
      });
    }
  }
  _renderLoading() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }
  }
  deleteAdd(id) {
    this.props.deleteAddress(this.state.user.id, id);
    // console.log(id);
  }
  // _renderItem({ item }) {

  //     return (

  //     )
  // }
  render() {
    return (
      <Container>
        <Header
          androidStatusBarColor={primaryColor}
          style={styles.routernavigationBarStyle}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignSelf: 'center',
              alignItems: 'center',
              width: scale(100),
            }}>
            <Button
              onPress={() => Actions.pop()}
              transparent
              /*background={TouchableNativeFeedback.Ripple(secondaryColor)}*/
              style={styles.headerBack}>
              <Icon
                name="keyboard-backspace"
                type="MaterialIcons"
                style={{
                  color: '#fff',
                  fontSize: 25,
                  transform: [{scaleX: L('scaleLang')}],
                }}
              />
            </Button>
            <Text numberOfLines={1} style={styles.headerText}>
              {this.props.title}
            </Text>
          </View>
        </Header>
        <Content>
          {this.props.addresses.length > 0 ? (
            this.props.addresses.map((item, index) => (
              <View
                style={{
                  flexDirection: 'row',
                  borderBottomColor: '#ddd',
                  borderBottomWidth: 1,
                  padding: scale(5),
                }}
                key={index}>
                <View style={{alignSelf: 'center', marginRight: scale(3)}}>
                  <Icon
                    name="globe"
                    type="Feather"
                    style={{color: primaryColor, fontSize: moderateScale(9)}}
                  />
                </View>
                <View style={{alignItems: 'flex-start', width: scale(75)}}>
                  <Text
                    style={[styles.fontRegular, {fontSize: moderateScale(5)}]}>
                    {item.address}
                  </Text>
                </View>
                <View>
                  <TouchableOpacity
                    onPress={() =>
                      Alert.alert(
                        L('alert'),
                        L('addressDelete'),
                        [
                          {text: L('no'), style: 'cancel'},
                          {
                            text: L('yes'),
                            onPress: () => this.deleteAdd(item.id),
                          },
                        ],
                        {cancelable: false},
                      )
                    }>
                    <Icon
                      name="trash-2"
                      type="Feather"
                      style={{color: '#ff8e93', fontSize: moderateScale(10)}}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            ))
          ) : (
            // <FlatList
            //     data={}
            //     renderItem={this._renderItem}
            //     horizontal={false}
            //     // numColumns={2}
            //     keyExtractor={(item, index) => index.toString()}
            //     extraData={this.state}
            //     // columnWrapperStyle={{ justifyContent: 'space-between' }}
            //     initialNumToRender={4}
            // />
            <View
              style={{
                alignSelf: 'center',
                justifyContent: 'center',
                height: verticalScale(50),
              }}>
              <View
                style={{
                  backgroundColor: '#f9f9f9',
                  alignSelf: 'center',
                  width: scale(60),
                  height: verticalScale(32),
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: moderateScale(100),
                  borderWidth: 0.3,
                  borderColor: primaryColor,
                }}>
                <Icon
                  name="globe"
                  type="Feather"
                  style={{
                    fontSize: moderateScale(35),
                    color: '#ddd',
                  }}
                />
              </View>
              <Text
                style={[
                  styles.fontRegular,
                  {textAlign: 'center', marginTop: verticalScale(3)},
                ]}>
                {L('noNot')} {L('addresses')}
              </Text>
            </View>
          )}
        </Content>
        <View>
          <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.modalVisible}
            onRequestClose={this.closeModal.bind(this)}
            hardwareAccelerated={true}
            presentationStyle="overFullScreen">
            <TouchableWithoutFeedback
              onPress={() => {
                this.setModalVisible(!this.state.modalVisible);
              }}>
              <View style={styles.modalContainerlStyle}>
                <View
                  style={[
                    styles.modalStyle,
                    {
                      width: scale(90),
                      padding: scale(5),
                      justifyContent: 'center',
                    },
                  ]}>
                  <Text
                    style={[
                      styles.fontRegular,
                      {
                        marginBottom: verticalScale(2),
                        textAlign: 'center',
                        color: primaryColor,
                      },
                    ]}>
                    {L('addAddressDetails')}
                  </Text>
                    <Item
                        style={[
                            styles.inputAuthContainer,
                            {
                                borderTopWidth: 0,
                                borderLeftWidth: 0,
                                borderRightWidth: 0,
                            },
                        ]}
                        floatingLabel>
                        <Label style={styles.inputLabel}>{L('country')}</Label>
                        <Input
                            onChangeText={this.onCountryAddressChanged.bind(this)}
                            value={this.state.countryAddress}
                            style={[styles.inputField, {textAlign: L('textAlignRight')}]}
                        />
                    </Item>
                    <Item
                        style={[
                            styles.inputAuthContainer,
                            {
                                borderTopWidth: 0,
                                borderLeftWidth: 0,
                                borderRightWidth: 0,
                            },
                        ]}
                        floatingLabel>
                        <Label style={styles.inputLabel}>{L('city')}</Label>
                        <Input
                            onChangeText={this.onCityAddressChanged.bind(this)}
                            value={this.state.cityAddress}
                            style={[styles.inputField, {textAlign: L('textAlignRight')}]}
                        />
                    </Item>
                    <Item
                        style={[
                            styles.inputAuthContainer,
                            {
                                borderTopWidth: 0,
                                borderLeftWidth: 0,
                                borderRightWidth: 0,
                            },
                        ]}
                        floatingLabel>
                        <Label style={styles.inputLabel}>{L('district')}</Label>
                        <Input
                            onChangeText={this.onDistrictAddressChanged.bind(this)}
                            value={this.state.districtAddress}
                            style={[styles.inputField, {textAlign: L('textAlignRight')}]}
                        />
                    </Item>
                    <Item
                        style={[
                            styles.inputAuthContainer,
                            {
                                borderTopWidth: 0,
                                borderLeftWidth: 0,
                                borderRightWidth: 0,
                            },
                        ]}
                        floatingLabel>
                        <Label style={styles.inputLabel}>{L('street')}</Label>
                        <Input
                            onChangeText={this.onStreetAddressChanged.bind(this)}
                            value={this.state.streetAddress}
                            style={[styles.inputField, {textAlign: L('textAlignRight')}]}
                        />
                    </Item>
                    <Item
                        style={[
                            styles.inputAuthContainer,
                            {
                                borderTopWidth: 0,
                                borderLeftWidth: 0,
                                borderRightWidth: 0,
                            },
                        ]}
                        floatingLabel>
                        <Label style={styles.inputLabel}>{L('building')}</Label>
                        <Input
                            onChangeText={this.onBuildingAddressChanged.bind(this)}
                            value={this.state.buildingAddress}
                            style={[styles.inputField, {textAlign: L('textAlignRight')}]}
                        />
                    </Item>
                  <Item regular style={styles.noBorder}>
                    <Button
                      block
                      style={[styles.buttonStyle2, {width: scale(80)}]}
                      onPress={() => this.onAddAddress()}>
                      <Text style={styles.buttonTextStyle}>
                        {L('addAddress')}
                      </Text>
                    </Button>
                  </Item>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </Modal>
        </View>
        <View>
          <Fab
            active={this.state.active}
            // direction="up"
            containerStyle={{}}
            style={{backgroundColor: primaryColor}}
            position="bottomRight"
            onPress={() => {
                this.initAddressfields();
              this.setModalVisible(true);
            }}>
            <Icon name="add" />
          </Fab>
        </View>
        {this._renderError()}
        {this._renderLoading()}
      </Container>
    );
  }
}

const mapStateToProps = ({others}) => {
  const {loading, message, addresses} = others;
  // console.log(addresses);
  return {loading, message, addresses};
};
export default connect(mapStateToProps, {
  getAddress,
  deleteAddress,
  addAddress,
    showMessageChanged
})(MyAddresses);
