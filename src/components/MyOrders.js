import React, {Component} from 'react';
import {
  Image,
 /* TouchableNativeFeedback,*/
  TouchableWithoutFeedback,
  TouchableOpacity,
  FlatList,
  Linking,
  BackHandler
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {Spinner} from './assets/common';
import {L} from '../Config';
import AsyncStorage from '@react-native-community/async-storage';

import styles, {
  verticalScale,
  scale,
  moderateScale,
  buttonStyle,
  buttonTextStyle,
  textContainer,
  solidText,
  inputField,
  primaryColor,
  secondaryColor,
  fontBold,
  DroidKufi,
  DroidKufiBold,
  brandImage,
  modalContainerlStyle,
  drawerHeadText,
  modalStyle,
  textAreaField,
  noBorder,
} from './assets/styles/Style';
import {
  Container,
  Content,
  Header,
  Item,
  Input,
  Button,
  Text,
  View,
  Icon,
  Toast,
  Textarea,
} from 'native-base';
import {getOrders, getPrice} from '../actions';

class MyOrders extends Component {
  state = {
    searchField: '',
    modalVisible: false,
    message: '',
    user: null,
  };
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }
  closeModal() {
    this.setState({
      modalVisible: false,
    });
  }
  onButtonPress = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    // then navigate
    // navigate('NewScreen');
  };

  handleBackButton = () => {
    Actions.pop();
    return true;
  };
  async componentDidMount() {
    try {
      let value = await AsyncStorage.getItem('user');
      // console.log(value);
      const user = JSON.parse(value);
      if (user) {
        this.setState({user: user});
        this._getData();
      }
      // console.log(user);
    } catch (error) {
      // console.log(error);
      // this._getData()
    }
  }
  _getData() {
    this.props.getOrders(this.state.user.id);
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }
  _renderError() {
    if (this.props.message) {
      this.props.clearMessage();
      return Toast.show({
        text: this.props.message,
        buttonText: L('dismiss'),
        duration: 3000,
        style: {backgroundColor: primaryColor},
        textStyle: {fontFamily: DroidKufi, fontSize: moderateScale(4.5)},
        buttonTextStyle: {fontFamily: DroidKufi, fontSize: moderateScale(4.5)},
      });
    }
  }
  _renderItem({item}) {
    return (
      <TouchableOpacity
        onPress={() =>
          Actions.push('orderPage', {orderID: item.id, title: '#' + item.id, fromPageMyOrder: true})
        }
        style={{
          flexDirection: 'row',
          backgroundColor: '#fefdfd',
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 3,
          },
          shadowOpacity: 0.27,
          shadowRadius: 4.65,
          elevation: 6,
          // width: scale(90),
          // padding: scale(5),
          marginBottom: verticalScale(2),
          alignSelf: 'center',
          borderRadius: moderateScale(3),
          marginTop: verticalScale(1),
        }}>
        <View
          style={{
            alignItems: 'center',
            alignSelf: 'center',
            justifyContent: 'space-between',
            marginBottom: verticalScale(1),
            flexDirection: 'row',
            width: scale(90),
            padding: scale(5),
          }}>
          <View
            style={{
              width: scale(21),
              height: verticalScale(11),
              borderWidth: 0.3,
              borderColor: secondaryColor,
              justifyContent: 'center',
            }}>
            <Text
              style={[
                styles.fontRegular,
                {color: primaryColor, textAlign: 'center', fontSize: moderateScale(8.5)},
              ]}>
              {'#' + item.id}
            </Text>
          </View>
          <View style={{width: scale(34), marginHorizontal:scale(2) ,alignItems: 'flex-start'}}>
            <Text style={[styles.fontRegular, {color: '#cbcbcb'}]}>
              {item.order_date}
            </Text>
            <Text
              style={[
                styles.fontRegular,
                {color: primaryColor, marginVertical: verticalScale(1)},
              ]}>
              {L('order_' + item.state)}
            </Text>
            <Text
              style={[
                styles.fontRegular,
                {
                  fontSize: moderateScale(4.2),
                  backgroundColor: primaryColor,
                  color: '#fff',
                  paddingHorizontal: scale(2),
                  borderRadius: scale(5),
                },
              ]}>
              {L('pay_' + item.pay)}
            </Text>
          </View>
          <View style={{flexDirection: 'column'}}>
            <Text
              style={[
                styles.fontRegular,
                {textAlign: 'center', fontSize: moderateScale(5)},
              ]}>
              {L('total')}
            </Text>
            <Text
              style={[
                styles.fontRegular,
                {color: primaryColor, fontSize: moderateScale(4.5)},
              ]}>
              {(item.pay === 9 ? 0 : getPrice(item.total)) + ' ' + L(global.currency.code)}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
  _renderLoading() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }
  }
  render() {
    return (
      <Container>
        <Header
          androidStatusBarColor={primaryColor}
          style={styles.routernavigationBarStyle}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignSelf: 'center',
              alignItems: 'center',
              width: scale(100),
            }}>
            <Button
              onPress={() => Actions.pop()}
              transparent
              /*background={TouchableNativeFeedback.Ripple(secondaryColor)}*/
              style={styles.headerBack}>
              <Icon
                name="keyboard-backspace"
                type="MaterialIcons"
                style={{
                  color: '#fff',
                  fontSize: 25,
                  transform: [{scaleX: L('scaleLang')}],
                }}
              />
            </Button>
            <Text numberOfLines={1} style={styles.headerText}>
              {this.props.title}
            </Text>
          </View>
        </Header>
        <Content padder>
          {this.props.orders && this.props.orders.length > 0 ? (
            <FlatList
              data={this.props.orders}
              renderItem={this._renderItem}
              horizontal={false}
              // numColumns={2}
              keyExtractor={(item, index) => index.toString()}
              extraData={this.state}
              // columnWrapperStyle={{ justifyContent: 'space-between' }}
              initialNumToRender={4}
            />
          ) : (
            <View
              style={{
                alignSelf: 'center',
                justifyContent: 'center',
                height: verticalScale(50),
              }}>
              <View
                style={{
                  backgroundColor: '#f9f9f9',
                  alignSelf: 'center',
                  width: scale(60),
                  height: verticalScale(32),
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: moderateScale(100),
                  borderWidth: 0.3,
                  borderColor: primaryColor,
                }}>
                <Icon
                  name="box"
                  type="Feather"
                  style={{
                    fontSize: moderateScale(35),
                    color: '#ddd',
                  }}
                />
              </View>
              <Text
                style={[
                  styles.fontRegular,
                  {textAlign: 'center', marginTop: verticalScale(3)},
                ]}>
                {L('noNot') + ' ' + L('orders')}
              </Text>
            </View>
          )}
        </Content>
        {this._renderError()}
        {this._renderLoading()}
      </Container>
    );
  }
}
const mapStateToProps = ({ordersR}) => {
  const {loading, message, orders} = ordersR;
  // console.log(catsByParent);
  return {loading, message, orders};
};
export default connect(mapStateToProps, {
  getOrders,
})(MyOrders);
