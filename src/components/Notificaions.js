import React, {Component} from 'react';
import {
  TouchableWithoutFeedback,
  /*TouchableNativeFeedback,*/
  FlatList,
  BackHandler
} from 'react-native';
import {connect} from 'react-redux';
import {Spinner} from './assets/common';
import {Actions} from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import {L} from '../Config';
import styles, {
  verticalScale,
  scale,
  buttonStyle,
  buttonTextStyle,
  moderateScale,
  inputField,
  primaryColor,
  fontBold,
  DroidKufi,
  DroidKufiBold,
  secondaryColor,
} from './assets/styles/Style';
import {
  Container,
  Content,
  Header,
  Form,
  Left,
  Item,
  Input,
  Button,
  Text,
  View,
  Icon,
  Toast,
  Label,
  Right,
} from 'native-base';
import {getNotifications} from '../actions';

class Notificaions extends Component {
  state = {
    language: 'ar',
    user: null,
  };
  onButtonPress = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    // then navigate
    // navigate('NewScreen');
  };

  handleBackButton = () => {
    Actions.pop();
    return true;
  };
  async componentDidMount() {
    try {
      let value = await AsyncStorage.getItem('user');
      // console.log(value);
      const user = JSON.parse(value);
      if (user) {
        this.setState({user: user});
        this._getData();
      }
      // console.log(user);
    } catch (error) {
      // console.log(error);
      // this._getData()
    }
  }
  _getData() {
    this.props.getNotifications(this.state.user.id);
    // console.log(this.state.user.id);
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }
  _renderError() {
    if (this.props.message) {
      this.props.clearMessage();
      return Toast.show({
        text: this.props.message,
        buttonText: L('dismiss'),
        duration: 3000,
        style: {backgroundColor: primaryColor},
        textStyle: {fontFamily: DroidKufi, fontSize: moderateScale(4.5)},
        buttonTextStyle: {fontFamily: DroidKufi, fontSize: moderateScale(4.5)},
      });
    }
  }
  _renderLoading() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }
  }
  _renderItem({item}) {
    return (
      <TouchableWithoutFeedback
        onPress={() => {
            if (item.type == 7) {
                Actions.push('productsPage', {title: item.title, brandId: item.item_id})
            } else if (item.type == 8) {
                Actions.push('videosPage');
            } else {
                Actions.push('orderPage', {orderID: item.order_id, title: '#' + item.order_id});
            }
            console.log(item);
        }
        }>
        <View
          style={{
            flexDirection: 'row',
            borderBottomColor: '#ddd',
            borderBottomWidth: 1,
            padding: scale(5),
          }}>
          <View style={{alignSelf: 'center', marginRight: scale(3)}}>
            <Icon
              name="exclamation"
              type="SimpleLineIcons"
              style={{color: primaryColor, fontSize: moderateScale(10)}}
            />
          </View>
          <View style={{alignItems: 'flex-start'}}>
            <Text style={[styles.fontRegular, {fontSize: moderateScale(6)}]}>
              {item.title}
            </Text>
            <Text style={styles.fontRegular}>{item.message}</Text>
            <Text style={[styles.fontRegular, {color: '#c4c4c4'}]}>
              {item.date}
            </Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
  render() {
    return (
      <Container>
        <Header
          androidStatusBarColor={primaryColor}
          style={styles.routernavigationBarStyle}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignSelf: 'center',
              alignItems: 'center',
              width: scale(100),
            }}>
            <Button
              onPress={() => Actions.pop()}
              transparent
              /*background={TouchableNativeFeedback.Ripple(secondaryColor)}*/
              style={styles.headerBack}>
              <Icon
                name="keyboard-backspace"
                type="MaterialIcons"
                style={{
                  color: '#fff',
                  fontSize: 25,
                  transform: [{scaleX: L('scaleLang')}],
                }}
              />
            </Button>
            <Text numberOfLines={1} style={styles.headerText}>
              {this.props.title}
            </Text>
          </View>
        </Header>
        <Content>
          {this.props.notifications.length > 0 ? (
            <FlatList
              data={this.props.notifications}
              renderItem={this._renderItem}
              horizontal={false}
              // numColumns={2}
              keyExtractor={(item, index) => index.toString()}
              extraData={this.state}
              // columnWrapperStyle={{ justifyContent: 'space-between' }}
              initialNumToRender={4}
            />
          ) : (
            <View
              style={{
                alignSelf: 'center',
                justifyContent: 'center',
                height: verticalScale(50),
              }}>
              <View
                style={{
                  backgroundColor: '#f9f9f9',
                  alignSelf: 'center',
                  width: scale(60),
                  height: verticalScale(32),
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: moderateScale(100),
                  borderWidth: 0.3,
                  borderColor: primaryColor,
                }}>
                <Icon
                  name="bell"
                  type="Feather"
                  style={{
                    fontSize: moderateScale(35),
                    color: '#ddd',
                  }}
                />
              </View>
              <Text
                style={[
                  styles.fontRegular,
                  {textAlign: 'center', marginTop: verticalScale(3)},
                ]}>
                {L('noNot')} {L('Notifications')}
              </Text>
            </View>
          )}
        </Content>
        {this._renderError()}
        {this._renderLoading()}
      </Container>
    );
  }
}

const mapStateToProps = ({others}) => {
  const {loading, message, notifications} = others;
  // console.log(notifications);
  return {loading, message, notifications};
};
export default connect(mapStateToProps, {
  getNotifications,
})(Notificaions);
