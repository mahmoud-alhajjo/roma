import React, {Component} from 'react';
import {Text} from "react-native";

class OfferTimer extends Component {
  constructor(props) {
      super();
      this.state = {
          day: 0,
          hours: 0,
          minutes: 0,
          seconds: 0,
          interval: null
      };
      var intervalFunction = setInterval(() => {
          let duration = new Date(Date.parse(props.specialEndDate)).valueOf() - new Date().valueOf();
          this.setState({day: Math.trunc(duration / (24 * 60 * 60 * 1000))})
          duration -= (this.state.day * 24 * 60 * 60 * 1000);
          this.setState({hours: Math.trunc(duration / (60 * 60 * 1000))});
          duration -= (this.state.hours * 60 * 60 * 1000);
          this.setState({minutes: Math.trunc(duration / (60 * 1000))});
          duration -= (this.state.minutes * 60 * 1000);
          this.setState({seconds: Math.trunc(duration / (1000))});
      }, 1000);
      this.state.interval = intervalFunction;
  }
  componentWillUnmount () {
      if (this.state.interval) {
          clearInterval(this.state.interval);
      }
  }

  render() {
      if (this.state.day > 0 || this.state.hours > 0 || this.state.minutes > 0 || this.state.seconds > 0) {
          return (
              <Text style={{textAlign: 'center'}}>
                    {this.state.day < 10 ? ('0' + this.state.day) : this.state.day}
                  : {this.state.hours < 10 ? ('0' + this.state.hours) : this.state.hours}
                  : {this.state.minutes < 10 ? ('0' + this.state.minutes) : this.state.minutes}
                  : {this.state.seconds < 10 ? ('0' + this.state.seconds) : this.state.seconds}
              </Text>
          );
      } else {
          return null;
      }
  }
}
export default OfferTimer;