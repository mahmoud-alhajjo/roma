import React, { Component } from 'react';
import {
    FlatList,
    BackHandler, Dimensions, StyleSheet,
    TouchableWithoutFeedback
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { Spinner } from './assets/common';
import { L } from '../Config';
import FastImage from 'react-native-fast-image';
import styles, {
    scale,
    moderateScale,
    primaryColor,
    secondaryColor,
    DroidKufi,
    DroidKufiBold,
} from './assets/styles/Style';
import {
    Container,
    Content,
    Header,
    Button,
    Text,
    View,
    Icon,
    Toast,
} from 'native-base';
import { AirbnbRating } from "react-native-elements";
import { getDiscountedProducts, clearMessage, getPrice } from '../actions';
import OfferTimer from "./OfferTimer";

const styles2 = StyleSheet.create({
    noNot: {
        fontFamily: DroidKufiBold,
        fontSize: moderateScale(5.5),
        color: secondaryColor,
        alignSelf: 'center',
    }
});
class OffersPage extends Component {

    state = {
        offersBrand: this.props.disProducts.length > 0 && this.props.disProducts.filter(item => this.props.catId ? item.category_id === this.props.id : item.brand_id === this.props.id),
    };

    onButtonPress = () => {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    };

    handleBackButton = () => {
        Actions.pop();
        return true;
    };
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        this.props.getDiscountedProducts();
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    _renderDiscountOfferItem = ({ item }) => {
        return (
            <View>
                <View>
                    <OfferTimer specialEndDate={item.specialEndDate} />
                </View>
                <TouchableWithoutFeedback
                    onPress={() =>
                        Actions.push('product', { productID: item.id, title: item.title })
                    }>
                    <View style={[styles.productContainer, { position: 'relative' }]}>
                        <Text
                            style={[
                                styles.fontRegular,
                                {
                                    position: 'absolute',
                                    top: 0,
                                    right: 0,
                                    backgroundColor: primaryColor,
                                    color: '#fff',
                                    zIndex: 11,
                                    paddingHorizontal: scale(2),
                                },
                            ]}>
                            {L('discount')} {item.sale} %
                            </Text>
                        {(item.category.offer || (item.brand && item.brand.offer)) ? <Text
                            style={[
                                styles.fontRegular,
                                {
                                    position: 'absolute',
                                    top: 0,
                                    left: 0,
                                    backgroundColor: "red",
                                    color: '#fff',
                                    zIndex: 11,
                                    paddingHorizontal: scale(2),
                                },
                            ]}>
                            {item.category.offer ?
                                `${item.category.offer.condition} + ${item.category.offer.result} ${L('free')}` :
                                `${item.brand.offer.condition} + ${item.brand.offer.result} ${L('free')}`
                            }

                        </Text> : null}
                        <View>
                            <FastImage
                                source={{
                                    uri: item.photos[0] && item.photos[0].thumb,
                                    priority: FastImage.priority.normal,
                                }}
                                style={styles.productImage}
                                resizeMode={FastImage.resizeMode.contain}
                            />
                        </View>
                        <View style={styles.productDetails}>
                            <Text
                                numberOfLines={1}
                                style={[styles.fontRegular, { color: primaryColor }]}>
                                {item.title}
                            </Text>
                            <AirbnbRating
                                showRating={false}
                                isDisabled
                                defaultRating={item.rating}
                                ratingColor="#f4b700"
                                size={10}
                                starContainerStyle={{ marginLeft: 0 }}
                            />
                            <Text style={styles.fontRegular}>
                                {getPrice(item.newPrice)} {L(global.currency.code)}
                            </Text>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        );
    };

    _renderError = () => {
        if (this.props.message) {
            this.props.clearMessage();
            return Toast.show({
                text: this.props.message,
                buttonText: L('dismiss'),
                duration: 3000,
                style: { backgroundColor: primaryColor },
                textStyle: { fontFamily: DroidKufi, fontSize: moderateScale(4.5) },
                buttonTextStyle: { fontFamily: DroidKufi, fontSize: moderateScale(4.5) },
            });
        }
    }
    _renderLoading = () => {
        if (this.props.loading) {
            return <Spinner size="large" />;
        }
    };

    render() {
        return (
            <Container>
                <Header
                    androidStatusBarColor={primaryColor}
                    style={styles.routernavigationBarStyle}>
                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'flex-start',
                            alignSelf: 'center',
                            alignItems: 'center',
                            width: scale(100),
                        }}>
                        <Button
                            onPress={() => Actions.pop()}
                            transparent
                            style={styles.headerBack}>
                            <Icon
                                name="keyboard-backspace"
                                type="MaterialIcons"
                                style={{
                                    color: '#fff',
                                    fontSize: 25,
                                    transform: [{ scaleX: L('scaleLang') }],
                                }}
                            />
                        </Button>
                        <Text numberOfLines={1} style={styles.headerText}>
                            {this.props.title}
                        </Text>
                    </View>
                </Header>

                {(!this.props.loading && this.state.offersBrand.length > 0) ?
                    <Content>
                        <FlatList
                            data={this.state.offersBrand}
                            renderItem={this._renderDiscountOfferItem}
                            horizontal={false}
                            numColumns={2}
                            extraData={this.state}
                            keyExtractor={(item, index) => index.toString()}
                            columnWrapperStyle={{ justifyContent: 'space-between' }}
                            initialNumToRender={4}
                        />
                    </Content>
                    : <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                        <Text style={styles2.noNot}>{L('noNot')}</Text>
                    </View>
                }
                {this._renderError()}
                {this._renderLoading()}
            </Container>
        );
    }
}
const mapStateToProps = ({ productsR }) => {
    const { disProducts, loading, message } = productsR;
    return { loading, message, disProducts };
};
export default connect(mapStateToProps, {
    getDiscountedProducts,
    clearMessage,
})(OffersPage);
