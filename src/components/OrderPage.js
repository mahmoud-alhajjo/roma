import React, { Component } from 'react';
import {
  Image,
  /*TouchableNativeFeedback,*/
  TouchableWithoutFeedback,
  TouchableOpacity,
  FlatList,
  Linking,
  Modal,
  BackHandler,
  StyleSheet,
  TouchableHighlight,
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { Spinner } from './assets/common';
import { L } from '../Config';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image';
import styles, {
  verticalScale,
  scale,
  moderateScale,
  buttonStyle,
  buttonTextStyle,
  textContainer,
  solidText,
  inputField,
  primaryColor,
  secondaryColor,
  fontBold,
  DroidKufi,
  DroidKufiBold,
  brandImage,
  modalContainerlStyle,
  drawerHeadText,
  modalStyle,
  textAreaField,
  noBorder,
} from './assets/styles/Style';
import {
  Container,
  Content,
  Header,
  Footer,
  Item,
  Input,
  Button,
  Text,
  View,
  Icon,
  Toast,
  Textarea,
} from 'native-base';
import { AirbnbRating } from 'react-native-ratings';
import { getOrder, addComment, getPrice, getWheelItems, giftToUser } from '../actions';
import WheelOfFortune from './assets/common/WheelOfFortune'

class OrderPage extends Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
    this.state = {
      searchField: '',
      modalVisible: false,
      message: '',
      user: null,
      rate: 0,
      comment: '',
      product_id: 0,
      winnerValue: '',
      winnerIndex: '',
      modalGiftVisible: false,
      shouldDisplayWheelOfFortune: true,
      language: ''
    };
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  closeModal() {
    this.setState({
      modalVisible: false,
    });
  }
  closeModalGift() {
    this.setState({
      modalGiftVisible: false,
      shouldDisplayWheelOfFortune: false
    });
  }
  onButtonPress = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    // then navigate
    // navigate('NewScreen');
  };

  handleBackButton = () => {
    Actions.pop();
    return true;
  };
  async componentDidMount() {
    this.props.getWheelItems()
    try {
      let value = await AsyncStorage.getItem('user');
      let lang = await AsyncStorage.getItem('language');
      const user = JSON.parse(value);
      if (user) {
        this.setState({ user: user, language: lang });
        this._getData();
      }
      // console.log(user);
    } catch (error) {
      // console.log(error);
      // this._getData()
    }
  }
  _getData() {
    // console.log(this.state.product_id);
    this.props.getOrder(this.props.orderID);
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }
  onAddRate() {
    const addComment = {
      user_id: this.state.user.id,
      product_id: this.state.product_id,
      comment: this.state.comment,
      rating: this.state.rate,
    };
    // console.log({addComment});

    this.props.addComment({ addComment });
    this.setModalVisible(false);
  }
  matchNameRewards(wheelItems, itemsWheelType) {
    let nameWheelItems = []
    wheelItems.map(item => {
      itemsWheelType.map(itemsWheel => {
        if (itemsWheel.type === item.type) {
          nameWheelItems.push(itemsWheel.name)
        }
      })
    })
    return nameWheelItems
  }
  _renderError() {
    if (this.props.message || this.props.othersMessage) {
      this.props.clearMessage();
      return Toast.show({
        text: this.props.message || this.props.othersMessage,
        buttonText: L('dismiss'),
        duration: 3000,
        style: { backgroundColor: primaryColor },
        textStyle: { fontFamily: DroidKufi, fontSize: moderateScale(4.5) },
        buttonTextStyle: { fontFamily: DroidKufi, fontSize: moderateScale(4.5) },
      });
    } else if (this.props.error) {
      this.props.clearMessage();
      return Toast.show({
        text: `${L('errorGift')}: ${this.props.error}`,
        buttonText: L('dismiss'),
        duration: 3000,
        style: { backgroundColor: primaryColor },
        textStyle: { fontFamily: DroidKufi, fontSize: moderateScale(4.5) },
        buttonTextStyle: { fontFamily: DroidKufi, fontSize: moderateScale(4.5) },
      });
    }
  }
  _renderItem = (item, payId) => {
    item = item.item;
    // const payCheck = this.props.singleOrder.pay
    return (
      <View
        // onPress={() => Actions.push('product', { productID: item.id })}
        style={{
          flexDirection: 'row',
          backgroundColor: '#fefdfd',
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 3,
          },
          shadowOpacity: 0.27,
          shadowRadius: 4.65,

          elevation: 6,
          width: scale(90),
          padding: scale(5),
          marginBottom: verticalScale(2),
          alignSelf: 'center',
          borderRadius: moderateScale(3),
          marginTop: verticalScale(1),
          alignSelf: 'center',
          alignItems: 'center',
        }}>
        {(item.price === 0 && !item.is_gift) &&
          <View style={style2.stripNewProduct}>
            <Text style={style2.textNewProduct}>{item.quantity} {L('free')}</Text>
            <View style={style2.triangleCorner}></View>
          </View>}
        {item.is_gift &&
          <View style={style2.stripNewProduct}>
            <Text style={style2.textNewProduct}>{L('gift')}</Text>
            <View style={[style2.triangleCorner, { borderTopColor: '#f9c74f' }]}></View>
          </View>}
        <View
          style={{
            backgroundColor: '#fff',
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 1,
            },
            shadowOpacity: 0.2,
            shadowRadius: 1.41,
            elevation: 2,
            alignItems: 'center',
            alignSelf: 'center',
            justifyContent: 'center',
            marginBottom: verticalScale(1),
          }}>
          <FastImage
            source={{ uri: item.product_photo }}
            style={{ height: verticalScale(10), width: scale(20) }}
            resizeMode={FastImage.resizeMode.contain}
          />
        </View>
        <View
          style={{
            marginLeft: moderateScale(3),
            alignSelf: 'center',
            width: scale(43),
            alignItems: 'flex-start',
          }}>
          <Text
            numberOfLines={1}
            style={[
              styles.fontRegular,
              { color: primaryColor, fontSize: moderateScale(7) },
            ]}>
            {item.product_name}
          </Text>
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.fontRegular}>{L('qqty') + ': '}</Text>
            <Text
              style={[
                styles.fontRegular,
                { color: primaryColor, fontSize: moderateScale(5) },
              ]}>
              {item.quantity}
            </Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            {item.size_right_details !== null ? (
              <View style={{ flexDirection: 'row' }}>
                <Text style={styles.fontRegular}>{L('right') + ': '}</Text>
                <Text
                  style={[
                    styles.fontRegular,
                    { color: primaryColor, fontSize: moderateScale(5) },
                  ]}>
                  {item.size_right_details && item.size_right_details.size}
                </Text>
              </View>
            ) : null}
            {item.size_left_details !== null ? (
              <View style={{ flexDirection: 'row' }}>
                <Text style={styles.fontRegular}>{L('left') + ': '}</Text>
                <Text
                  style={[
                    styles.fontRegular,
                    { color: primaryColor, fontSize: moderateScale(5) },
                  ]}>
                  {item.size_left_details && item.size_left_details.size}
                </Text>
              </View>
            ) : null}
          </View>
          <View style={{ flexDirection: 'row' }}>
            {/* <Text style={styles.fontRegular}>{L('price')}</Text> */}
            <Text
              style={[
                styles.fontRegular,
                { color: primaryColor, fontSize: moderateScale(5) },
              ]}>
              {payId === 9 ?
                `${item.price} ${L('point')}`
                :
                `${getPrice(item.price)} ${L(global.currency.code)}`
              }
            </Text>
          </View>
        </View>
        {this.props.singleOrder && this.props.singleOrder.state == 4 ? (
          <TouchableOpacity
            onPress={() => {
              this.setModalVisible(true);
              this.setState({ product_id: item.product_id });
            }}>
            <Text style={[styles.fontBold, { color: primaryColor }]}>
              {L('rateProduct')}
            </Text>
          </TouchableOpacity>
        ) : null}
      </View>
    );
  };
  _renderLoading() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }
  }

  render() {
    const itemsWheelType = [
      { type: 'product', name: L('product') },
      { type: 'coupon', name: L('acoupon') },
      { type: 'points', name: L('apoints') },
      { type: 'no_gift', name: L('goodLuke') }
    ]
    var item = this.props.singleOrder;
    console.log("order details");
    console.log(item);
    var delivery = item['shipping'];
    /*if (global.currency && global.currency.delivery) {
        delivery = global.currency.delivery;
    }*/
    var vat = this.props.vat;
    if (global.currency && global.currency.vat) {
      vat = global.currency.vat;
    }
    var order_details = this.props.order_details;
    var total = 0;
    total = item.subtotal;
    /*if (order_details.length) {
      var total = 0;
      // totalWithDelivery = 0;
      for (var i = 0; i < order_details.length; i++) {
        // console.log(cart[i]);
        total =
          Number(total) +
          Number(order_details[i].quantity) * Number(order_details[i].price);
        // var totalWithDelivery = Number(total) + Number(delivery) + Number(vat);
      }
    }*/
    return (
      <Container>
        <Header
          androidStatusBarColor={primaryColor}
          style={styles.routernavigationBarStyle}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignSelf: 'center',
              alignItems: 'center',
              width: scale(100),
            }}>
            {this.props.paymentDone == true ? (
              <Button
                onPress={() => Actions.jump('home')}
                transparent
                /*background={TouchableNativeFeedback.Ripple(secondaryColor)}*/
                style={styles.headerBack}>
                <Icon
                  name="keyboard-backspace"
                  type="MaterialIcons"
                  style={{
                    color: '#fff',
                    fontSize: 25,
                    transform: [{ scaleX: L('scaleLang') }],
                  }}
                />
              </Button>
            ) : (
                <Button
                  onPress={() => Actions.pop()}
                  transparent
                  /*background={TouchableNativeFeedback.Ripple(secondaryColor)}*/
                  style={styles.headerBack}>
                  <Icon
                    name="keyboard-backspace"
                    type="MaterialIcons"
                    style={{
                      color: '#fff',
                      fontSize: 25,
                      transform: [{ scaleX: L('scaleLang') }],
                    }}
                  />
                </Button>
              )}

            <Text numberOfLines={1} style={styles.headerText}>
              #{this.props.singleOrder.id}
            </Text>
          </View>
        </Header>

        {(this.props.wheelItems && this.props.wheelItems.length > 0 && !this.props.loading && this.state.shouldDisplayWheelOfFortune && !this.state.modalGiftVisible && !this.props.fromPageMyOrder && this.props.paymentDone) ? <View style={{
          position: 'absolute',
          top: 0,
          bottom: 0,
          zIndex: 999,
          width: scale(100),
          height: verticalScale(100),
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          alignContent: 'center',
          backgroundColor: 'rgba(52, 52, 52, 0.8)',
          elevation: scale(100),
          flex: 1,
          flexGrow: 10,
          flexShrink: 10
        }}>
          {/* {console.log('this.props.wheelItems', this.props.wheelItems)} */}
          <WheelOfFortune
            // onRef={ref => (this.child = ref)}
            onRef={ref => this.myRef = ref}
            rewards={this.matchNameRewards(this.props.wheelItems, itemsWheelType)}
            // rewards={['product', 'copoun', 'good luck', 'points', 'good luck', 'points', 'test' , 'copoun', 'good luck', 'points', 'good luck', 'points', 'test']}
            // rewards={['منتج', 'نقاط', 'كوبون', 'حظ أوفر','هدية', 'كوبون', 'حظ أوفر', 'منتج', 'نقاط', 'كوبون', 'حظ أوفر','هدية', 'كوبون', 'حظ أوفر']}
            // colors={['#ffa69e', '#ff686b']}
            language={this.state.language}
            duration={3000}
            knobSize={17}
            borderWidth={scale(1)}
            borderColor={"#84dcc6"}
            innerRadius={scale(10)}
            textColor={"#FFF"}
            backgroundColor={'#84dcc6'}
            getWinner={(value, index) => {
              let matchValue = itemsWheelType.filter(itemsWheel => itemsWheel.name === value.toString())
              let winner = this.props.wheelItems.filter((item, inx) => matchValue[0].type === item.type && index === inx)
              winner.length > 0 && this.props.giftToUser(this.state.user.id, winner[0].id)
              this.setState({ winnerValue: winner, winnerIndex: value, modalGiftVisible: true })
            }}
            playButton={() => {
              return (
                <Text style={{
                  color: primaryColor,
                  fontFamily: DroidKufi,
                  fontSize: moderateScale(9),
                  alignSelf: 'center',
                  alignContent: 'center',
                  paddingTop: verticalScale(4)
                }}>{L('start')}</Text>
              )
            }}
          />
        </View> : null}

        <Content padder>
          {this.props.order_details && this.props.order_details.length > 0 ? (
            <FlatList
              data={this.props.order_details}
              renderItem={(item) => this._renderItem(item, this.props.singleOrder.pay)}
              horizontal={false}
              // numColumns={2}
              keyExtractor={(item, index) => index.toString()}
              extraData={this.state}
              // columnWrapperStyle={{ justifyContent: 'space-between' }}
              initialNumToRender={4}
            />
          ) : (
              <View
                style={{
                  alignSelf: 'center',
                  justifyContent: 'center',
                  height: verticalScale(50),
                }}>
                <View
                  style={{
                    backgroundColor: '#f9f9f9',
                    alignSelf: 'center',
                    width: scale(60),
                    height: verticalScale(32),
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: moderateScale(100),
                    borderWidth: 0.3,
                    borderColor: primaryColor,
                  }}>
                  <Icon
                    name="box"
                    type="Feather"
                    style={{
                      fontSize: moderateScale(35),
                      color: '#ddd',
                    }}
                  />
                </View>
                <Text
                  style={[
                    styles.fontRegular,
                    { textAlign: 'center', marginTop: verticalScale(3) },
                  ]}>
                  {L('noNot') + ' ' + L('orders')}
                </Text>
              </View>
            )}
          {this.props.singleOrder.pay == 3 ? (
            <View style={{ justifyContent: 'center', alignSelf: 'center' }}>
              <Text
                style={[
                  styles.fontRegular,
                  { textAlign: 'center', marginBottom: verticalScale(2) },
                ]}>
                {L('receiptPhoto')}
              </Text>
              <Image
                source={{ uri: this.props.singleOrder.receipt_photo }}
                style={{ height: verticalScale(40), width: scale(80) }}
                resizeMode="contain"
              />
            </View>
          ) : null}
        </Content>
        <View>
          <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.modalVisible}
            onRequestClose={this.closeModal.bind(this)}
            hardwareAccelerated={true}
            presentationStyle="overFullScreen">
            <TouchableWithoutFeedback
              onPress={() => {
                this.setModalVisible(!this.state.modalVisible);
              }}>
              <View style={styles.modalContainerlStyle}>
                <View
                  style={[
                    styles.modalStyle,
                    {
                      width: scale(90),
                      padding: scale(5),
                      justifyContent: 'center',
                    },
                  ]}>
                  <Text
                    style={[
                      styles.fontRegular,
                      {
                        marginBottom: verticalScale(1),
                        textAlign: 'center',
                        color: primaryColor,
                      },
                    ]}>
                    {L('rateProduct')}
                  </Text>
                  <AirbnbRating
                    showRating={false}
                    // isDisabled
                    defaultRating={3}
                    onFinishRating={text => this.setState({ rate: text })}
                    ratingColor="#f4b700"
                    size={30}
                    starContainerStyle={{ marginLeft: 0 }}
                  />
                  <Item
                    style={[
                      styles.inputAuthContainer,
                      {
                        borderRadius: moderateScale(5),
                        marginBottom: verticalScale(0),
                        width: scale(80),
                        marginTop: verticalScale(2),
                      },
                    ]}>
                    <Textarea
                      rowSpan={5}
                      onChangeText={text => this.setState({ comment: text })}
                      placeholder={L('writeComment')}
                      value={this.state.comment}
                      style={styles.textAreaField}
                      placeholderTextColor="#a2a2a2"
                    />
                  </Item>
                  <Item regular style={styles.noBorder}>
                    <Button
                      block
                      style={[styles.buttonStyle2, { width: scale(80) }]}
                      onPress={() => this.onAddRate(item.product_id)}>
                      <Text style={styles.buttonTextStyle}>{L('send')}</Text>
                    </Button>
                  </Item>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </Modal>
        </View>
        <Footer
          style={{
            // paddingTop: verticalScale(1),
            minHeight: item.pay === 9 ? verticalScale(43) : verticalScale(40),
            alignSelf: 'center',
            alignItems: 'center',
            // marginBottom: verticalScale(11),
            flexDirection: 'column',
            backgroundColor: '#F9F9F9',
            borderTopWidth: 1,
            borderTopColor: '#f9f9f9',
            width: scale(100),
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.23,
            shadowRadius: 2.62,
            elevation: 4,
          }}>
          <View
            style={{
              // shadowColor: "#000",
              // shadowOffset: {
              //     width: 0,
              //     height: 2,
              // },
              // shadowOpacity: 0.23,
              // shadowRadius: 2.62,
              // elevation: 4,
              // backgroundColor: '#fff',
              width: scale(90),
              flexDirection: 'column',
              paddingHorizontal: scale(5),
              borderRadius: moderateScale(5),
            }}>
            {/* <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                borderBottomColor: '#ddd',
                borderBottomWidth: 1,
                paddingVertical: verticalScale(1.3),
              }}>
              <Text style={styles.fontRegular}>{L('total')}</Text>
              <Text style={styles.fontRegular}>
                {item.total} {L('SAR')}
              </Text>
            </View> */}
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                borderBottomColor: '#ddd',
                borderBottomWidth: 1,
                paddingVertical: verticalScale(1.3),
              }}>
              <Text style={styles.fontRegular}>{L('address')}</Text>
              <Text style={styles.fontRegular}>
                {item.address && item.address.address}
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                borderBottomColor: '#ddd',
                borderBottomWidth: 1,
                paddingVertical: verticalScale(1.3),
              }}>
              <Text style={styles.fontRegular}>{L('sum')}</Text>
              <Text style={styles.fontRegular}>
                {item.pay === 9 ?
                  `${total + (item.couponValue == null ? 0 : item.couponValue)} ${L('point')}`
                  :
                  `${getPrice(total + (item.couponValue == null ? 0 : item.couponValue))} ${L(global.currency.code)}`
                }
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                borderBottomColor: '#ddd',
                borderBottomWidth: 1,
                paddingVertical: verticalScale(1.3),
              }}>
              <Text style={styles.fontRegular}>{L('coupon')}</Text>
              <Text style={styles.fontRegular}>
                {item.pay === 9 ?
                  `${item.couponValue} ${L('point')}`
                  :
                  `${getPrice(item.couponValue)} ${L(global.currency.code)}`
                }
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                borderBottomColor: '#ddd',
                borderBottomWidth: 1,
                paddingVertical: verticalScale(1.3),
              }}>
              <Text style={styles.fontRegular}>{L('delivery')}</Text>
              <Text style={styles.fontRegular}>
                {item.pay === 9 ?
                  `${delivery} ${L('point')}`
                  :
                  `${getPrice(delivery)} ${L(global.currency.code)}`
                }
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                borderBottomColor: '#ddd',
                borderBottomWidth: 1,
                paddingVertical: verticalScale(1.3),
              }}>
              <Text style={styles.fontRegular}>{L('VAT')}</Text>
              <Text style={styles.fontRegular}>{vat} %</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingVertical: verticalScale(1.3),
              }}>
              <Text style={[styles.fontRegular, { color: primaryColor }]}>
                {L('total')}
              </Text>
              <Text style={[styles.fontRegular, { color: primaryColor }]}>
                {' '}
                {item.pay === 9 ?
                  `${item.total} ${L('point')}`
                  :
                  `${getPrice(item.total)} ${L(global.currency.code)}`
                }
              </Text>
            </View>
            {item.pay === 9 ? <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                paddingVertical: verticalScale(1.3),
              }}>
              {/* <Text style={styles.fontRegular}></Text> */}
              <Text style={[styles.fontRegular, { color: '#c44569' }]}>
                {L('doneBuyProductByPoints')}
              </Text>
            </View> : null}
          </View>
        </Footer>
        <Modal
          animationType="fade"
          transparent={true}
          visible={this.props.error ? false : this.state.modalGiftVisible}
          onRequestClose={this.closeModalGift.bind(this)}
          style={style2.centeredView}
          hardwareAccelerated={true}
          presentationStyle="overFullScreen"
        >
          <TouchableHighlight
            style={style2.centeredView}
            onPress={this.closeModalGift.bind(this)}
          >
            <View style={style2.modalView}>
              {(this.state.winnerValue && this.state.winnerValue.length > 0 && this.state.winnerValue[0].type !== 'no_gift') ? <Text style={style2.modalText}>{this.state.winnerValue[0].description}</Text> : <Text style={style2.modalText}>{L('goodLuke')}</Text>}
              {(this.state.winnerValue && this.state.winnerValue.length > 0 && this.state.winnerValue[0].type !== 'no_gift') && <Text style={[style2.openButton, style2.textStyle]}>{L('congratulations')}</Text>}
            </View>
          </TouchableHighlight>
        </Modal>
        {this._renderError()}
        {this._renderLoading()}
      </Container>
    );
  }
}

const style2 = StyleSheet.create({
  stripNewProduct: {
    position: 'absolute',
    top: 0,
    right: 0,
    width: 80,
    height: 80
  },
  triangleCorner: {
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderLeftWidth: 80,
    borderTopWidth: 80,
    borderLeftColor: 'transparent',
    borderTopColor: '#c44569',
    borderTopRightRadius: moderateScale(3),
  },
  textNewProduct: {
    position: 'absolute',
    top: 20,
    right: -5,
    width: 60,
    transform: [{ rotate: '315deg' }],
    color: '#ffffff',
    fontFamily: DroidKufi,
    fontSize: moderateScale(4.5),
    zIndex: 100,
    textAlign: 'center',
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    // marginTop: 22,
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
  },
  modalView: {
    margin: 20,
    backgroundColor: "#7d7d7d",
    borderRadius: 20,
    padding: 25,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: '#95afc0',
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
    fontFamily: DroidKufi,
    fontSize: moderateScale(7.5)
  },
  modalText: {
    color: '#fff',
    marginBottom: 15,
    textAlign: "center",
    fontFamily: DroidKufi,
    fontSize: moderateScale(6)
  }
});


const mapStateToProps = ({ ordersR, others, productsR }) => {
  const { wheelItems, error } = productsR;
  const { loading, message, singleOrder, order_details } = ordersR;
  // console.log(singleOrder);
  const { vat, delivery } = others;
  const othersMessage = others.message;
  return {
    loading,
    message,
    singleOrder,
    order_details,
    othersMessage,
    vat,
    delivery,
    wheelItems,
    error
  };
};
export default connect(mapStateToProps, {
  getOrder,
  addComment,
  getWheelItems,
  giftToUser
})(OrderPage);
