import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Alert, View,BackHandler} from 'react-native';
import {clearMessage, showMessageChanged} from '../actions';
import {baseUrl, L} from '../Config';
import {Actions} from 'react-native-router-flux';
import {Spinner} from './assets/common';
import AsyncStorage from '@react-native-community/async-storage';
import {verticalScale, scale} from './assets/styles/Style';
import {WebView} from 'react-native-webview';

class Pay extends Component {
  state = {loading: false};
  onButtonPress = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    // then navigate
    // navigate('NewScreen');
  };

  handleBackButton = () => {
    Actions.pop();
    return true;
  };
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }
  componentDidMount() {
    // console.log(this.props.orderID)
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  
  checkUrl(text) {
    // console.log(text);

    if (text.url == baseUrl + 'failed') {
      // console.log('fail', text);
      Alert.alert(L('error'), L('paymentFaild'), [
        {text: L('sucess'), style: 'cancel'},
      ]);
      //   this.props.clearUserData();
      Actions.reset('homeStack');
    } else if (text.url == baseUrl + 'success') {
      // console.log('success', text);
      //   this.props.getUser(this.props.userId);
      Alert.alert(L('sucess'), L('paymentSucess'), [
        {text: L('sucess'), style: 'cancel'},
      ]);
      Actions.reset('homeStack');
      Actions.push('myOrders');
      // if (this.login == true) {
      //     // this.props.activatePrograme();
      //     Actions.reset('main');
      // } else {
      //   this._storeData();
      // }
    }
    // console.log(text.url);
  }
  _renderError() {
    if (this.props.message) {
      this.props.clearMessage();
      return Toast.show({
        text: this.props.message,
        buttonText: 'undo',
        duration: 3000,
      });
    }
  }
  _renderLoading() {
    if (this.props.loading || this.state.loading) {
      return <Spinner size="large" />;
    }
  }
  render() {
    return (
      <View
        style={{
          height: verticalScale(100),
          width: scale(100),
          overflow: 'hidden',
        }}>
        <WebView
          source={{
            uri: baseUrl + 'payment/' + this.props.orderID,
          }}
          onNavigationStateChange={this.checkUrl.bind(this)}
          onLoad={() => this.setState({loading: true})}
          onLoadStart={() => this.setState({loading: true})}
          onLoadEnd={() => this.setState({loading: false})}
          useWebKit={true}
          scalesPageToFit={true}
        />
        {this._renderLoading()}
        {this._renderError()}
      </View>
    );
  }
}

const mapStateToProps = ({ordersR}) => {
  const {loading, message} = ordersR;
  // console.log(result);
  return {loading, message};
};

export default connect(mapStateToProps, {clearMessage, showMessageChanged})(
  Pay,
);
