import React, { Component } from 'react';
import {
  Image,
  /*TouchableNativeFeedback,*/
  TouchableOpacity,
  TouchableWithoutFeedback,
  FlatList,
  Modal,
  BackHandler
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { Spinner } from './assets/common';
import AsyncStorage from '@react-native-community/async-storage';
import PhotoUpload from 'react-native-photo-upload';
import ImageResizer from 'react-native-image-resizer';
import { L } from '../Config';
import styles, {
  verticalScale,
  scale,
  moderateScale,
  buttonStyle,
  buttonTextStyle,
  textContainer,
  solidText,
  inputField,
  primaryColor,
  secondaryColor,
  fontBold,
  DroidKufi,
  DroidKufiBold,
  brandImage,
  thinSeperator,
} from './assets/styles/Style';
import {
  Container,
  Content,
  Header,
  Textarea,
  Separator,
  Item,
  Input,
  Button,
  Text,
  View,
  Icon,
  Toast,
  Picker, Label,
} from 'native-base';
import {
  getAddress,
  clearMessage,
  showMessageChanged,
  addAddress,
  uploadPhoto,
  saveOrder,
  checkCoupon,
  clearUplodePhotoStatus,
  getUser
} from '../actions';

class Payment extends Component {
  state = {
    modalVisible: false,
    address: '',
    user: null,
    payment: 0,
    payId: 0,
    code: '',
    countryAddress: '',
    cityAddress: '',
    districtAddress: '',
    streetAddress: '',
    buildingAddress: '',
    payNowButton: true,
    sumPointsProducts: ''
  };
  onAddressChange(value) {
    this.setState({
      address: value,
    });
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  closeModal() {
    this.setState({
      modalVisible: false,
    });
  }
  onButtonPress = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    // then navigate
    // navigate('NewScreen');
  };

  handleBackButton = () => {
    Actions.pop();
    return true;
  };
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    this.props.clearUplodePhotoStatus()
  }
  UNSAFE_componentWillReceiveProps(nextProps, nextState) {
    if (nextProps.upload_photo) {
      this.setState({ payNowButton: false })
    }
    if (nextProps.addresses && nextProps.addresses.length > 0) {
      this.setState({ address: nextProps.addresses[0].id })
    }
  }
  async componentDidMount() {
    let points = 0
    this.props.cart.map((item) => {
      if (!item.is_gift && !item.is_offer) { points += item.points * item.quantity }
    })
    try {
      let value = await AsyncStorage.getItem('user');
      const user = JSON.parse(value);
      if (user) {
        this.setState({ user: user, sumPointsProducts: points });
        this._getData();
      }
      // console.log(user);
    } catch (error) {
      // console.log(error);
      // this._getData()
    }
  }
  _getData() {
    this.props.getAddress(this.state.user.id);
    this.props.getUser(this.state.user.id);
    // console.log(this.state.payId);
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  onCountryAddressChanged(text) {
    this.setState({ countryAddress: text });
  }
  onCityAddressChanged(text) {
    this.setState({ cityAddress: text });
  }
  onDistrictAddressChanged(text) {
    this.setState({ districtAddress: text });
  }
  onStreetAddressChanged(text) {
    this.setState({ streetAddress: text });
  }
  onBuildingAddressChanged(text) {
    this.setState({ buildingAddress: text });
  }
  initAddressfields() {
    this.setState({ countryAddress: '', cityAddress: '', districtAddress: '', streetAddress: '', buildingAddress: '' });
  }
  onAddAddress() {
    if (this.state.countryAddress == '') {
      this.props.showMessageChanged(L('emptyField') + ' ' + L('country'));
    } else if (this.state.cityAddress == '') {
      this.props.showMessageChanged(L('emptyField') + ' ' + L('city'));
    } else if (this.state.districtAddress == '') {
      this.props.showMessageChanged(L('emptyField') + ' ' + L('district'));
    } else if (this.state.streetAddress == '') {
      this.props.showMessageChanged(L('emptyField') + ' ' + L('street'));
    } else if (this.state.buildingAddress == '') {
      this.props.showMessageChanged(L('emptyField') + ' ' + L('building'));
    } else {
      var address = this.state.countryAddress + ' - ' +
        this.state.cityAddress + ' - ' +
        this.state.districtAddress + ' - ' +
        this.state.streetAddress + ' - ' +
        this.state.buildingAddress;
      const addAddress = {
        client_id: this.state.user.id,
        //address: this.state.address,
        address: address,
        countryAddress: this.state.countryAddress,
        cityAddress: this.state.cityAddress,
        districtAddress: this.state.districtAddress,
        streetAddress: this.state.streetAddress,
        buildingAddress: this.state.buildingAddress,
      };
      this.props.addAddress({ addAddress });
      this.setModalVisible(false);
    }
  }
  saveOrder() {
    const { cart, billPhoto } = this.props;
    const { user, payId, address, code } = this.state;
    const data = {
      cart,
      user_id: user.id,
      pay_with_points: payId === 9 ? true : false,
      payId: payId,
      addressId: address,
      receipt_photo: billPhoto,
      coupon: code,
      currency_id: global.currency.id
    };
    this.props.saveOrder(data);
    // console.log(data);
  }
  _renderError() {
    if (this.props.message) {
      this.props.clearMessage();
      return Toast.show({
        text: this.props.message,
        buttonText: L('dismiss'),
        duration: 3000,
        style: { backgroundColor: primaryColor },
        textStyle: { fontFamily: DroidKufi, fontSize: moderateScale(4.5) },
        buttonTextStyle: { fontFamily: DroidKufi, fontSize: moderateScale(4.5) },
      });
    }
  }
  _renderLoading() {
    if (this.props.loading || this.props.othersLoading) {
      return <Spinner size="large" />;
    }
  }
  render() {
    const paymentMethods = [
      {
        id: 1,
        title: L('pay_1'),
        image: require('./assets/images/cod.png'),
        method: 1,
      },
      {
        id: 2,
        title: L('pay_2'),
        image: require('./assets/images/visa.png'),
        method: 2,
      },
      {
        id: 9,
        title: L('pay_9'),
        image: require('./assets/images/points.png'),
        method: 9,
      },
      {
        id: 3,
        title: L('pay_3'),
        image: require('./assets/images/transfer.png'),
        method: 3,
      }
    ];
    return (
      <Container>
        <Header
          androidStatusBarColor={primaryColor}
          style={styles.routernavigationBarStyle}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignSelf: 'center',
              alignItems: 'center',
              width: scale(100),
            }}>
            <Button
              onPress={() => Actions.pop()}
              transparent
              /*background={TouchableNativeFeedback.Ripple(secondaryColor)}*/
              style={styles.headerBack}>
              <Icon
                name="keyboard-backspace"
                type="MaterialIcons"
                style={{
                  color: '#fff',
                  fontSize: 25,
                  transform: [{ scaleX: L('scaleLang') }],
                }}
              />
            </Button>
            <Text numberOfLines={1} style={styles.headerText}>
              {this.props.title}
            </Text>
          </View>
        </Header>
        <Content padder style={{ backgroundColor: '#f9f9f9' }}>
          <Item
            rounded
            style={[
              styles.inputAuthContainer,
              {
                width: scale(90),
                borderRadius: moderateScale(8),
                height: verticalScale(8),
                borderColor: '#f3f3f3',
              },
            ]}>
            <Picker
              mode="dropdown"
              placeholder={L('chooseAddress')}
              placeholderStyle={{ color: '#a2a2a2' }}
              placeholderIconColor="#fff"
              selectedValue={this.state.address}
              onValueChange={this.onAddressChange.bind(this)}
              itemStyle={{ textAlign: 'center', fontFamily: DroidKufi }}>
              <Picker.Item
                label={L('chooseAddress')}
                value
                key="val0"
                disabled
              />
              {this.props.addresses &&
                this.props.addresses.map(item => {
                  return (
                    <Picker.Item
                      label={item.address}
                      value={item.id}
                      key={item.id}
                    />
                  );
                })}
            </Picker>
            <Icon
              type="Feather"
              name="chevron-down"
              style={{ fontSize: moderateScale(7) }}
            />
          </Item>
          <Text
            style={[
              styles.fontRegular,
              { textAlign: 'center', marginBottom: verticalScale(1) },
            ]}>
            {L('or')}
          </Text>
          <Button
            style={styles.buttonStyle2}
            onPress={() => {
              this.initAddressfields();
              this.setModalVisible(true);

            }}>
            <Text style={styles.buttonTextStyle}>{L('addNewAdd')}</Text>
          </Button>
          <Separator style={styles.thinSeperator} />
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Item
              style={[
                styles.inputAuthContainer,
                {
                  marginBottom: 0,
                  borderRadius: moderateScale(8),
                  width: scale(60),
                  paddingHorizontal: scale(2),
                },
              ]}>
              <Input

                onChangeText={text => this.setState({ code: text })}
                placeholder={L('coupon')}
                placeholderStyle={{ color: '#a2a2a2', fontFamily: DroidKufi }}
                value={this.state.code}
                style={[styles.inputField, { textAlign: L('textAlignRight') }]}
              />
            </Item>
            <Button
              onPress={() => this.props.checkCoupon(this.state.code)}
              style={[
                styles.buttonStyle2,
                {
                  width: scale(30),
                  height: verticalScale(7),
                  marginBottom: 0,
                  marginTop: 0,
                },
              ]}>
              <Text style={styles.buttonTextStyle}>{L('confirm')}</Text>
            </Button>
          </View>
          <Separator style={styles.thinSeperator} />
          {paymentMethods.map(paymentId => {
            if (paymentId.method == 2) {
              return null;
              /*return (
                <Button
                  transparent
                  rounded
                  style={[
                    styles.buttonStyle2,
                    {
                      backgroundColor: '#fff',
                      width: scale(90),
                      borderWidth: 1,
                      borderColor:
                        this.state.payment == paymentId.id
                          ? primaryColor
                          : '#a2a2a2',
                      height: verticalScale(8),
                      justifyContent: 'flex-start',
                      marginBottom: verticalScale(1),
                    },
                  ]}
                  onPress={() =>
                    this.setState({
                      payment: paymentId.id,
                      payId: paymentId.method,
                    })
                  }>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      width: scale(75),
                      paddingHorizontal: scale(3),
                    }}>
                    <Image
                      source={paymentId.image}
                      resizeMode="contain"
                      style={{width: scale(10), height: verticalScale(10)}}
                    />
                    <Text
                      style={[
                        styles.buttonTextStyle,
                        {
                          color:
                            this.state.payment == paymentId.id
                              ? primaryColor
                              : '#707070',
                          fontSize: moderateScale(4),
                          marginHorizontal: scale(2),
                        },
                      ]}>
                      {paymentId.title}
                    </Text>
                  </View>
                  {this.state.payment == paymentId.id ? (
                    <Icon
                      name="checkmark-circle"
                      type="Ionicons"
                      style={{color: primaryColor}}
                    />
                  ) : null}
                </Button>
              );*/
            } else if (((this.props.user && !(this.props.user.points > this.state.sumPointsProducts ? true : false)) && paymentId.method == 9) || (this.state.sumPointsProducts === 0 && paymentId.method == 9)) {
              return null
            } else {
              return (
                <Button
                  transparent
                  rounded
                  style={[
                    styles.buttonStyle2,
                    {
                      backgroundColor: '#fff',
                      width: scale(90),
                      borderWidth: 1,
                      borderColor:
                        this.state.payment == paymentId.id
                          ? primaryColor
                          : '#a2a2a2',
                      height: verticalScale(8),
                      justifyContent: 'flex-start',
                      marginBottom: verticalScale(1),
                    },
                  ]}
                  onPress={() =>
                    this.setState({
                      payment: paymentId.id,
                      payId: paymentId.method,
                    })
                  }>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      width: scale(75),
                      paddingHorizontal: scale(3),
                    }}>
                    <Image
                      source={paymentId.image}
                      resizeMode="contain"
                      style={{ width: scale(10), height: verticalScale(10) }}
                    />
                    <Text
                      style={[
                        styles.buttonTextStyle,
                        {
                          color:
                            this.state.payment == paymentId.id
                              ? primaryColor
                              : '#707070',
                          fontSize: moderateScale(4),
                          marginHorizontal: scale(2),
                        },
                      ]}>
                      {paymentId.title}
                    </Text>
                  </View>
                  {this.state.payment == paymentId.id ? (
                    <Icon
                      name="checkmark-circle"
                      type="Ionicons"
                      style={{ color: primaryColor }}
                    />
                  ) : null}
                </Button>
              );
            }
          })}
          {(this.state.payment == 9 && this.props.user) ?
            <View style={{ width: scale(90), alignItems: 'flex-start', backgroundColor: '#fff', padding: scale(5), }}>
              <Text style={[styles.fontRegular]}>{L('priceOrdersPoints')} : {this.state.sumPointsProducts} {L('point')} </Text>
              <Text style={[styles.fontRegular]}>{L('faresDeliveryPoint')} : 25 {L('point')} </Text>
              <Text style={[styles.fontRegular]}>{L('youHavePoints')} : {this.props.user.points} {L('point')}</Text>
              <Text style={[styles.fontRegular]}>{L('restpoints')} : {this.props.user.points - this.state.sumPointsProducts - 25} {L('point')}</Text>
            </View>
            : null}
          {this.state.payment == 3 ? (
            <View>
              <Text
                style={[
                  styles.fontRegular,
                  {
                    width: scale(90),
                    alignSelf: 'center',
                    backgroundColor: '#fff',
                    padding: scale(5),
                  },
                ]}>
                {this.props.bank_transfer}
              </Text>
              <PhotoUpload
                containerStyle={[
                  styles.photoUpload,
                  { marginTop: verticalScale(2) },
                ]}
                onResponse={image => {
                  if (image.fileName || image.fileSize) {
                    ImageResizer.createResizedImage(
                      image.path,
                      800,
                      600,
                      'JPEG',
                      60,
                    )
                      .then(image => {
                        this.props.uploadPhoto(image, 2);
                        // resizeImageUri is the URI of the new image that can now be displayed, uploaded...
                      })
                      .catch(err => {
                        // Oops, something went wrong. Check that the filename is correct and
                        // inspect err to get more details.
                      });
                    // console.log(image.fileName);
                  }
                }}>
                {this.props.billPhoto == '' ? (
                  <Image
                    style={[styles.photo, { marginBottom: verticalScale(1) }]}
                    resizeMode="contain"
                    source={require('./assets/images/upload.png')}
                  />
                ) : (
                    <Image
                      style={[styles.photo, { marginBottom: verticalScale(1) }]}
                      resizeMode="cover"
                      source={{ uri: this.props.billPhoto }}
                    />
                  )}
                <Text style={[styles.fontRegular, { color: primaryColor }]}>
                  {L('addBill')}
                </Text>
              </PhotoUpload>
            </View>
          ) : null}
          <View style={{ marginVertical: verticalScale(1) }}>
            <Button
              style={[styles.buttonStyle, { backgroundColor: (this.state.payNowButton && this.state.payment === 3) ? '#b2bec3' : primaryColor }]}
              disabled={this.state.payNowButton && this.state.payment === 3}
              onPress={() => this.saveOrder() /*this.handleCheckout()*/}>
              <Text style={styles.buttonTextStyle}>{L('payNow')}</Text>
            </Button>
          </View>
        </Content>
        <View>
          <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.modalVisible}
            onRequestClose={this.closeModal.bind(this)}
            hardwareAccelerated={true}
            presentationStyle="overFullScreen">
            <TouchableWithoutFeedback
              onPress={() => {
                this.setModalVisible(!this.state.modalVisible);
              }}>
              <View style={styles.modalContainerlStyle}>
                <View
                  style={[
                    styles.modalStyle,
                    {
                      width: scale(90),
                      padding: scale(5),
                      justifyContent: 'center',
                    },
                  ]}>
                  <Text
                    style={[
                      styles.fontRegular,
                      {
                        marginBottom: verticalScale(2),
                        textAlign: 'center',
                        color: primaryColor,
                      },
                    ]}>
                    {L('addAddressDetails')}
                  </Text>
                  {/*<Item
                    style={[
                      styles.inputAuthContainer,
                      {
                        borderRadius: moderateScale(5),
                        marginBottom: verticalScale(0),
                        width: scale(80),
                      },
                    ]}>
                    <Textarea
                      rowSpan={5}
                      onChangeText={text => this.setState({address: text})}
                      placeholder={L('addAddress')}
                      value={this.state.address}
                      style={styles.textAreaField}
                      placeholderTextColor="#a2a2a2"
                    />
                  </Item>*/}
                  <Item
                    style={[
                      styles.inputAuthContainer,
                      {
                        borderTopWidth: 0,
                        borderLeftWidth: 0,
                        borderRightWidth: 0,
                      },
                    ]}
                    floatingLabel>
                    <Label style={styles.inputLabel}>{L('country')}</Label>
                    <Input
                      onChangeText={this.onCountryAddressChanged.bind(this)}
                      value={this.state.countryAddress}
                      style={[styles.inputField, { textAlign: L('textAlignRight') }]}
                    />
                  </Item>
                  <Item
                    style={[
                      styles.inputAuthContainer,
                      {
                        borderTopWidth: 0,
                        borderLeftWidth: 0,
                        borderRightWidth: 0,
                      },
                    ]}
                    floatingLabel>
                    <Label style={styles.inputLabel}>{L('city')}</Label>
                    <Input
                      onChangeText={this.onCityAddressChanged.bind(this)}
                      value={this.state.cityAddress}
                      style={[styles.inputField, { textAlign: L('textAlignRight') }]}
                    />
                  </Item>
                  <Item
                    style={[
                      styles.inputAuthContainer,
                      {
                        borderTopWidth: 0,
                        borderLeftWidth: 0,
                        borderRightWidth: 0,
                      },
                    ]}
                    floatingLabel>
                    <Label style={styles.inputLabel}>{L('district')}</Label>
                    <Input
                      onChangeText={this.onDistrictAddressChanged.bind(this)}
                      value={this.state.districtAddress}
                      style={[styles.inputField, { textAlign: L('textAlignRight') }]}
                    />
                  </Item>
                  <Item
                    style={[
                      styles.inputAuthContainer,
                      {
                        borderTopWidth: 0,
                        borderLeftWidth: 0,
                        borderRightWidth: 0,
                      },
                    ]}
                    floatingLabel>
                    <Label style={styles.inputLabel}>{L('street')}</Label>
                    <Input
                      onChangeText={this.onStreetAddressChanged.bind(this)}
                      value={this.state.streetAddress}
                      style={[styles.inputField, { textAlign: L('textAlignRight') }]}
                    />
                  </Item>
                  <Item
                    style={[
                      styles.inputAuthContainer,
                      {
                        borderTopWidth: 0,
                        borderLeftWidth: 0,
                        borderRightWidth: 0,
                      },
                    ]}
                    floatingLabel>
                    <Label style={styles.inputLabel}>{L('building')}</Label>
                    <Input
                      onChangeText={this.onBuildingAddressChanged.bind(this)}
                      value={this.state.buildingAddress}
                      style={[styles.inputField, { textAlign: L('textAlignRight') }]}
                    />
                  </Item>
                  <Item regular style={styles.noBorder}>
                    <Button
                      block
                      style={[styles.buttonStyle2, { width: scale(80) }]}
                      onPress={() => this.onAddAddress()}>
                      <Text style={styles.buttonTextStyle}>
                        {L('addAddress')}
                      </Text>
                    </Button>
                  </Item>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </Modal>
        </View>
        {this._renderError()}
        {this._renderLoading()}
      </Container>
    );
  }
}
const mapStateToProps = ({ ordersR, others, auth }) => {
  const { loading, message, cart } = ordersR;
  const { addresses, bank_transfer } = others;
  const { billPhoto, billPhoto_thumb, upload_photo, user } = auth;
  const othersLoading = others.loading;
  // console.log(billPhoto_thumb);
  return {
    loading,
    message,
    addresses,
    billPhoto,
    billPhoto_thumb,
    upload_photo,
    cart,
    bank_transfer,
    user,
    othersLoading
  };
};
export default connect(mapStateToProps, {
  getAddress,
  clearMessage,
  clearUplodePhotoStatus,
  showMessageChanged,
  addAddress,
  uploadPhoto,
  saveOrder,
  checkCoupon,
  getUser
})(Payment);
