import React, { Component } from 'react';
import {
  Image,
  /* TouchableNativeFeedback,*/
  TouchableOpacity,
  TouchableWithoutFeedback,
  Alert,
  Platform,
  StyleSheet,
  Dimensions,
  Modal, ScrollView,
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { Spinner } from './assets/common';
import ColorPalette from 'react-native-color-palette'

import { L } from '../Config';
import styles, {
  verticalScale,
  scale,
  moderateScale,
  ButtonStyle,
  ButtonTextStyle,
  textContainer,
  solidText,
  inputField,
  primaryColor,
  secondaryColor,
  fontBold,
  DroidKufi,
  DroidKufiBold,
  brandImage,
} from './assets/styles/Style';
import {
  Container,
  Content,
  Header,
  Item,
  Input,
  Button,
  Text,
  View,
  Icon,
  Toast,
  Picker,
  Card,
  CardItem,
  Body,
  Footer,
  FooterTab,
} from 'native-base';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import FastImage from 'react-native-fast-image';
import { AirbnbRating } from 'react-native-ratings';
import AsyncStorage from '@react-native-community/async-storage';
import PhotoZoom from 'react-native-photo-zoom';
import {
  getProduct,
  clearMessage,
  showMessageChanged,
  addToCart,
  addToWhishlist,
  removeToWishlist,
  getWhishlist,
  deleteWhishlist, getPrice,
} from '../actions';
import HTML from 'react-native-render-html';
import WebView from "react-native-webview/lib/WebView";

const { width: screenWidth } = Dimensions.get('window');

class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clicks: 1,
      show: true,
      Rsize_id: 0,
      Lsize_id: 0,
      Rsize_additional_price: 0,
      Lsize_additional_price: 0,
      user: null,
      wishlist: null,
      selected_color: 0,
      active: false,
      modalVisible: false,
      additionalPrice: 0,
      giftProduct: props.giftProduct ? true : false,
      chosenOfferItem: props.chosenOfferItem ? true : false,
      pressedButton: false,
      giftId: props.giftId ? props.giftId : '',
      indexOfColor: (props.product && props.product.colors_with_photos.length) > 0 ? props.product.colors_with_photos[0].color : ''
    };
  }
  UNSAFE_componentWillReceiveProps(nextProps, nextState) {
    if (nextProps.product && nextProps.product.colors_with_photos.length > 0) {
      this.setState({ indexOfColor: nextProps.product.colors_with_photos[0].color })
    }
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  closeModal() {
    this.setState({
      modalVisible: false,
    });
  }
  IncrementItem = () => {
    this.setState({ clicks: this.state.clicks + 1 });
  };
  DecreaseItem = () => {
    if (this.state.clicks == 1) {
      this.setState({ clicks: this.state.clicks });
    } else {
      this.setState({ clicks: this.state.clicks - 1 });
    }
  };
  onRightValueChange(value) {
    var selectedItem = this.props.product.quantityPerSize.filter(item => {
      return item.size_id === value;
    })[0];

    this.setState({
      Rsize_id: value,
      Rsize_additional_price: selectedItem ? parseFloat(selectedItem.price) : 0
    });
  }
  onLeftValueChange(value) {
    var selectedItem = this.props.product.quantityPerSize.filter(item => {
      return item.size_id === value;
    })[0];
    this.setState({
      Lsize_id: value,
      Lsize_additional_price: selectedItem ? parseFloat(selectedItem.price) : 0
    });
  }
  onColorChange(value) {
    this.setState({
      selected_color: value,
    });
  }
  async componentDidMount() {
    try {
      let value = await AsyncStorage.getItem('user');
      // console.log(value);
      const user = JSON.parse(value);
      if (user) {
        this.setState({ user: user });
        this._getData();
      }
      // console.log(user);
      this._getData2();
    } catch (error) {
      // console.log(error);
      // this._getData()
    }
  }
  _getData() {
    this.props.getWhishlist(this.state.user.id);
    this.props.getProduct(this.props.productID);
  }
  _getData2() {
    this.props.getProduct(this.props.productID);
  }
  storeProduct() {
    let added = false;
    const { Lsize_id, Rsize_id, Lsize_additional_price, Rsize_additional_price, clicks, indexOfColor, giftProduct, giftId, chosenOfferItem } = this.state;
    if (Rsize_id == '' && Lsize_id == '') {
      // this.props.showMessageChanged(
      //   L('emptyField') + ' ' + L('size') + ' ' + L('right'),
      // );
      this.setState({ Rsize_id: 70, Lsize_id: 70 });
      this.setState({ Lsize_additional_price: 0, Rsize_additional_price: 0 });
    }
    // else if (Lsize_id == '') {
    // this.props.showMessageChanged(
    //   L('emptyField') + ' ' + L('size') + ' ' + L('left'),
    // );
    // this.setState({Lsize_id: 70});
    // }
    // else if (selected_color == '') {
    //   this.props.showMessageChanged(L('emptyField') + ' ' + L('eyeColor'));
    // }
    var cart = this.props.cart;
    // const {Lsize_id, Rsize_id, clicks, selected_color} = this.state;
    if (Lsize_id && Rsize_id) {
      if (Lsize_id && Rsize_id) {
        if (Lsize_id == Rsize_id) {
          var nItem = {
            ...this.props.product,
            id: new Date().getTime(),
            product_id: this.props.product.id,
            quantity: clicks,
            size_right: Rsize_id,
            size_left: Lsize_id,
            Rsize_additional_price: Rsize_additional_price,
            Lsize_additional_price: 0,
            selected_color: indexOfColor,
            is_gift: giftProduct,
            gift_id: giftId,
            is_offer: chosenOfferItem
          };

          cart.push(nItem);
          this.props.addToCart(cart);
          added = true;
        } else {
          var nItem = {
            ...this.props.product,
            id: new Date().getTime(),
            product_id: this.props.product.id,
            quantity: clicks,
            size_right: 0,
            Rsize_additional_price: 0,
            Lsize_additional_price: Lsize_additional_price,
            size_left: Lsize_id,
            selected_color: indexOfColor,
            is_gift: giftProduct,
            gift_id: giftId,
            is_offer: chosenOfferItem
          };
          cart.push(nItem);
          this.props.addToCart(cart);
          added = true;
          var nItem = {
            ...this.props.product,
            product_id: this.props.product.id,
            quantity: clicks,
            id: new Date().getTime(),
            size_right: Rsize_id,
            size_left: 0,
            Rsize_additional_price: Rsize_additional_price,
            Lsize_additional_price: 0,
            selected_color: indexOfColor,
            is_gift: giftProduct,
            gift_id: giftId,
            is_offer: chosenOfferItem
          };
          cart.push(nItem);
          this.props.addToCart(cart);
          added = true;
        }
      } else {
        var nItem = {
          ...this.props.product,
          product_id: this.props.product.id,
          quantity: clicks,
          id: new Date().getTime(),
          size_right: Rsize_id,
          size_left: Lsize_id,
          Rsize_additional_price: Rsize_additional_price,
          Lsize_additional_priceL: Lsize_additional_price,
          selected_color: indexOfColor,
          is_gift: giftProduct,
          gift_id: giftId,
          is_offer: chosenOfferItem
        };
        cart.push(nItem);
        this.props.addToCart(cart);
        added = true;
      }
    } else if (Lsize_id == '' && Rsize_id == '') {
      var nItem = {
        ...this.props.product,
        id: new Date().getTime(),
        product_id: this.props.product.id,
        quantity: clicks,
        size_right: 70,
        size_left: 70,
        Rsize_additional_price: 0,
        Lsize_additional_priceL: 0,
        selected_color: indexOfColor,
        is_gift: giftProduct,
        gift_id: giftId,
        is_offer: chosenOfferItem
      };
      cart.push(nItem);
      this.props.addToCart(cart);
      added = true;
    }
    if (added) {
      return Toast.show({
        text: 'تمت الاضافة بنجاح',
        ButtonText: L('dismiss'),
        duration: 3000,
        style: { backgroundColor: primaryColor },
        textStyle: { fontFamily: DroidKufi, fontSize: moderateScale(4.5) },
        ButtonTextStyle: { fontFamily: DroidKufi, fontSize: moderateScale(4.5) },
      });
    }
    // }
  }
  _renderError() {
    if (this.props.message) {
      this.props.clearMessage();
      return Toast.show({
        text: this.props.message,
        ButtonText: L('dismiss'),
        duration: 3000,
        style: { backgroundColor: primaryColor },
        textStyle: { fontFamily: DroidKufi, fontSize: moderateScale(4.5) },
        ButtonTextStyle: { fontFamily: DroidKufi, fontSize: moderateScale(4.5) },
      });
    }
  }
  _renderLoading() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }
  }
  _renderItem = ({ item, index }) => {
    // console.log('item', item)
    return (
      <TouchableOpacity
        key={index}
        onPress={() => {
          this.setModalVisible(true);
        }}>
        <View style={styles2.item}>
          <FastImage
            source={{ uri: item.thumb ? item.thumb : item.photo, priority: FastImage.priority.normal }}
            style={styles2.image}
            resizeMode={FastImage.resizeMode.contain}
          />
        </View>
      </TouchableOpacity>
    );
  };
  pagination() {
    const { activeSlide } = this.state;
    return (
      <Pagination
        dotsLength={
          this.props.product &&
          this.props.product.photos &&
          this.props.product.photos.length
        }
        activeDotIndex={activeSlide}
        containerStyle={{
          // backgroundColor: 'rgba(0, 0, 0, 0.75)',
          paddingTop: 0,
          paddingBottom: 0,
          position: 'absolute',
          bottom: verticalScale(2),
        }}
        dotStyle={{
          width: 10,
          height: 10,
          borderRadius: 5,
          // marginHorizontal: 8,
          backgroundColor: primaryColor,
        }}
        inactiveDotStyle={{
          // Define styles for inactive dots here
          backgroundColor: '#fff',
        }}
        inactiveDotOpacity={0.9}
        inactiveDotScale={0.6}
      />
    );
  }
  inArray(needle) {
    // console.log(needle);
    var haystack = this.props.wishlistArr;
    var length = haystack.length;
    for (var i = 0; i < length; i++) {
      if (haystack[i].id == needle) return true;
    }
    return false;
  }
  addToWishlistAction() {
    const postFavourite = {
      client_id: this.state.user.id,
      product_id: this.props.productID,
    };
    this.props.addToWhishlist({ postFavourite });
    this.setState(state => {
      return (
        this.props.getProduct(this.props.product.id),
        this.props.getWhishlist(this.state.user.id)
      );
    });
  }
  removeToWishlist() {
    const deleteFavourite = {
      client_id: this.state.user.id,
      product_id: this.props.productID,
    };
    this.props.deleteWhishlist({ deleteFavourite });
    // console.log({ deleteFavourite });
    // this.setState({ item })
    this.setState(state => {
      return (
        this.props.getProduct(this.props.product.id),
        this.props.getWhishlist(this.state.user.id)
      );
    });
  }

  render() {
    var item = this.props.product;
    // console.log('prodict', item);

    var favouriteClick;
    var heart;
    if (this.props.wishlistArr) {
      var check = this.inArray(this.props.productID);
      if (check == true) {
        heart = 'heart';
        favouriteClick = () => this.removeToWishlist();
      } else {
        heart = 'heart-empty';
        favouriteClick = () => this.addToWishlistAction();
      }
    }
    return (
      <Container>
        <Header
          androidStatusBarColor={primaryColor}
          style={styles.routernavigationBarStyle}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignSelf: 'center',
              alignItems: 'center',
              width: scale(100),
            }}>
            <Button
              onPress={() => Actions.pop()}
              transparent
              /*background={TouchableNativeFeedback.Ripple(secondaryColor)}*/
              style={styles.headerBack}>
              <Icon
                name="keyboard-backspace"
                type="MaterialIcons"
                style={{
                  color: '#fff',
                  fontSize: 25,
                  transform: [{ scaleX: L('scaleLang') }],
                }}
              />
            </Button>
            <Text numberOfLines={1} style={styles.headerText}>
              {this.props.title}
            </Text>
            <Button
              onPress={() => Actions.jump('cart', { loading: true })}
              transparent
              /*background={TouchableNativeFeedback.Ripple(secondaryColor)}*/
              style={[styles.headerBack]}>
              <View
                style={{
                  position: 'absolute',
                  height: 15,
                  width: 15,
                  borderRadius: 15,
                  backgroundColor: '#fff',
                  left: 8,
                  top: 5,
                  alignItems: 'center',
                  justifyContent: 'center',
                  zIndex: 2000,
                }}>
                <Text
                  style={{
                    color: primaryColor,
                    fontFamily: DroidKufi,
                    fontSize: moderateScale(5),
                  }}>
                  {this.props.qty}
                </Text>
              </View>
              <Icon
                name="shopping-cart"
                type="Feather"
                style={{
                  fontSize: moderateScale(8),
                  color: '#fff',
                }}
              />
              {/* <Icon name='keyboard-backspace' type='MaterialIcons'
            style={styles.backButtonIcon}
        /> */}
            </Button>
          </View>
        </Header>
        {this.props.product ? (
          <Content>
            {/*<Text
              style={[
                styles.fontRegular,
                {
                  fontSize: moderateScale(8),
                  paddingHorizontal: scale(3),
                  textAlign: L('textAlignLeft'),
                },
              ]}>
              {item.title}
            </Text>*/}
            <View
              style={{
                alignItems: 'center',
                overflow: 'hidden',
                marginTop: verticalScale(0),
              }}>
              <Carousel
                ref={c => {
                  this._carousel = c;
                }}
                layout={'default'}
                sliderWidth={screenWidth}
                sliderHeight={screenWidth}
                itemWidth={screenWidth}
                data={item.colors_with_photos.length > 0 ? item.colors_with_photos.filter(item => item.color === this.state.indexOfColor) : item.photos}
                renderItem={this._renderItem}
                hasParallaxImages={false}
                autoplay={true}
                loop={true}
                inactiveSlideScale={1}
                extraData={this.state}
                slideStyle={styles2.imageContainer}
                onSnapToItem={index => this.setState({ activeSlide: index })}
              />
              {this.pagination()}
              {this.state.user != null ? (
                <TouchableOpacity
                  onPress={favouriteClick}
                  style={{
                    position: 'absolute',
                    right: 10,
                    top: 10,
                    backgroundColor: '#fff',
                    width: scale(9),
                    height: verticalScale(4.5),
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderRadius: moderateScale(2),
                    borderWidth: 1,
                    borderColor: '#f9f9f9',
                  }}>
                  <Icon
                    name={heart}
                    type="Ionicons"
                    style={{ color: 'red', fontSize: moderateScale(8) }}
                  />
                </TouchableOpacity>
              ) : null}
            </View>

            {item.colors_with_photos.length > 0 ?
              <ColorPalette
                onChange={color => this.setState({ indexOfColor: color })}
                defaultColor={this.state.indexOfColor}
                colors={item.colors_with_photos.map(color => color.color)}
                title={""}
                icon={<Text>✔</Text>}
              /> : null
            }

            <Content padder>
              <View
                style={{
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                  alignItems: 'center',
                  borderBottomColor: '#ddd',
                  borderBottomWidth: 1,
                  paddingVertical: 15,
                  marginBottom: 15,
                  paddingHorizontal: scale(3),
                }}>
                <View
                  style={{
                    flexDirection: 'column',
                    alignSelf: 'center',
                    alignSelf: 'center',
                  }}>
                  <Text
                    style={[
                      styles.fontRegular,
                      { fontSize: moderateScale(7), color: primaryColor },
                    ]}>
                    {getPrice(item.newPrice)} {L(global.currency.code)}
                  </Text>
                  {item.price != item.newPrice ? (
                    <Text
                      style={[
                        styles.fontRegular,
                        {
                          textDecorationLine: 'line-through',
                          textDecorationStyle: 'solid',
                        },
                      ]}>
                      {getPrice(item.price)} {L(global.currency.code)}
                    </Text>
                  ) : null}
                </View>
                <View style={{ alignItems: 'center' }}>
                  <AirbnbRating
                    showRating={false}
                    isDisabled
                    defaultRating={item.rating}
                    ratingColor="#f4b700"
                    size={15}
                    starContainerStyle={{ marginLeft: 0 }}
                  />
                  <Text style={[styles.fontRegular, { color: '#8492a6' }]}>
                    {L('rateIs')} {item.rating}
                  </Text>
                  <TouchableOpacity
                    onPress={() =>
                      Actions.push('ratePage', {
                        title: L('rates'),
                        productID: this.props.product.id,
                      })
                    }>
                    <Text style={[styles.fontRegular, { color: primaryColor }]}>
                      {L('showRates')}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'column',
                  justifyContent: 'space-between',
                }}>
                <View
                  style={{ marginBottom: verticalScale(1), flexDirection: 'row', justifyContent: 'space-between' }}>
                  <Text
                    style={[
                      styles.fontRegular,
                      {
                        backgroundColor: primaryColor,
                        color: '#fff',
                        width: scale(25),
                        height: verticalScale(5),
                        borderRadius: moderateScale(15),
                        textAlignVertical: 'center',
                        textAlign: 'center',
                      },
                    ]}>
                    {L('qqty')}
                  </Text>
                  <View
                    style={{
                      borderWidth: 1,
                      borderColor: '#f3f3f3',
                      flexDirection: 'row',
                      width: scale(65),
                      height: verticalScale(5),
                      justifyContent: 'space-between',
                      paddingHorizontal: scale(2),
                      borderRadius: moderateScale(8),
                    }}>
                    {this.state.show ? (
                      <View
                        style={{ width: scale(30), paddingTop: verticalScale(1) }}>
                        <Text>{this.state.clicks}</Text>
                      </View>
                    ) : (
                        ''
                      )}
                    {(this.state.giftProduct || this.props.chosenOfferItem) ? null :
                      <View style={{ flexDirection: 'column' }}>
                        <TouchableOpacity onPress={this.IncrementItem}>
                          <Icon
                            name="chevron-up"
                            type="Feather"
                            style={{
                              fontSize: moderateScale(7),
                            }}
                          />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.DecreaseItem}>
                          <Icon
                            name="chevron-down"
                            type="Feather"
                            style={{ fontSize: moderateScale(7) }}
                          />
                        </TouchableOpacity>
                      </View>}
                  </View>
                </View>


                {item.quantityPerSize && item.quantityPerSize.length && item.is_in_lens ?
                  <View
                    style={{
                      marginTop: verticalScale(2),
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      borderBottomColor: '#ddd',
                      borderBottomWidth: 1,
                      marginBottom: verticalScale(1),
                    }}>
                    <Text
                      style={[
                        styles.fontRegular,
                        {
                          backgroundColor: primaryColor,
                          color: '#fff',
                          width: scale(25),
                          height: verticalScale(5),
                          borderRadius: moderateScale(15),
                          textAlignVertical: 'center',
                          textAlign: 'center',
                        },
                      ]}>
                      {L('eyeStrength')}
                    </Text>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        width: scale(65),
                      }}>
                      <Item
                        rounded
                        style={[
                          styles.inputAuthContainer,
                          {
                            width: scale(30),
                            borderRadius: moderateScale(8),
                            height: verticalScale(5),
                            borderColor: '#f3f3f3',
                          },
                        ]}>
                        <Picker
                          mode="dropdown"
                          placeholder={L('right')}
                          placeholderStyle={{ color: '#a2a2a2' }}
                          placeholderIconColor="#fff"
                          selectedValue={this.state.Rsize_id}
                          onValueChange={this.onRightValueChange.bind(this)}
                          itemStyle={{ textAlign: 'center', fontFamily: DroidKufi }}>
                          <Picker.Item
                            label={L('right')}
                            value=""
                            key="val0"
                            disabled
                          />
                          {item.quantityPerSize &&
                            item.quantityPerSize.map(item => {
                              return (
                                <Picker.Item
                                  label={item.size}
                                  value={item.size_id}
                                  key={item.size_id}
                                />
                              );
                            })}
                        </Picker>
                        <Icon
                          type="Feather"
                          name="chevron-down"
                          style={{ fontSize: moderateScale(7) }}
                        />
                      </Item>
                      <Item
                        rounded
                        style={[
                          styles.inputAuthContainer,
                          {
                            width: scale(30),
                            borderRadius: moderateScale(8),
                            height: verticalScale(5),
                            borderColor: '#f3f3f3',
                          },
                        ]}>
                        <Picker
                          mode="dropdown"
                          placeholder={L('left')}
                          placeholderStyle={{ color: '#a2a2a2' }}
                          placeholderIconColor="#fff"
                          selectedValue={this.state.Lsize_id}
                          onValueChange={this.onLeftValueChange.bind(this)}
                          itemStyle={{ textAlign: 'center', fontFamily: DroidKufi }}>
                          <Picker.Item
                            label={L('left')}
                            value=""
                            key="val0"
                            disabled
                          />
                          {item.quantityPerSize &&
                            item.quantityPerSize.map(item => {
                              return (
                                <Picker.Item
                                  label={item.size}
                                  value={item.size_id}
                                  key={item.size_id}
                                />
                              );
                            })}
                        </Picker>
                        <Icon
                          type="Feather"
                          name="chevron-down"
                          style={{ fontSize: moderateScale(7) }}
                        />
                      </Item>
                    </View>
                  </View> : null
                }

                {(item.category.offer || (item.brand && item.brand.offer) || item.points || item.gift_points) ? <View
                  style={{
                    borderBottomColor: '#ddd',
                    borderBottomWidth: 1,
                    marginBottom: verticalScale(2),
                    paddingBottom: verticalScale(3),
                  }}>
                  {((item.category.offer && parseInt(item.category.offer.condition) > 0) ||
                    (item.brand.offer && parseInt(item.brand.offer.condition) > 0)) ?
                    <View
                      style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: verticalScale(2) }}>
                      <Text
                        style={[
                          styles.fontRegular,
                          {
                            backgroundColor: '#ff5252',
                            color: '#fff',
                            width: scale(25),
                            height: verticalScale(5),
                            borderRadius: moderateScale(15),
                            textAlignVertical: 'center',
                            textAlign: 'center',
                          },
                        ]}>
                        {L('offers')}
                      </Text>
                      <View
                        style={{
                          borderWidth: 1,
                          borderColor: '#f3f3f3',
                          flexDirection: 'row',
                          width: scale(65),
                          height: verticalScale(5),
                          justifyContent: 'space-between',
                          paddingHorizontal: scale(2),
                          borderRadius: moderateScale(8),
                        }}>
                        <View
                          style={{ width: scale(60), paddingTop: verticalScale(1), paddingHorizontal: 4 }}>
                          <Text style={[styles.fontRegular, { fontSize: moderateScale(4.5), textAlign: 'center', textAlignVertical: 'center', color: '#84817a' }]} >
                            {item.category.offer ?
                              `${L('buy')} ${item.category.offer.condition} ${L('get')} ${Math.abs(item.category.offer.result)} ${L('free')}` :
                              `${L('buy')} ${item.brand.offer.condition} ${L('get')} ${Math.abs(item.brand.offer.result)} ${L('free')}`
                            }
                          </Text>
                        </View>
                      </View>
                    </View>
                    : null}

                  {(item.points > 0) ?
                    <View
                      style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: verticalScale(2) }}>
                      <Text
                        style={[
                          styles.fontRegular,
                          {
                            backgroundColor: '#16a085',
                            color: '#fff',
                            width: scale(25),
                            height: verticalScale(5),
                            borderRadius: moderateScale(15),
                            textAlignVertical: 'center',
                            textAlign: 'center',
                          },
                        ]}>
                        {L('points')}
                      </Text>
                      <View
                        style={{
                          borderWidth: 1,
                          borderColor: '#f3f3f3',
                          flexDirection: 'row',
                          width: scale(65),
                          height: verticalScale(5),
                          justifyContent: 'space-between',
                          paddingHorizontal: scale(2),
                          borderRadius: moderateScale(8),
                        }}>
                        <View
                          style={{ width: scale(60), paddingTop: verticalScale(1), paddingHorizontal: 4 }}>
                          <Text style={[styles.fontRegular, { fontSize: moderateScale(4.5), textAlign: 'center', textAlignVertical: 'center', color: '#84817a' }]} >
                            {L('priceProductPoints')} {item.points}
                          </Text>
                        </View>
                      </View>
                    </View>
                    : null}

                  {(item.gift_points > 0) ?
                    <View
                      style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: verticalScale(2) }}>
                      <Text
                        style={[
                          styles.fontRegular,
                          {
                            backgroundColor: '#4a69bd',
                            color: '#fff',
                            width: scale(25),
                            height: verticalScale(5),
                            borderRadius: moderateScale(15),
                            textAlignVertical: 'center',
                            textAlign: 'center',
                          },
                        ]}>
                        {L('gift')}
                      </Text>
                      <View
                        style={{
                          borderWidth: 1,
                          borderColor: '#f3f3f3',
                          flexDirection: 'row',
                          width: scale(65),
                          height: verticalScale(5),
                          justifyContent: 'space-between',
                          paddingHorizontal: scale(2),
                          borderRadius: moderateScale(8),
                        }}>
                        <View
                          style={{ width: scale(60), paddingTop: verticalScale(1), paddingHorizontal: 4 }}>
                          <Text style={[styles.fontRegular, { fontSize: moderateScale(4.5), textAlign: 'center', textAlignVertical: 'center', color: '#84817a' }]} >
                            {L('giftPoints')} {item.gift_points}
                          </Text>
                        </View>
                      </View>
                    </View>
                    : null}
                </View> : null}
                {/* <Text
                    style={[
                      styles.fontRegular,
                      {
                        backgroundColor: 'rgba(140, 74, 164, 0.5)',
                        padding: scale(2),
                        textAlign: 'center',
                        borderRadius: scale(2),
                        color: '#FFF',
                      },
                    ]}>
                    {L('lensNotice')}
                  </Text>*/}
                {/* <View
                style={{
                  // marginVertical: verticalScale(1),
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  borderBottomColor: '#ddd',
                  borderBottomWidth: 1,
                }}>
                <Text
                  style={[
                    styles.fontRegular,
                    {
                      backgroundColor: primaryColor,
                      color: '#fff',
                      width: scale(25),
                      height: verticalScale(5),
                      borderRadius: moderateScale(15),
                      textAlignVertical: 'center',
                      textAlign: 'center',
                    },
                  ]}>
                  {L('eyeColor')}
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    width: scale(65),
                  }}>
                  <Item
                    rounded
                    style={[
                      styles.inputAuthContainer,
                      {
                        width: scale(65),
                        borderRadius: moderateScale(8),
                        height: verticalScale(5),
                        borderColor: '#f3f3f3',
                      },
                    ]}>
                    <Picker
                      mode="dropdown"
                      placeholder="لون العدسه"
                      placeholderStyle={{color: '#a2a2a2'}}
                      placeholderIconColor="#fff"
                      selectedValue={this.state.selected_color}
                      onValueChange={this.onColorChange.bind(this)}
                      itemStyle={{textAlign: 'center', fontFamily: DroidKufi}}>
                      <Picker.Item label="لون العدسه" value key="val0" disabled />
                      {item.colors &&
                        item.colors.map(item => {
                          return (
                            <Picker.Item
                              label={item.name}
                              value={item.id}
                              key={item.id}
                            />
                          );
                        })}
                    </Picker>
                    <Icon
                      type="Feather"
                      name="chevron-down"
                      style={{fontSize: moderateScale(7)}}
                    />
                  </Item>
                </View>
              </View> */}
                <Card noShadow>
                  <CardItem>
                    <Body>
                      {item.details != '' ? (
                        <ScrollView style={{ flex: 1 }}>
                          <HTML
                            html={item.details}
                            baseFontStyle={{ fontFamily: DroidKufi }}
                            ignoredStyles={["font-family", "letter-spacing", "display", "text-align"]}
                            imagesMaxWidth={Dimensions.get('window').width}
                            allowedStyles={["none", "flex"]}
                          />
                        </ScrollView>
                        /*<HTML style={styles.fontRegular} html={item.details} imagesMaxWidth={Dimensions.get('window').width} />*/
                      ) : (
                          <Text style={styles.fontRegular}>{L('noDetails')}</Text>
                        )}

                    </Body>
                  </CardItem>
                </Card>
              </View>
            </Content>
          </Content>
        ) : null}
        {this.props.product ? (
          <Footer
            // onPress={this.handleCartClick}
            style={{
              backgroundColor: '#fff',
              height: verticalScale(11),
              borderTopWidth: 0,
              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 12,
              },
              shadowOpacity: 0.58,
              shadowRadius: 16.0,
              elevation: 24,
              backgroundColor: '#fff',
            }}>
            <Button
              iconLeft
              block
              style={[styles.buttonStyle, {
                marginTop: 0, backgroundColor: ((this.state.pressedButton && this.state.giftProduct) ||
                  (this.state.pressedButton && this.state.chosenOfferItem)
                ) ? '#b2bec3' : primaryColor
              }]}
              onPress={() => {
                this.storeProduct()
                this.setState({ pressedButton: true })
              }}
              disabled={((this.state.pressedButton && this.state.giftProduct) ||
                (this.state.pressedButton && this.state.chosenOfferItem)
              )}
            >
              <Icon
                name="shopping-cart"
                type="Feather"
                style={{ fontSize: moderateScale(6) }}
              />
              <Text style={styles.buttonTextStyle}>{L('shopNow')}</Text>
            </Button>
          </Footer>
        ) : null}

        <View>
          <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.modalVisible}
            onRequestClose={this.closeModal.bind(this)}
            hardwareAccelerated={true}
            presentationStyle="overFullScreen">
            {/* <TouchableWithoutFeedback
              onPress={() => {
                this.setModalVisible(!this.state.modalVisible);
              }}> */}
            <View style={[styles.modalContainerlStyle, { width: scale(100) }]}>
              <View
                style={[
                  styles.modalStyle,
                  {
                    width: scale(100),
                    padding: scale(0),
                    justifyContent: 'center',
                    backgroundColor: 'rgba(0,0,0,0.3)',
                    alignContent: 'center',
                    height: verticalScale(100),
                    // paddingTop:verticalScale(20)
                    position: 'relative',
                  },
                ]}>
                <Content horizontal>
                  {this.props.product &&
                    this.props.product.photos &&
                    this.props.product.photos.map((item, index) => {
                      return (
                        <PhotoZoom
                          key={index}
                          source={{ uri: item.thumb }}
                          minimumZoomScale={1}
                          maximumZoomScale={5}
                          androidScaleType="center"
                          // onLoad={() => console.log('Image loaded!')}
                          style={{
                            width: scale(100),
                            height: verticalScale(100),
                            justifyContent: 'center',
                          }}
                        />
                      );
                    })}
                </Content>
                <TouchableWithoutFeedback
                  onPress={() => {
                    this.setModalVisible(!this.state.modalVisible);
                  }}>
                  <View
                    style={{
                      position: 'absolute',
                      top: verticalScale(4),
                      right: scale(4),
                    }}>
                    <Icon
                      name="close"
                      style={{
                        backgroundColor: 'rgba(265,265,265,0.4)',
                        width: 30,
                        height: 30,
                        borderRadius: 100,
                        textAlign: 'center',
                        textAlignVertical: 'center',
                        fontSize: 15,
                      }}
                    />
                  </View>
                </TouchableWithoutFeedback>
              </View>
            </View>
            {/* </TouchableWithoutFeedback> */}
          </Modal>
        </View>
        {this._renderError()}
        {this._renderLoading()}
      </Container>
    );
  }
}
const styles2 = StyleSheet.create({
  item: {

    width: "100%",
    height: screenWidth,
  },
  imageContainer: {
    //backgroundColor: "red",
    flex: 1,
    marginBottom: Platform.select({ ios: 0, android: 1 }), // Prevent a random Android rendering issue
    //backgroundColor: 'white',
  },
  image: {
    ...StyleSheet.absoluteFillObject,
    resizeMode: 'contain',
  },
});
const mapStateToProps = ({ productsR, ordersR, auth }) => {
  const { product, loading, message, wishlistArr } = productsR;
  const { cart, qty } = ordersR;
  const { user } = auth;
  // console.log('product', product);
  return { loading, message, product, cart, qty, user, wishlistArr };
};
export default connect(
  mapStateToProps,
  {
    getProduct,
    clearMessage,
    showMessageChanged,
    addToCart,
    addToWhishlist,
    removeToWishlist,
    getWhishlist,
    deleteWhishlist,
  },
)(Product);
