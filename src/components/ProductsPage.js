import React, {Component} from 'react';
import {
    Image,
    /*TouchableNativeFeedback,*/
    TouchableWithoutFeedback,
    FlatList,
    BackHandler, Dimensions, StyleSheet
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {Spinner} from './assets/common';
import {L} from '../Config';
import styles, {
  verticalScale,
  scale,
  moderateScale,
  buttonStyle,
  buttonTextStyle,
  textContainer,
  solidText,
  inputField,
  primaryColor,
  secondaryColor,
  fontBold,
  DroidKufi,
  DroidKufiBold,
  brandImage,
} from './assets/styles/Style';
import {
  Container,
  Content,
  Header,
  Item,
  Input,
  Button,
  Text,
  View,
  Icon,
  Toast,
} from 'native-base';
import {AirbnbRating} from 'react-native-ratings';
import FastImage from 'react-native-fast-image';
import {
    getProductsByBrandId,
    getProductsByCatId,
    clearMessage,
    showMessageChanged,
    getDiscountedProducts,
    productsByCatsBrand, getPrice, getSlidersByCategory, getSlidersByBrand,
} from '../actions';
import Carousel from 'react-native-snap-carousel';
import OfferTimer from "./OfferTimer";

const {width: screenWidth} = Dimensions.get('window');
const styles2 = StyleSheet.create({
    item: {
        width: "100%",
        height: screenWidth - 150,
        marginTop: verticalScale(0),
    },
    imageContainer: {
        flex: 1,
        marginBottom: Platform.select({ios: 0, android: 1}), // Prevent a random Android rendering issue
        backgroundColor: 'white',
        borderRadius: moderateScale(2),
        marginHorizontal: scale(2.5),
    },
    image: {
        ...StyleSheet.absoluteFillObject,
        resizeMode: 'cover',
        /*borderRadius: moderateScale(6),*/
        // overflow: 'hidden',
        alignSelf: 'center',
        //marginHorizontal: scale(0),
    },
});
class ProductsPage extends Component {
  state = {
    searchField: '',
  };
  onButtonPress = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    // then navigate
    // navigate('NewScreen');
  };

  handleBackButton = () => {
    Actions.pop();
    return true;
  };
    componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    // console.log(this.props.catId, this.props.brandId);
    if (this.props.catId == false) {
      this.props.getDiscountedProducts();
    } else if (
      this.props.catId !== undefined &&
      this.props.brandId !== undefined
    ) {
      // console.warn('a7a');
      this.props.productsByCatsBrand(this.props.catId, this.props.brandId);
    } else {
      if (this.props.brandId == undefined) {
        this.props.getProductsByCatId(this.props.catId);
          this.props.getSlidersByCategory(this.props.catId);
      } else {
        this.props.getProductsByBrandId(this.props.brandId);
          this.props.getSlidersByBrand(this.props.brandId);
      }
    }
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }
  _renderError() {
    if (this.props.message) {
      this.props.clearMessage();
      return Toast.show({
        text: this.props.message,
        buttonText: L('dismiss'),
        duration: 3000,
        style: {backgroundColor: primaryColor},
        textStyle: {fontFamily: DroidKufi, fontSize: moderateScale(4.5)},
        buttonTextStyle: {fontFamily: DroidKufi, fontSize: moderateScale(4.5)},
      });
    }
  }
  _renderLoading() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }
  }
    _renderSliderItem = ({item, index}) => {
        return (
            <View style={styles2.item} key={index}>
                <FastImage
                    source={{uri: item.photo, priority: FastImage.priority.normal}}
                    containerStyle={styles2.imageContainer}
                    style={styles2.image}
                />
            </View>
        );
    };
  _renderItem = item => {
    // console.log(item);
    item = item.item;
    return (
      <View>
      {this.props.discount && item.sale > 0 ? <View>
        <OfferTimer specialEndDate={item.specialEndDate} />
      </View>
      : null }
      <TouchableWithoutFeedback
        onPress={() =>
          Actions.push('product', {productID: item.id, title: item.title, chosenOfferItem: this.props.chosenOfferItem })
        }>
        <View style={[styles.productContainer, {position: 'relative'}]}>
          {((this.props.discount && item.sale > 0) || this.props.catId == false) ? (
            <Text
              style={[
                styles.fontRegular,
                {
                  position: 'absolute',
                  top: 0,
                  right: 0,
                  backgroundColor: primaryColor,
                  color: '#fff',
                  zIndex: 11,
                  paddingHorizontal: scale(2),
                },
              ]}>
              {L('discount')} {item.sale} %
            </Text>
          ) : null}
          <View>
            <FastImage
              source={{
                uri: item.photos[0] && item.photos[0].thumb,
                priority: FastImage.priority.normal,
              }}
              style={styles.productImage}
              resizeMode={FastImage.resizeMode.contain}
            />
          </View>
          <View style={styles.productDetails}>
            <Text
              numberOfLines={1}
              style={[styles.fontRegular, {color: primaryColor}]}>
              {item.title}
            </Text>
            <AirbnbRating
              showRating={false}
              isDisabled
              defaultRating={item.rating}
              ratingColor="#f4b700"
              size={10}
              starContainerStyle={{marginLeft: 0}}
            />
            <Text style={styles.fontRegular}>
              {getPrice(item.newPrice)} {L(global.currency.code)}
            </Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
      </View>
    );
  };
  render() {
    return (
      <Container>
        <Header
          androidStatusBarColor={primaryColor}
          style={styles.routernavigationBarStyle}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignSelf: 'center',
              alignItems: 'center',
              width: scale(100),
            }}>
            <Button
              onPress={() => Actions.pop()}
              transparent
              /*background={TouchableNativeFeedback.Ripple(secondaryColor)}*/
              style={styles.headerBack}>
              <Icon
                name="keyboard-backspace"
                type="MaterialIcons"
                style={{
                  color: '#fff',
                  fontSize: 25,
                  transform: [{scaleX: L('scaleLang')}],
                }}
              />
            </Button>
            <Text numberOfLines={1} style={styles.headerText}>
              {this.props.title}
            </Text>
            <Button
              onPress={() => Actions.jump('cart', {loading: true})}
              transparent
              /*background={TouchableNativeFeedback.Ripple(secondaryColor)}*/
              style={[styles.headerBack]}>
              <View
                style={{
                  position: 'absolute',
                  height: 15,
                  width: 15,
                  borderRadius: 15,
                  backgroundColor: '#fff',
                  left: 8,
                  top: 5,
                  alignItems: 'center',
                  justifyContent: 'center',
                  zIndex: 2000,
                }}>
                <Text
                  style={{
                    color: primaryColor,
                    fontFamily: DroidKufi,
                    fontSize: moderateScale(5),
                  }}>
                  {this.props.qty}
                </Text>
              </View>
              <Icon
                name="shopping-cart"
                type="Feather"
                style={{
                  fontSize: moderateScale(8),
                  color: '#fff',
                }}
              />
              {/* <Icon name='keyboard-backspace' type='MaterialIcons'
            style={styles.backButtonIcon}
        /> */}
            </Button>
          </View>
        </Header>
          <Content >
              <Carousel slideStyle={{height: screenWidth - 180}}
                        sliderWidth={screenWidth}
                        sliderHeight={screenWidth }
                        itemWidth={screenWidth - 0}
                        data={this.props.brandId == undefined ? this.props.categorySlider: this.props.brandSlider}
                        renderItem={this._renderSliderItem}
                        hasParallaxImages={false}
                        activeSlideAlignment="center"
                        autoplay={true}
                        loop={true}
                        inactiveSlideScale={1}
              />

        <Content padder>
          {this.props.catId == false ? (
            <FlatList
              data={this.props.disProducts}
              renderItem={this._renderItem}
              horizontal={false}
              numColumns={2}
              keyExtractor={(item, index) => index.toString()}
              extraData={this.state}
              columnWrapperStyle={{justifyContent: 'space-between'}}
              initialNumToRender={4}
            />
          ) : this.props.brandId !== undefined &&
            this.props.catId !== undefined ? (
            <FlatList
              data={this.props.brandCatProducts}
              renderItem={this._renderItem}
              horizontal={false}
              numColumns={2}
              keyExtractor={(item, index) => index.toString()}
              extraData={this.state}
              columnWrapperStyle={{justifyContent: 'space-between'}}
              initialNumToRender={4}
            />
          ) : this.props.brandId == undefined &&
            this.props.catId !== undefined ? (
            <FlatList
              data={this.props.catProducts}
              renderItem={this._renderItem}
              horizontal={false}
              numColumns={2}
              keyExtractor={(item, index) => index.toString()}
              extraData={this.state}
              columnWrapperStyle={{justifyContent: 'space-between'}}
              initialNumToRender={4}
            />
          ) : (
            <FlatList
              data={this.props.brandProducts}
              renderItem={this._renderItem}
              horizontal={false}
              numColumns={2}
              keyExtractor={(item, index) => index.toString()}
              extraData={this.state}
              columnWrapperStyle={{justifyContent: 'space-between'}}
              initialNumToRender={4}
            />
          )}
          {/* {this.props.brandId == undefined ? (
            <FlatList
              data={this.props.catProducts}
              renderItem={this._renderItem}
              horizontal={false}
              numColumns={2}
              keyExtractor={(item, index) => index.toString()}
              extraData={this.state}
              columnWrapperStyle={{justifyContent: 'space-between'}}
              initialNumToRender={4}
            />
          ) : (
            <FlatList
              data={this.props.brandProducts}
              renderItem={this._renderItem}
              horizontal={false}
              numColumns={2}
              keyExtractor={(item, index) => index.toString()}
              extraData={this.state}
              columnWrapperStyle={{justifyContent: 'space-between'}}
              initialNumToRender={4}
            />
          )} */}
        </Content>
          </Content>
        {this._renderError()}
        {this._renderLoading()}
      </Container>
    );
  }
}
const mapStateToProps = ({productsR, ordersR}) => {
  const {
    catProducts,
    brandProducts,
      brandSlider,
      categorySlider,
      loading,
    message,
    disProducts,
    brandCatProducts,
  } = productsR;
  const {cart, qty} = ordersR;
  // console.log(brandCatProducts);
  return {
    loading,
    message,
    brandProducts,
    catProducts,
    cart,
    qty,
      categorySlider,
      brandSlider,
    disProducts,
    brandCatProducts,
  };
};
export default connect(mapStateToProps, {
  getProductsByCatId,
  getProductsByBrandId,
  clearMessage,
    getSlidersByCategory,
    getSlidersByBrand,
  showMessageChanged,
  getDiscountedProducts,
  productsByCatsBrand,
})(ProductsPage);
