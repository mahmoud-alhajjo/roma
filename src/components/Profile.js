import React, { Component } from 'react';
import {
  Image,
  /*TouchableNativeFeedback,*/
  Alert, BackHandler, ScrollView, Linking, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Spinner } from './assets/common';
import { L } from '../Config';
import styles, {
  verticalScale,
  scale,
  buttonStyle,
  buttonTextStyle,
  moderateScale,
  inputField,
  primaryColor,
  fontBold,
  DroidKufi,
  DroidKufiBold,
  secondaryColor,
} from './assets/styles/Style';
import {
  Container,
  Content,
  Header,
  List,
  ListItem,
  Button,
  Text,
  View,
  Icon,
  Toast,
  Left,
  Body,
  Footer,
} from 'native-base';
import {
  pages,
  clearUser,
  getUser,
  showMessageChanged,
  clearMessage, getSettings,
} from '../actions';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import { SocialIcon } from 'react-native-elements'
// import * as Linking from "react-native";
import SocialMediaAddress from "./SocialMediaAddress";

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      language: 'ar',
      user: null,
    };
    this.navigationWillFocusListener = this.props.navigation.addListener('willFocus', async () => {
      try {
        let value = await AsyncStorage.getItem('user');
        const user = JSON.parse(value);
        if (user) {
          this.setState({ user: user });
          this._getData();
        } else {
          this._getInfo();
        }
      } catch (error) {
        // console.log(error);
      }
    })
  };

  onButtonPress = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    // then navigate
    // navigate('NewScreen');
  };

  handleBackButton = () => {

    BackHandler.exitApp();
    return true;
  };
  openInStor = () => {
    Platform.OS === 'ios' ?
      Linking.openURL(`https://apps.apple.com/us/app/id1524857901`) :
      Linking.openURL(`https://play.google.com/store/apps/details?id=com.romaopticals.waailh`)
  };
  async componentDidMount() {
    try {
      let value = await AsyncStorage.getItem('user');
      // console.log(value);
      const user = JSON.parse(value);
      if (user) {
        this.setState({ user: user });
        this._getData();
      } else {
        this._getInfo();
      }
      // console.log(user);
    } catch (error) {
      // console.log(error);
      // this._getData()
    }
  }
  _getData() {
    // console.log('use', this.state.user);
    this.props.pages();
    this.props.getUser(this.state.user.id);
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  _getInfo() {
    this.props.pages();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  componentWillUnmount() {
    this.navigationWillFocusListener.remove()
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }
  onPushRegister() {
    Actions.reset('auth');
    Actions.push('register');
  }
  _logOut = async () => {
    // console.log('sdds');
    try {
      await AsyncStorage.removeItem('user');
    } catch (error) { }
    this.props.clearUser();
    Actions.reset('auth');
  };
  _renderError() {
    if (this.props.message) {
      this.props.clearMessage();
      return Toast.show({
        text: this.props.message,
        buttonText: L('dismiss'),
        duration: 3000,
        style: { backgroundColor: primaryColor },
        textStyle: { fontFamily: DroidKufi, fontSize: moderateScale(4.5) },
        buttonTextStyle: { fontFamily: DroidKufi, fontSize: moderateScale(4.5) },
      });
    }
  }
  _renderLoading() {
    if (this.props.loading || this.props.othersLoading) {
      return <Spinner size="large" />;
    }
  }
  render() {
    return (
      <Container>
        <Header
          androidStatusBarColor={primaryColor}
          style={styles.routernavigationBarStyle}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              alignSelf: 'center',
              alignItems: 'center',
              width: scale(100),
            }}>
            <Text
              numberOfLines={1}
              style={[styles.fontRegular, { color: '#fff', fontSize: 14 }]}>
              {L('myAccount')}
            </Text>
          </View>
        </Header>
        <Content>
          {this.state.user != null ? (
            <View
              style={{
                // justifyContent: 'center',
                paddingTop: verticalScale(1),
                alignItems: 'center',
                height: verticalScale(20),
                backgroundColor: primaryColor,
              }}>
              { this.props.user && this.props.user.photo ?
                <Image
                  source={{ uri: this.props.user.photo }}
                  style={{
                    width: scale(24),
                    height: verticalScale(12.5),
                    borderRadius: moderateScale(100),
                    borderWidth: 1,
                    borderColor: secondaryColor,
                    backgroundColor: '#fff',
                  }}
                /> : null
              }
              <Text
                style={[
                  styles.fontRegular,
                  { color: '#fff', marginVertical: verticalScale(1) },
                ]}>
                {this.props.user && this.props.user.name}
              </Text>
            </View>
          ) : null}
          <List>
            {this.state.user != null ? (
              <View>
                <ListItem itemDivider>
                  <Text style={[styles.fontRegular, { color: primaryColor }]}>
                    {L('myAccount')}
                  </Text>
                </ListItem>
                <ListItem icon onPress={() => Actions.push('editProfile')}>
                  <Left>
                    <Icon name="edit" type="Feather" style={styles.InputIcon} />
                  </Left>
                  <Body>
                    <Text style={[styles.fontRegular, { color: '#686e7f' }]}>
                      {L('editProfile')}
                    </Text>
                  </Body>
                </ListItem>
                <ListItem
                  icon
                  onPress={() =>
                    Actions.push('address', { title: L('addresses') })
                  }>
                  <Left>
                    <Icon
                      name="globe"
                      type="Feather"
                      style={styles.InputIcon}
                    />
                  </Left>
                  <Body>
                    <Text style={[styles.fontRegular, { color: '#686e7f' }]}>
                      {L('addresses')}
                    </Text>
                  </Body>
                </ListItem>
                <ListItem
                  icon
                  onPress={() =>
                    Actions.push('myOrders', { title: L('myOrders') })
                  }>
                  <Left>
                    <Icon name="box" type="Feather" style={styles.InputIcon} />
                  </Left>
                  <Body>
                    <Text style={[styles.fontRegular, { color: '#686e7f' }]}>
                      {L('myOrders')}
                    </Text>
                  </Body>
                </ListItem>
                <ListItem
                  icon
                  onPress={() =>
                    Actions.push('about', {
                      title: L('myPoints'),
                      pagesElement: this.props.user && this.props.user,
                      myPoints: true
                    })
                  }>
                  <Left>
                    <Icon name="dotchart" type="AntDesign" style={styles.InputIcon} />
                  </Left>
                  <Body>
                    <Text style={[styles.fontRegular, { color: '#686e7f' }]}>
                      {L('myPoints')}
                    </Text>
                  </Body>
                </ListItem>
                <ListItem
                  icon
                  onPress={() =>
                    Actions.push('about', {
                      title: L('mygifts'),
                      pagesElement: this.props.user && this.props.user,
                    })
                  }>
                  <Left>
                    <Icon name="gift" type="AntDesign" style={styles.InputIcon} />
                  </Left>
                  <Body>
                    <Text style={[styles.fontRegular, { color: '#686e7f' }]}>
                      {L('mygifts')}
                    </Text>
                  </Body>
                </ListItem>
                <ListItem
                  icon
                  onPress={() =>
                    Actions.push('contact', { title: L('contactUs') })
                  }>
                  <Left>
                    <Icon
                      name="phone"
                      type="Feather"
                      style={styles.InputIcon}
                    />
                  </Left>
                  <Body>
                    <Text style={[styles.fontRegular, { color: '#686e7f' }]}>
                      {L('contactUs')}
                    </Text>
                  </Body>
                </ListItem>
              </View>
            ) : null}
            <ListItem
              icon
              onPress={() =>
                Actions.push('language', {
                  langPush: true,
                })
              }>
              <Left>
                <Icon name="globe" type="Feather" style={styles.InputIcon} />
              </Left>
              <Body>
                <Text style={[styles.fontRegular, { color: '#686e7f' }]}>
                  {L('changeLanguage')}
                </Text>
              </Body>
            </ListItem>
            {this.props.pagesContent.length > 0 &&
              this.props.pagesContent.map((item, index) => {
                var iconName;
                if (item.id == 1) {
                  iconName = 'info';
                } else if (item.id == 2) {
                  iconName = 'key';
                } else if (item.id == 3) {
                  iconName = 'file-text';
                } else if (item.id == 4) {
                  iconName = 'refresh-ccw';
                } else if (item.id == 5) {
                  iconName = 'target';
                } else if (item.id == 6) {
                  iconName = 'truck';
                }
                return (
                  <ListItem
                    key={index}
                    icon
                    onPress={() =>
                      Actions.push('about', {
                        title: item.title,
                        pagesElement: item,
                      })
                    }>
                    <Left>
                      <Icon
                        name={iconName}
                        type="Feather"
                        style={styles.InputIcon}
                      />
                    </Left>
                    <Body>
                      <Text style={[styles.fontRegular, { color: '#686e7f' }]}>
                        {item.title}
                      </Text>
                    </Body>
                  </ListItem>
                );
              })}
            <ListItem icon onPress={this.openInStor}>
              <Left>
                <Icon name="staro" type="AntDesign" style={styles.InputIcon} />
              </Left>
              <Body>
                <Text style={[styles.fontRegular, { color: '#686e7f' }]}>
                  {L('rateUs')}
                </Text>
              </Body>
            </ListItem>
            {this.state.user != null ? (
              <View>
                <ListItem itemDivider>
                  <Text style={[styles.fontRegular, { color: primaryColor }]}>
                    {L('myInfo')}
                  </Text>
                </ListItem>
                <ListItem icon>
                  <Left>
                    <Icon name="user" type="Feather" style={styles.InputIcon} />
                  </Left>
                  <Body>
                    <Text
                      style={[
                        styles.fontRegular,
                        { color: '#686e7f', textAlign: L('textAlignLeft') },
                      ]}>
                      {' '}
                      {this.props.user && this.props.user.name}
                    </Text>
                  </Body>
                </ListItem>
                <ListItem icon>
                  <Left>
                    <Icon name="mail" type="Feather" style={styles.InputIcon} />
                  </Left>
                  <Body>
                    <Text
                      style={[
                        styles.fontRegular,
                        { color: '#686e7f', textAlign: L('textAlignLeft') },
                      ]}>
                      {' '}
                      {this.props.user && this.props.user.email}
                    </Text>
                  </Body>
                </ListItem>
                <ListItem icon>
                  <Left>
                    <Icon
                      name="phone"
                      type="Feather"
                      style={styles.InputIcon}
                    />
                  </Left>
                  <Body>
                    <Text
                      style={[
                        styles.fontRegular,
                        { color: '#686e7f', textAlign: L('textAlignLeft') },
                      ]}>
                      {' '}
                      {this.props.user && this.props.user.phone}
                    </Text>
                  </Body>
                </ListItem>
              </View>
            ) : null}
          </List>
          <ScrollView>
            <SocialMediaAddress settings={this.props.settings} />
          </ScrollView>
          {this.state.user != null ? (
            <View
              style={{
                backgroundColor: '#f4f4f4',
                paddingBottom: verticalScale(2),
                paddingTop: verticalScale(1),
              }}>
              <Button
                style={styles.buttonStyle}
                onPress={() =>
                  Alert.alert(
                    L('logOut'),
                    L('logoutMsg'),
                    [
                      { text: L('no'), style: 'cancel' },
                      { text: L('yes'), onPress: () => this._logOut() },
                    ],
                    { cancelable: false },
                  )
                }>
                <Text style={styles.buttonTextStyle}>{L('logOut')}</Text>
              </Button>
            </View>
          ) : null}
        </Content>
        {this.state.user == null ? (
          <Footer
            style={{ height: verticalScale(20), backgroundColor: 'transparent' }}>
            <View>
              <Button
                style={styles.buttonStyle}
                onPress={() => Actions.reset('auth')}>
                <Text style={styles.buttonTextStyle}>{L('login')}</Text>
              </Button>
              <Button
                style={styles.buttonStyle}
                onPress={() => this.onPushRegister()}>
                <Text style={styles.buttonTextStyle}>{L('register')}</Text>
              </Button>
            </View>
          </Footer>
        ) : null}
        {this._renderError()}
        {this._renderLoading()}
      </Container>
    );
  }
}

const mapStateToProps = ({ auth, others }) => {
  const { loading, message, user } = auth;
  const { pagesContent, settings } = others;
  const othersLoading = others.loading;
  // console.log(user);
  return { loading, message, pagesContent, user, settings };
};
export default connect(mapStateToProps, {
  pages,
  getSettings,
  clearUser,
  getUser,
  showMessageChanged,
  clearMessage,
})(Profile);
