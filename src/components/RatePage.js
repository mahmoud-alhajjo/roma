import React, {Component} from 'react';
import {
  Image,
  /*TouchableNativeFeedback,*/
  TouchableOpacity,
  FlatList,
  BackHandler
} from 'react-native';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {Spinner} from './assets/common';
import {L} from '../Config';
import styles, {
  verticalScale,
  scale,
  moderateScale,
  buttonStyle,
  buttonTextStyle,
  textContainer,
  solidText,
  inputField,
  primaryColor,
  secondaryColor,
  fontBold,
  DroidKufi,
  DroidKufiBold,
  brandImage,
} from './assets/styles/Style';
import {
  Container,
  Content,
  Header,
  Item,
  Input,
  Button,
  Text,
  View,
  Icon,
  Toast,
} from 'native-base';
import FastImage from 'react-native-fast-image';
import {AirbnbRating} from 'react-native-ratings';

import {getComments, clearMessage, showMessageChanged} from '../actions';

class RatePage extends Component {
  state = {
    searchField: '',
  };
  onButtonPress = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    // then navigate
    // navigate('NewScreen');
  };

  handleBackButton = () => {
    Actions.pop();
    return true;
  };
  componentDidMount() {
    this.props.getComments(this.props.productID);
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }
  _renderError() {
    if (this.props.message) {
      this.props.clearMessage();
      return Toast.show({
        text: this.props.message,
        buttonText: L('dismiss'),
        duration: 3000,
        style: {backgroundColor: primaryColor},
        textStyle: {fontFamily: DroidKufi, fontSize: moderateScale(4.5)},
        buttonTextStyle: {fontFamily: DroidKufi, fontSize: moderateScale(4.5)},
      });
    }
  }
  _renderLoading() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }
  }
  _renderItem = item => {
    item = item.item;
    return (
      <View
        style={{
          flexDirection: 'row',
          width: scale(90),
          backgroundColor: '#fff',
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.25,
          shadowRadius: 3.84,
          elevation: 5,
          alignSelf: 'center',
          marginBottom: verticalScale(1.5),
          padding: scale(5),
        }}>
        <View
          style={{
            borderWidth: 1,
            borderColor: '#ddd',
            borderRadius: scale(100),
            marginRight: scale(2),
          }}>
          <FastImage
            style={{
              width: scale(20),
              height: verticalScale(11),
              borderRadius: scale(100),
            }}
            source={{
              uri: item.photo,
              priority: FastImage.priority.normal,
            }}
            resizeMode={FastImage.resizeMode.contain}
          />
        </View>
        <View style={{alignSelf: 'center'}}>
          <Text
            style={[
              styles.fontRegular,
              {
                textAlign: L('textAlignLeft'),
                fontSize: moderateScale(7),
                color: '#515151',
              },
            ]}>
            {item.name}
          </Text>
          <Text
            style={[
              styles.fontRegular,
              {
                textAlign: L('textAlignLeft'),
                fontSize: moderateScale(5),
                color: primaryColor,
                marginBottom: verticalScale(1),
              },
            ]}>
            {item.comment}
          </Text>
          <AirbnbRating
            showRating={false}
            isDisabled
            defaultRating={item.rating}
            ratingColor={primaryColor}
            size={10}
            starContainerStyle={{marginLeft: 0}}
          />
        </View>
      </View>
    );
  };
  render() {
    return (
      <Container>
        <Header
          androidStatusBarColor={primaryColor}
          style={styles.routernavigationBarStyle}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignSelf: 'center',
              alignItems: 'center',
              width: scale(100),
            }}>
            <Button
              onPress={() => Actions.pop()}
              transparent
              /*background={TouchableNativeFeedback.Ripple(secondaryColor)}*/
              style={styles.headerBack}>
              <Icon
                name="keyboard-backspace"
                type="MaterialIcons"
                style={{
                  color: '#fff',
                  fontSize: 25,
                  transform: [{scaleX: L('scaleLang')}],
                }}
              />
            </Button>
            <Text numberOfLines={1} style={styles.headerText}>
              {this.props.title}
            </Text>
          </View>
        </Header>
        <Content
          style={{backgroundColor: '#f9f9f9', paddingTop: verticalScale(1.5)}}>
          {this.props.comments.length > 0 ? (
            <FlatList
              data={this.props.comments}
              renderItem={this._renderItem}
              horizontal={false}
              // numColumns={2}
              keyExtractor={(item, index) => index.toString()}
              extraData={this.state}
              // columnWrapperStyle={{justifyContent: 'space-between'}}
              initialNumToRender={4}
            />
          ) : (
            <View
              style={{
                alignSelf: 'center',
                justifyContent: 'center',
                height: verticalScale(50),
              }}>
              <View
                style={{
                  backgroundColor: '#f9f9f9',
                  alignSelf: 'center',
                  width: scale(60),
                  height: verticalScale(32),
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: moderateScale(100),
                  borderWidth: 0.3,
                  borderColor: primaryColor,
                }}>
                <Icon
                  name="message-circle"
                  type="Feather"
                  style={{
                    fontSize: moderateScale(35),
                    color: '#ddd',
                  }}
                />
              </View>
              <Text
                style={[
                  styles.fontRegular,
                  {textAlign: 'center', marginTop: verticalScale(3)},
                ]}>
                {L('noNot')} {L('comments')}
              </Text>
            </View>
          )}
        </Content>
        {this._renderError()}
        {this._renderLoading()}
      </Container>
    );
  }
}
const mapStateToProps = ({others}) => {
  const {message, loading, comments} = others;
  // console.log(catsByParent);
  return {loading, message, comments};
};
export default connect(mapStateToProps, {
  getComments,
  clearMessage,
  showMessageChanged,
})(RatePage);
