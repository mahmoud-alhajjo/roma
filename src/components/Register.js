import React, {Component} from 'react';
import {Image,
    /*TouchableNativeFeedback,*/
    BackHandler} from 'react-native';
import PhotoUpload from 'react-native-photo-upload';
import {connect} from 'react-redux';
import {Spinner} from '../components/assets/common';
import firebase from 'react-native-firebase';
import styles, {
  verticalScale,
  scale,
  buttonStyle,
  buttonTextStyle,
  moderateScale,
  DroidKufi,
  textAreaContainer,
  textAreaField,
  photoUpload,
  photo,
  primaryColor,
} from './assets/styles/Style';
import {
  Container,
  Content,
  Label,
  Item,
  Toast,
  Input,
  Button,
  Text,
  View,
  Icon,
  Header,
  Left,
} from 'native-base';
import {L} from '../Config';
import ImageResizer from 'react-native-image-resizer';
import AsyncStorage from '@react-native-community/async-storage';
import {
  registerUser,
  clearMessage,
  showMessageChanged,
  emailUserChanged,
  uploadPhoto,
  passwordChanged,
  confirmPasswordChanged,
  phoneChanged,
  usernameChanged,
} from '../actions';
import SocialLogin from "./SocialLogin";

class Register extends Component {
  state = {
    name: '',
    email: '',
    phone: '',
    photo: '',
    password: '',
    token: '',
    showPassword: true,
  };
  onButtonPress = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    // then navigate
    // navigate('NewScreen');
  };

  handleBackButton = () => {
    Actions.pop();
    return true;
  };
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }
  componentWillMount() {
      AsyncStorage.getItem('oneSignalProviderId').then(res => {
          this.setState({token: res});
          console.log("token is " , res);
      });
    // this.getToken();
    this.getLanguage();
  }
  getLanguage = async () => {
    try {
      lang = await AsyncStorage.getItem('language');
      // console.log(lang);

      this.setState({language: lang});
    } catch (error) {}
  };
  /*getToken = async () => {
    const fcmToken = await firebase.messaging().getToken();
    if (fcmToken) {
      this.setState({token: fcmToken});
    } else {
    }
  };*/
  onRegister() {
    const {token} = this.state;
    const {name, phone, email, password, rePassword, photo} = this.props;
    if (name == '') {
      this.props.showMessageChanged(L('emptyField') + ' ' + L('username'));
    } else if (email == '') {
      this.props.showMessageChanged(L('emptyField') + ' ' + L('email'));
    } else if (phone == '') {
      this.props.showMessageChanged(L('emptyField') + ' ' + L('phone'));
    } else if (phone.length < 10) {
        this.props.showMessageChanged(L('invalidPhone'));
    } else if (password == '') {
      this.props.showMessageChanged(L('emptyField') + ' ' + L('password'));
    } else if (password != rePassword) {
      this.props.showMessageChanged(L('notPassword'));
    } else {
      const user = {
        name,
        email,
        phone,
        photo,
        password,
        token,
      };
      this.props.registerUser({user});
      // console.log({ user });
    }
  }
  _renderError() {
    if (this.props.message) {
      this.props.clearMessage();
      return Toast.show({
        text: this.props.message,
        buttonText: L('dismiss'),
        duration: 3000,
        style: {backgroundColor: primaryColor},
        textStyle: {fontFamily: DroidKufi, fontSize: moderateScale(4.5)},
        buttonTextStyle: {fontFamily: DroidKufi, fontSize: moderateScale(4.5)},
      });
    }
  }
  _renderLoading() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }
  }
  onUsernameChanged(text) {
    this.props.usernameChanged(text);
  }
  onPhoneChanged(text) {
      text = text.replace(/[^0-9]/g, '');
    this.props.phoneChanged(text);
  }
  emailUserChanged(text) {
    this.props.emailUserChanged(text);
  }
  onPasswordChange(text) {
    this.props.passwordChanged(text);
  }
  onRePasswordChange(text) {
    this.props.confirmPasswordChanged(text);
  }
  renderShowHidePass() {
    if (this.state.showPassword == true) {
      return (
        <Icon
          name="eye-off"
          style={{color: '#929292'}}
          /*background={TouchableNativeFeedback.Ripple(primaryColor)}*/
          onPress={() => this.setState({showPassword: false})}
        />
      );
    } else {
      return (
        <Icon
          transparent
          name="eye"
          style={{color: primaryColor}}
          /*background={TouchableNativeFeedback.Ripple(primaryColor)}*/
          onPress={() => this.setState({showPassword: true})}
        />
      );
    }
  }
  render() {
    return (
      <Container>
        <Header
          style={{display: 'none'}}
          androidStatusBarColor={primaryColor}
        />
        <Content style={{height: verticalScale(90)}}>
          <Item
            style={{
              borderBottomWidth: 0,
              flexDirection: 'row',
              justifyContent: 'flex-end',
              //width: scale(90),
              alignSelf: 'center',
              marginBottom: verticalScale(3),
            }}>
            {/*<PhotoUpload
              containerStyle={styles.photoUpload}
              onResponse={image => {
                if (image.fileName) {
                  ImageResizer.createResizedImage(
                    image.path,
                    300,
                    250,
                    'JPEG',
                    30,
                  )
                    .then(image => {
                      this.props.uploadPhoto(image, 1);
                      // resizeImageUri is the URI of the new image that can now be displayed, uploaded...
                    })
                    .catch(err => {
                      // Oops, something went wrong. Check that the filename is correct and
                      // inspect err to get more details.
                    });
                  // console.log(image.fileName);
                }
              }}>
              <Image
                style={[
                  styles.photo,
                  {borderRadius: 100, marginBottom: verticalScale(3)},
                ]}
                resizeMode="cover"
                source={require('./assets/images/splash.png')}
              />
              <View
                style={{
                  backgroundColor: primaryColor,
                  borderRadius: 100,
                  width: scale(7.5),
                  height: verticalScale(4),
                  position: 'absolute',
                  right: scale(14),
                  bottom: verticalScale(5),
                  textAlign: 'center',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  name="add"
                  style={{
                    color: '#fff',
                    fontSize: moderateScale(6),
                  }}
                />
              </View>
              <Text style={[styles.fontRegular, {color: primaryColor}]}>
                {L('addUserPhoto')}
              </Text>
            </PhotoUpload>*/}
            <Image
              style={{width: scale(50)}}
              resizeMode="contain"
              source={require('./assets/images/logo.png')}
            />
          </Item>
          <Item
            style={[
              styles.inputAuthContainer,
              {
                borderTopWidth: 0,
                borderLeftWidth: 0,
                borderRightWidth: 0,
              },
            ]}
            floatingLabel>
            <Label style={styles.inputLabel}>{L('username')}</Label>
            <Input
              onChangeText={this.onUsernameChanged.bind(this)}
              value={this.props.name}
              style={[styles.inputField, {textAlign: L('textAlignRight')}]}
            />
          </Item>
          <Item
            style={[
              styles.inputAuthContainer,
              {
                borderTopWidth: 0,
                borderLeftWidth: 0,
                borderRightWidth: 0,
              },
            ]}
            floatingLabel>
            <Label style={styles.inputLabel}>{L('email')}</Label>
            <Input
              onChangeText={this.emailUserChanged.bind(this)}
              value={this.props.email}
              style={[styles.inputField, {textAlign: L('textAlignRight')}]}
            />
          </Item>
          <Item
            style={[
              styles.inputAuthContainer,
              {
                borderTopWidth: 0,
                borderLeftWidth: 0,
                borderRightWidth: 0,
              },
            ]}
            floatingLabel>
            <Label style={styles.inputLabel}>{L('phone')}</Label>
            <Input
              onChangeText={this.onPhoneChanged.bind(this)}
              value={this.props.phone}
              style={[styles.inputField, {textAlign: L('textAlignRight')}]}
              keyboardType={'numeric'}
            />
          </Item>
          <Item
            style={[
              styles.inputAuthContainer,
              {
                borderTopWidth: 0,
                borderLeftWidth: 0,
                borderRightWidth: 0,
              },
            ]}
            floatingLabel>
            <Label style={styles.inputLabel}>{L('password')}</Label>
            <Input
              onChangeText={this.onPasswordChange.bind(this)}
              secureTextEntry={this.state.showPassword}
              value={this.props.password}
              style={[styles.inputField, {textAlign: L('textAlignRight')}]}
            />
            {this.renderShowHidePass()}
          </Item>
          <Item
            style={[
              styles.inputAuthContainer,
              {
                borderTopWidth: 0,
                borderLeftWidth: 0,
                borderRightWidth: 0,
              },
            ]}
            floatingLabel>
            <Label style={styles.inputLabel}>{L('confirm_password')}</Label>
            <Input
              onChangeText={this.onRePasswordChange.bind(this)}
              secureTextEntry={this.state.showPassword}
              value={this.props.rePassword}
              style={[styles.inputField, {textAlign: L('textAlignRight')}]}
            />
            {this.renderShowHidePass()}
          </Item>
          <Item style={{borderBottomWidth: 0, alignSelf: 'center'}}>
            <Button
              block
              style={[
                styles.buttonStyle,
                {
                  marginBottom: verticalScale(2),
                },
              ]}
              onPress={() => this.onRegister()}>
              <Text style={styles.buttonTextStyle}>{L('register_now')}</Text>
            </Button>
          </Item>
            <Item>
                <SocialLogin/>
            </Item>
        </Content>
        {this._renderLoading()}
        {this._renderError()}
      </Container>
    );
  }
}

const mapStateToProps = ({auth}) => {
  const {
    user,
    loading,
    message,
    photo,
    name,
    phone,
    photo_thumb,
    rePassword,
    email,
    password,
  } = auth;
  // console.log(photo);
  return {
    user,
    loading,
    message,
    photo,
    photo_thumb,
    name,
    phone,
    email,
    password,
    rePassword,
  };
};

export default connect(mapStateToProps, {
  registerUser,
  clearMessage,
  passwordChanged,
  showMessageChanged,
  emailUserChanged,
  uploadPhoto,
  confirmPasswordChanged,
  phoneChanged,
  usernameChanged,
})(Register);
