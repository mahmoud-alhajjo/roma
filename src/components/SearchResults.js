import React, {Component} from 'react';
import {
  Image,
  TouchableWithoutFeedback,
  FlatList,
  TouchableOpacity,
  /*TouchableNativeFeedback,*/
  BackHandler
} from 'react-native';
import {connect} from 'react-redux';
import {Spinner} from './assets/common';
import {L} from '../Config';
import {AirbnbRating} from 'react-native-ratings';
import FastImage from 'react-native-fast-image';
import styles, {
  verticalScale,
  scale,
  buttonStyle,
  buttonStyle3,
  buttonTextStyle,
  moderateScale,
  secondaryColor,
  primaryColor,
} from './assets/styles/Style';
import {
  Container,
  Content,
  Toast,
  Text,
  View,
  Icon,
  Button,
  CardItem,
  Header,
} from 'native-base';
import {Actions} from 'react-native-router-flux';
import {clearMessage, search, showMessageChanged, filter, getPrice} from '../actions';

class SearchResults extends Component {
  state = {
    search: '',
    searchResult: [],
  };
  onButtonPress = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    // then navigate
    // navigate('NewScreen');
  };

  handleBackButton = () => {
    Actions.pop();
    return true;
  };
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    // console.log(this.props.postFilter,this.props.filterCheck);
    if (this.props.filterCheck == true) {
      this.props.filter(this.props.postFilter);
    } else {
      this.props.search(this.props.postSearch);
    }
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }
  _renderError() {
    // if (this.props.updateAssc) {
    //     this._getData()
    // }
    if (this.props.message) {
      this.props.clearMessage();
      return Toast.show({
        text: this.props.message,
        buttonText: 'تم',
        duration: 3000,
        style: {backgroundColor: '#000'},
      });
    }
  }
  renderLoading() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }
  }
  _renderItem({item}) {
    // console.log(item);

    return (
      <TouchableWithoutFeedback
        onPress={() =>
          Actions.push('product', {productID: item.id, title: item.title})
        }>
        <View style={styles.productContainer}>
          <View>
            <FastImage
              source={{
                uri: item.photos[0] && item.photos[0].thumb,
                priority: FastImage.priority.normal,
              }}
              style={styles.productImage}
              resizeMode={FastImage.resizeMode.contain}
            />
          </View>
          <View style={styles.productDetails}>
            <Text
              numberOfLines={1}
              style={[styles.fontRegular, {color: primaryColor}]}>
              {item.title}
            </Text>
            <AirbnbRating
              showRating={false}
              isDisabled
              defaultRating={item.rating}
              ratingColor="#f4b700"
              size={10}
              starContainerStyle={{marginLeft: 0}}
            />
            <Text style={styles.fontRegular}>
              {getPrice(item.price)} {L(global.currency.code)}
            </Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
  render() {
    return (
      <Container>
        <Header
          androidStatusBarColor={primaryColor}
          style={styles.routernavigationBarStyle}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignSelf: 'center',
              alignItems: 'center',
              width: scale(100),
            }}>
            <Button
              onPress={() => Actions.pop()}
              transparent
              /*background={TouchableNativeFeedback.Ripple(secondaryColor)}*/
              style={styles.headerBack}>
              <Icon
                name="keyboard-backspace"
                type="MaterialIcons"
                style={{
                  color: '#fff',
                  fontSize: 25,
                  transform: [{scaleX: L('scaleLang')}],
                }}
              />
            </Button>
            <Text numberOfLines={1} style={styles.headerText}>
              {this.props.title}
            </Text>
          </View>
        </Header>
        <Content padder>
          {this.props.searchResult && this.props.searchResult.length == 0 ? (
            <View
              style={{
                alignSelf: 'center',
                justifyContent: 'center',
                height: verticalScale(50),
                flexDirection: 'column',
              }}>
              <Icon
                name="thumbnails"
                type="Foundation"
                style={{
                  color: '#ddd',
                  alignSelf: 'center',
                  fontSize: moderateScale(70),
                }}
              />
              <Text
                style={[
                  styles.fontBold,
                  {
                    color: '#ddd',
                    alignSelf: 'center',
                    fontSize: moderateScale(7),
                  },
                ]}>
                لا يوجد نتائج
              </Text>
            </View>
          ) : (
            <FlatList
              data={this.props.searchResult}
              renderItem={this._renderItem}
              horizontal={false}
              numColumns={2}
              keyExtractor={(item, index) => index.toString()}
              extraData={this.state}
              columnWrapperStyle={{justifyContent: 'space-between'}}
            />
          )}
        </Content>
        {this.renderLoading()}
        {this._renderError()}
      </Container>
    );
  }
}

const mapStateToProps = ({auth, others}) => {
  const {user} = auth;
  const {searchResult, loading, message, refresh} = others;
  // console.log(searchResult);

  return {message, loading, user, searchResult, refresh};
};

export default connect(mapStateToProps, {
  clearMessage,
  showMessageChanged,
  search,
  filter,
})(SearchResults);
