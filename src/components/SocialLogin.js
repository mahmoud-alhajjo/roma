import React, { Component } from 'react';
import {DroidKufi, fontColor, moderateScale, secondaryColor} from "./assets/styles/Style";
import {L} from "../Config";
import {Text, Toast, View} from "native-base";
import {getSettings, loginSocialUser} from "../actions";
import {connect} from "react-redux";
import AsyncStorage from "@react-native-community/async-storage";
import {Image, StyleSheet, TouchableOpacity} from "react-native";
import InstagramLogin from 'react-native-instagram-login';
import {AccessToken, GraphRequest, LoginManager, GraphRequestManager} from "react-native-fbsdk";
import I18n  from 'react-native-i18n';
class SocialLogin extends Component {
    constructor(properties) {
        super(properties);
    }
    state = {
        language: 'ar',
        token: null,
        showFacebookSocialLogin: false,
        showInstaSocialLogin: false,
    };
    render() {
        return (
            <View style={styles.container}>
                {this.renderFacebookLogin()}
                {this.renderInstaLogin()}
            </View>
        );
    }
    renderFacebookLogin() {
        return (
        this.state.showFacebookSocialLogin ?
            <View style={styles.item}>
                <TouchableOpacity onPress={() => this.loginWithFacebook()}>
                    <Image style={styles.loginImages} source={require('./assets/images/facebookLogin.png')}/>
                    {/*<Text style={{color: 'white'}}> Login With Facebook </Text>*/}
                </TouchableOpacity>
            </View>
            : null
        );
    }
    renderInstaLogin() {
        return (
            this.state.showInstaSocialLogin ?
                <View style={styles.item}>
                    <TouchableOpacity onPress={()=> this.instagramLogin.show()}>
                        <Image style={styles.loginImages} source={require('./assets/images/instaLogin.png')} />
                        {/*<Text style={{color: 'white'}}>Login</Text>*/}
                    </TouchableOpacity>
                    <InstagramLogin
                        ref={ref => (this.instagramLogin = ref)}
                        responseType='code'
                        appId='775123343301823'
                        appSecret='b2651d22c9ce8af4559d6d408a2596d1'
                        redirectUrl='https://www.google.com/'
                        scopes={['user_profile']}
                        onLoginSuccess={this.loginWithInstagram}
                        onLoginFailure={(data) => { Toast.show({
                            text: I18n.local === 'en' ? 'failed login via instagram' : 'فشل تسجيل الدخول عن طريق الانستغرام',
                            buttonText: L('dismiss'),
                            duration: 3000,
                            style: {backgroundColor: secondaryColor, color: fontColor},
                            textStyle: {fontFamily: DroidKufi, fontSize: moderateScale(4.5)},
                            buttonTextStyle: {fontFamily: DroidKufi, fontSize: moderateScale(4.5)},
                        });
                            console.log('data', data);
                        }}
                    />
                </View>
                : null
        );
    }
    componentDidMount = async () => {
        AsyncStorage.getItem('oneSignalProviderId').then(res => {
            this.setState({token: res});
        });
        AsyncStorage.getItem('showFacebookSocialLogin').then(res => {
            this.setState({showFacebookSocialLogin: res == '1' });
        });
        AsyncStorage.getItem('showInstaSocialLogin').then(res => {
            this.setState({showInstaSocialLogin: res == '1' });
        });
    };
    loginWithInstagram = (data) => {
        this.props.loginSocialUser(data.user_id, '', '', 'instagram', this.state.token);
    };
    async loginWithFacebook () {
        const _this = this;
        LoginManager.logInWithPermissions(["public_profile", "email"]).then(
            function (result) {
                if (result.isCancelled) {
                    console.log(" ----------------- facebook login canceled----------------");
                } else {
                    AccessToken.getCurrentAccessToken().then(
                        async (data) => {
                            _this.successFacebookTokenReady(data);
                        });
                }
            },
            function(error) {
                Toast.show({
                    text: I18n.local === 'en' ? 'failed login via facebook' : 'فشل تسجيل الدخول عن طريق الفيسبوك',
                    buttonText: L('dismiss'),
                    duration: 3000,
                    style: {backgroundColor: secondaryColor, color: fontColor},
                    textStyle: {fontFamily: DroidKufi, fontSize: moderateScale(4.5)},
                    buttonTextStyle: {fontFamily: DroidKufi, fontSize: moderateScale(4.5)},
                });
            }
        );
    }
    successFacebookTokenReady(data) {
        const infoRequest = new GraphRequest(
            '/me?fields=name,picture,email',
            null,
             this.successFacebookUserInfoReady
        );
        // Start the graph request.
        new GraphRequestManager().addRequest(infoRequest).start();

    }

    successFacebookUserInfoReady = (error, result) => {
        if (error) {
            alert('Error fetching data: ' + error.toString());
        } else {
            console.log(result);
            this.props.loginSocialUser(result.id, result.email, result.name, 'facebook', this.state.token);

        }
    }
}

const mapStateToProps = ({auth}) => {
    const {
        email,
        password,
        loading,
        message,
        user,
        token,
        success,
        emailF
    } = auth;
    return {email, password, loading, message, user, token, success, emailF};
};
export default connect(mapStateToProps, {
    loginSocialUser,
})(SocialLogin);
const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center' // if you want to fill rows left to right
    },
    item: {
        padding: 10,
        width: '50%' // is 50% of container width
    },
    loginImages: {
        flex: 1,
        width: '100%',
        resizeMode: 'contain'
    }
});