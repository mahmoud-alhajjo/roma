import React, {Component, useState, useEffect} from 'react';
import { SocialIcon } from 'react-native-elements'

import {
    Alert,
    BackHandler,
    Dimensions,
    FlatList,
    Image,
    Linking,
    StyleSheet,
    TextInput,
    TouchableOpacity,
} from 'react-native';

import styles, {
    brandImage,
    buttonStyle,
    buttonTextStyle,
    fontBold,
    inputField,
    solidText,
    textContainer,
    verticalScale,
} from './assets/styles/Style';
import { Text, View,} from 'native-base';


function SocialMediaAddress(props) {
     return (
            <View style={{
                flexDirection: 'row',
                alignSelf: 'center',
                justifyContent: 'center',
                flex: 1,
                paddingTop: verticalScale(4),
            }}>
                <SocialIcon
                    type='facebook'
                      onPress={() => Linking.openURL(props.settings.facebook)
                      }
                  />
                <SocialIcon
                    type='twitter'
                    onPress={() => Linking.openURL(props.settings.twitter)
                    }
                />
                <SocialIcon
                    type='instagram'
                    onPress={() => Linking.openURL(props.settings.insta)
                    }
                />
            </View>
        );
}
export default SocialMediaAddress;
