import React, {Component} from 'react';
import {
    Image,
    FlatList,
    Alert,
    TouchableOpacity,
    BackHandler, Dimensions,
} from 'react-native';
import {connect} from 'react-redux';
import {Spinner} from './assets/common';
import {L} from '../Config';
import styles, {
  verticalScale,
  scale,
  buttonStyle,
  buttonTextStyle,
  moderateScale,
  inputField,
  primaryColor,
  fontBold,
  DroidKufi,
  DroidKufiBold,
} from './assets/styles/Style';
import AsyncStorage from '@react-native-community/async-storage';
import {
  Container,
  Content,
  Header,
  List,
  ListItem,
  Button,
  Text,
  View,
  Icon,
  Toast,
  Thumbnail,
  Right,
  Left,
  Body,
  Footer,
} from 'native-base';
import {AirbnbRating} from 'react-native-ratings';
import {
    AddToCart,
    getSizes,
    getSettings,
    updateCartQty,
    clearMessage,
    showMessageChanged,
    clearCart, getPrice, getVideos,
} from '../actions';
import {Actions} from 'react-native-router-flux';
import YoutubePlayer from 'react-native-youtube-iframe';
/*const playerRef = useRef(null);
const [playing, setPlaying] = useState(true);*/
const {width: screenWidth} = Dimensions.get('window');

class Videos extends Component {
  state = {
    language: 'ar',
  };
  onButtonPress = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  };

  handleBackButton = () => {
    BackHandler.exitApp();
    return true;
  };
  componentDidMount() {
      this.props.getVideos(null);
    try {
      //.this._getData();
    } catch (error) {
    }
  }
  _getData() {
      this.props.getVideos(null);
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }
  _renderError() {
      return null;
  }
  _renderLoading() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }
  }
    _renderItems = () => {
      console.log(this.props.videos);
      if (this.props.videos) {
          this.props.videos.map((item, index) => {
              return (
                  <View
                      key={index}
                      style={{
                          borderBottomColor: '#ddd',
                          borderBottomWidth: 2,
                      }}>
                      <YoutubePlayer
                          height={230}
                          width={screenWidth}
                          videoId={item.videoId}
                          onChangeState={event => console.log(event)}
                          onReady={() => console.log("ready")}
                          onError={e => console.log(e)}
                          onPlaybackQualityChange={q => console.log(q)}
                          volume={50}
                          playbackRate={1}
                          playerParams={{
                              cc_lang_pref: "us",
                              showClosedCaptions: true
                          }}
                      />
                      <Text style={{paddingLeft: 20, paddingRight: 20}}>{item.name}</Text>
                      <Text style={{paddingLeft: 20, paddingRight: 20, paddingTop: 10}}>{item.details}</Text>
                  </View>
              );
          });
      }
  };
  render() {
    return (
      <Container>
        <Header
          androidStatusBarColor={primaryColor}
          style={styles.routernavigationBarStyle}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              alignSelf: 'center',
              alignItems: 'center',
              width: scale(100),
            }}>
            <Text style={[styles.fontRegular, {color: '#fff', fontSize: 14}]}>
              {L('videos')}
            </Text>
          </View>
        </Header>
        <Content>
            {this.props.videos.length > 0 ? (
            this.props.videos.map((item, index) => {
                return (<View
                    key={index}
                    style={{
                        borderBottomColor: '#ddd',
                        borderBottomWidth: 2,
                    }}>
                    <YoutubePlayer
                        height={230}
                        width={screenWidth}
                        videoId={item.videoId}
                        onChangeState={event => console.log(event)}
                        onReady={() => console.log("ready")}
                        onError={e => console.log(e)}
                        onPlaybackQualityChange={q => console.log(q)}
                        volume={50}
                        playbackRate={1}
                        playerParams={{
                            cc_lang_pref: "us",
                            showClosedCaptions: true
                        }}
                    />
                    <Text style={{paddingLeft: 20, paddingRight: 20}}>{item.name}</Text>
                    <Text style={{paddingLeft: 20, paddingRight: 20, paddingTop: 10}}>{item.details}</Text>
                </View>
                )
            })) : (
                <View
                    style={{
                        alignSelf: 'center',
                        justifyContent: 'center',
                        height: verticalScale(50),
                    }}>
                    <View
                        style={{
                            backgroundColor: '#f9f9f9',
                            alignSelf: 'center',
                            width: scale(60),
                            height: verticalScale(32),
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderRadius: moderateScale(100),
                            borderWidth: 0.3,
                            borderColor: primaryColor,
                        }}>
                        <Icon
                            name="video"
                            type="Feather"
                            style={{
                                fontSize: moderateScale(35),
                                color: '#ddd',
                            }}
                        />
                    </View>
                    <Text
                        style={[
                            styles.fontRegular,
                            {textAlign: 'center', marginTop: verticalScale(3)},
                        ]}>
                        {L('videos')} {L('empty')}
                    </Text>
                </View>
            )
            }
        </Content>
        {this._renderError()}
        {this._renderLoading()}
      </Container>
    );
  }
}

const mapStateToProps = ({others}) => {
  const {videos, loading} = others;
  return {
      videos,
    loading
  };
};
export default connect(mapStateToProps, {
    getVideos
})(Videos);
