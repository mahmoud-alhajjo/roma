import React, {Component} from 'react';
import {
  Image,
  FlatList,
  TouchableOpacity,
  Alert,
  BackHandler,
} from 'react-native';
import {connect} from 'react-redux';
import {Spinner} from './assets/common';
import {L} from '../Config';
import AsyncStorage from '@react-native-community/async-storage';
import styles, {
  verticalScale,
  scale,
  buttonStyle,
  buttonTextStyle,
  moderateScale,
  inputField,
  primaryColor,
  fontBold,
  DroidKufi,
  DroidKufiBold,
} from './assets/styles/Style';
import {
  Container,
  Content,
  Header,
  Form,
  Item,
  Input,
  Button,
  Text,
  View,
  Icon,
  Toast,
  Label,
} from 'native-base';
import {
    getWhishlist,
    deleteWhishlist,
    clearMessage,
    showMessageChanged, getPrice,
} from '../actions';
import {AirbnbRating} from 'react-native-ratings';
import {Actions} from 'react-native-router-flux';

class Wishlist extends Component {
  state = {
    language: 'ar',
    user: null,
  };
  onButtonPress = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    // then navigate
    // navigate('NewScreen');
  };

  handleBackButton = () => {
    BackHandler.exitApp();
    return true;
  };
  async componentDidMount() {
    try {
      let value = await AsyncStorage.getItem('user');
      // console.log(value);
      const user = JSON.parse(value);
      if (user) {
        this.setState({user: user});
      }
      // console.log(user);
      this._getData();
    } catch (error) {
      // console.log(error);
      // this._getData()
    }
  }
  _getData() {
    this.props.getWhishlist(this.state.user.id);
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }
  _renderError() {
    if (this.props.message) {
      this.props.clearMessage();
      return Toast.show({
        text: this.props.message,
        buttonText: L('dismiss'),
        duration: 3000,
        style: {backgroundColor: primaryColor},
        textStyle: {fontFamily: DroidKufi, fontSize: moderateScale(4.5)},
        buttonTextStyle: {fontFamily: DroidKufi, fontSize: moderateScale(4.5)},
      });
    }
  }
  _renderLoading() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }
  }
  removeToWishlist(product_id) {
    // console.log(id);
    const deleteFavourite = {
      client_id: this.state.user.id,
      product_id: product_id,
    };
    this.props.deleteWhishlist({deleteFavourite});
    // console.log({ deleteFavourite });
    // this.setState({ item })
  }
  render() {
    return (
      <Container>
        <Header
          androidStatusBarColor={primaryColor}
          style={styles.routernavigationBarStyle}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              alignSelf: 'center',
              alignItems: 'center',
              width: scale(100),
            }}>
            <Text style={[styles.fontRegular, {color: '#fff', fontSize: 14}]}>
              {L('wishlist')}
            </Text>
          </View>
        </Header>
        <Content padder>
          {this.props.wishlistArr.length > 0 ? (
            this.props.wishlistArr.map((item, index) => (
              <TouchableOpacity
                key={index}
                onPress={() => Actions.push('product', {productID: item.id})}
                style={{
                  flexDirection: 'row',
                  backgroundColor: '#fefdfd',
                  shadowColor: '#000',
                  shadowOffset: {
                    width: 0,
                    height: 3,
                  },
                  shadowOpacity: 0.27,
                  shadowRadius: 4.65,

                  elevation: 6,
                  width: scale(90),
                  padding: scale(5),
                  marginBottom: verticalScale(2),
                  alignSelf: 'center',
                  borderRadius: moderateScale(3),
                  marginTop: verticalScale(1),
                }}>
                <View
                  style={{
                    backgroundColor: '#fff',
                    shadowColor: '#000',
                    shadowOffset: {
                      width: 0,
                      height: 1,
                    },
                    shadowOpacity: 0.2,
                    shadowRadius: 1.41,
                    elevation: 2,
                    alignItems: 'center',
                    alignSelf: 'center',
                    justifyContent: 'center',
                    marginBottom: verticalScale(1),
                  }}>
                  <Image
                    source={{uri: item.photos[0].thumb}}
                    style={{height: verticalScale(10), width: scale(20)}}
                  />
                </View>
                <View
                  style={{
                    marginLeft: moderateScale(3),
                    alignSelf: 'center',
                    width: scale(48),
                    alignItems: 'flex-start',
                  }}>
                  <Text
                    numberOfLines={1}
                    style={[
                      styles.fontRegular,
                      {color: '#000', fontSize: moderateScale(6)},
                    ]}>
                    {item.title}
                  </Text>
                  <Text
                    style={[
                      styles.fontRegular,
                      {color: primaryColor, fontSize: moderateScale(5)},
                    ]}>
                      {getPrice(item.price)} {L(global.currency.code)}
                  </Text>
                  <AirbnbRating
                    showRating={false}
                    isDisabled
                    defaultRating={item.rating}
                    ratingColor="#f4b700"
                    size={10}
                    starContainerStyle={{marginLeft: 0}}
                  />
                </View>
                <View style={{alignSelf: 'center', width: scale(10)}}>
                  <TouchableOpacity
                    transparent
                    onPress={() =>
                      Alert.alert(
                        L('alert'),
                        L('wishListDelete'),
                        [
                          {text: L('no'), style: 'cancel'},
                          {
                            text: L('yes'),
                            onPress: () => this.removeToWishlist(item.id),
                          },
                        ],
                        {cancelable: false},
                      )
                    }>
                    <Icon
                      name="trash-2"
                      type="Feather"
                      style={{color: '#ff8e93', fontSize: moderateScale(10)}}
                    />
                  </TouchableOpacity>
                </View>
              </TouchableOpacity>
            ))
          ) : (
            <View
              style={{
                alignSelf: 'center',
                justifyContent: 'center',
                height: verticalScale(50),
              }}>
              <View
                style={{
                  backgroundColor: '#f9f9f9',
                  alignSelf: 'center',
                  width: scale(60),
                  height: verticalScale(32),
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: moderateScale(100),
                  borderWidth: 0.3,
                  borderColor: primaryColor,
                }}>
                <Icon
                  name="heart"
                  type="Feather"
                  style={{
                    fontSize: moderateScale(35),
                    color: '#ddd',
                  }}
                />
              </View>
              <Text
                style={[
                  styles.fontRegular,
                  {textAlign: 'center', marginTop: verticalScale(3)},
                ]}>
                {L('wishlist')} {L('empty')}
              </Text>
            </View>
          )}
        </Content>
        {this._renderError()}
        {this._renderLoading()}
      </Container>
    );
  }
}

const mapStateToProps = ({productsR}) => {
  const {wishlistArr, loading, message} = productsR;
  // console.log(wishlistArr);
  return {loading, message, wishlistArr};
};
export default connect(mapStateToProps, {
  getWhishlist,
  deleteWhishlist,
  clearMessage,
  showMessageChanged,
})(Wishlist);
