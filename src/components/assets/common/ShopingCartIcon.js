import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    Platform,
    /*TouchableNativeFeedback*/
} from "react-native";
import styles, {
    verticalScale, scale, moderateScale, buttonStyle, buttonTextStyle,
    textContainer, solidText, inputField, primaryColor, secondaryColor,
    fontBold, DroidKufi, DroidKufiBold, brandImage
} from '../styles/Style';
import { withNavigation } from 'react-navigation'

import { connect } from 'react-redux'
import { Icon, Button } from 'native-base'
import { Actions } from 'react-native-router-flux';

const ShoppingCartIcon = (props) => (
    <Button onPress={() => Actions.jump('cart')} transparent
        /*background={TouchableNativeFeedback.Ripple(secondaryColor)}*/
        style={[styles.headerBack]}
    >
        <View style={{
            position: 'absolute', height: 15, width: 15,
            borderRadius: 15, backgroundColor: '#fff',
            left: 8, top: 5, alignItems: 'center',
            justifyContent: 'center', zIndex: 2000
        }}>
            <Text style={{ color: primaryColor, fontFamily: DroidKufi }}>{this.props.cart.length}</Text>
        </View>
        <Icon name="shopping-cart" type='Feather' style={{
            fontSize: moderateScale(10),
            color: '#fff'
        }} />
        {/* <Icon name='keyboard-backspace' type='MaterialIcons'
            style={styles.backButtonIcon}
        /> */}
    </Button>
    // <Button transparent onPress={() => Actions.jump('cart')}
    //     style={[{ padding: 5 }]}>

    // </Button>
)

const mapStateToProps = ({ ordersR }) => {
    const { cart } = ordersR
    return { cart}
}
export default connect(mapStateToProps,{})(withNavigation(ShoppingCartIcon));

