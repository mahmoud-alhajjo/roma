import I18n from 'react-native-i18n';
import AsyncStorage from '@react-native-community/async-storage';
import en from './translations/en';
import ar from './translations/ar';
var language;
I18n.translations = {
  ar,
  en,
};
// I18n.locale = 'ar';

getLangStored = async () => {
  // var lan g ='ar';
  try {
    let lang = await AsyncStorage.getItem('language');
    if (lang !== null) {
      return lang;
    }
    // return lang;
  } catch (error) {
    // Error retrieving data
  }
};

getLangStored().then(async result => {
  I18n.locale = await result;
  this.language;
});
I18n.fallbacks = true;

// console.log('2',I18n.locale);
export default I18n;
export const baseUrl = 'https://adasatik.com/' + this.language + '/mobile/';
export const base = 'https://adasatik.com/upload/';

export const headers = {
  Accept: 'application/json',
  // 'content-type': 'multipart/form-data',
  client: 'fb28d9c9fb43490c1d28384f6fcbf9dde6ceeab8',
  secret: 'eaeba67bcdffcbca250a5d0c1b3c7ee6e63c8855',
};
