import {
  STATE_ADD_TO_CART,
  CHANGE_MESSAGE,
  CLEAR_MESSAGE,
  GET_SIZES,
  GET_SIZES_FAIL,
  GET_SIZES_SUCCESS,
  SAVE_ORDER,
  SAVE_ORDER_FAIL,
  SAVE_ORDER_SUCCESS,
  GET_ORDERS,
  GET_ORDERS_FAIL,
  GET_ORDERS_SUCCESS,
  UPDATE_CART,
  UPDATE_CART_SUCCESS,
  GET_ORDER,
  GET_ORDER_FAIL,
  GET_ORDER_SUCCESS,
  CHECK_COUPON,
  CHECK_COUPON_FAIL,
  CHECK_COUPON_SUCCESS,
  CLEAR_CART
} from '../actions/types';
import {baseUploadUrl, L} from '../Config';

const INITAL_STATE = {
  cart: [],
  qty: 0,
  loading: false,
  messsage: '',
  sizes: [],
  orders: [],
  singleOrder: [],
  order_details: [],
  refresh: false,
  updateCart: false,
  // checkCoupon: '',
};
export default (state = INITAL_STATE, action) => {
  switch (action.type) {
    case CHANGE_MESSAGE:
      return {...state, message: action.payload};
    case CLEAR_MESSAGE:
      return {...state, message: '', updateCart: false};
    case STATE_ADD_TO_CART:
      return {
        ...state,
        cart: action.payload.shoppingCartItems,
        qty: action.payload.shoppingCartItems.length,
        updateCart: true,
      };
    case GET_SIZES:
      return {...state, loading: true};
    case GET_SIZES_SUCCESS:
      return {...state, sizes: action.payload, loading: false};
    case GET_SIZES_FAIL:
      return {...state, message: action.payload, loading: false};
    case GET_ORDERS:
      return {...state, loading: true};
    case GET_ORDERS_SUCCESS:
      return {...state, loading: false, orders: action.payload};
    case GET_ORDERS_FAIL:
      return {...state, loading: false};
    case GET_ORDER:
      return {...state, loading: true};
    case GET_ORDER_SUCCESS:
      return {
        ...state,
        loading: false,
        singleOrder: action.payload,
        order_details: action.payload.order_details,
      };
    case GET_ORDER_FAIL:
      return {...state, loading: false};
    case SAVE_ORDER:
      return {...state, loading: true, refresh: true};
    case SAVE_ORDER_FAIL:
      return {
        ...state,
        message: action.payload,
        loading: false,
        refresh: false,
      };
    case SAVE_ORDER_SUCCESS:
      return {...state, loading: false, cart: [], qty: 0, refresh: false};
    case CHECK_COUPON:
      return {...state, loading: true, refresh: true};
    case CHECK_COUPON_FAIL:
      return {
        ...state,
        message: action.payload,
        loading: false,
      };
    case CHECK_COUPON_SUCCESS:
      return {...state, loading: false, message: action.payload.message};
    case UPDATE_CART:
      return {...state, loading: true};
    case UPDATE_CART_SUCCESS:
      return {
        ...state,
        cart: action.payload,
        qty: action.payload.length,
        loading: false,
      };
    case CLEAR_CART:
      return {
        ...state,
        cart: [],
        qty: 0,
        message: L('cancelOrder'),
        loading: false,
      };
    // console.log(message);
    default:
      return state;
  }
};
