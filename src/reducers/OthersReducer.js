import {
    CONTACT_FORM,
    CONTACT_FORM_FAIL,
    CHANGE_MESSAGE,
    CLEAR_MESSAGE,
    CONTACT_FORM_SUCCESS,
    GET_PAGE,
    PAGES_SUCCESS,
    PAGES_FAIL,
    GET_HOMESCREEN,
    GET_HOMESCREEN_SUCCESS,
    GET_HOMESCREEN_FAIL,
    GET_NOTIFICATIONS,
    GET_NOTIFICATIONS_FAIL,
    GET_NOTIFICATIONS_SUCCESS,
    GET_SETTINGS,
    GET_SETTINGS_FAIL,
    GET_SETTINGS_SUCCESS,
    GET_ADDRESSES,
    GET_ADDRESSES_SUCCESS,
    GET_ADDRESSES_FAIL,
    DELETE_ADDRESSES,
    DELETE_ADDRESSES_SUCCESS,
    DELETE_ADDRESSES_FAIL,
    ADD_ADDRESS,
    ADD_ADDRESS_FAIL,
    ADD_ADDRESS_SUCCESS,
    SEARCH,
    SEARCH_FAIL,
    SEARCH_SUCCESS,
    ADD_COMMENT,
    ADD_COMMENT_FAIL,
    ADD_COMMENT_SUCCESS,
    GET_COMMENTS,
    GET_COMMENTS_FAIL,
    GET_COMMENTS_SUCCESS,
    FILTER,
    FILTER_FAIL,
    FILTER_SUCCESS,
    GET_FILTER_OPTIONS,
    GET_FILTER_OPTIONS_FAIL,
    GET_FILTER_OPTIONS_SUCCESS,
    GET_CURRENCIRES, GET_CURRENCIRES_SUCCESS, GET_CURRENCIRES_FAIL, GET_VIDEOS, GET_VIDEOS_SUCCESS, GET_VIDEOS_FAIL
} from '../actions/types';
import AsyncStorage from "@react-native-community/async-storage";

const INITAL_STATE = {
  name: '',
  content: '',
  loading: false,
  loadingImage: false,
  title: '',
  result: null,
  message: '',
  users: [],
  notifications: [],
  type: null,
  pagesContent: '',
  user_id: null,
  slider: [],
  brands: [],
  offeredCats: [],
  settings: {},
  delivery: 0,
  addresses: [],
  searchResult: [],
  adminPhone: '',
  comments: [],
  durations: [],
  colors: [],
  vat: '',
  disProductPhoto: '',
  specialOrderPhoto: '',
  bank_transfer: '',
  address_ar: '',
  admin_email: '',
  settings: '',
  bannerApp: ' ',
  cartMessage: '',
    currencies: [],
    videos: []
};

export default (state = INITAL_STATE, action) => {
  switch (action.type) {
    case CHANGE_MESSAGE:
      return {...state, message: action.payload};
    case CLEAR_MESSAGE:
      return {...state, message: ''};
    case CONTACT_FORM:
      return {...state, loading: true};
    case CONTACT_FORM_FAIL:
      return {...state, loading: false, message: action.payload.message};
    case CONTACT_FORM_SUCCESS:
      return {...state, loading: false, message: action.payload.message};
    case GET_PAGE:
      return {...state, loading: true};
    case PAGES_SUCCESS:
      // console.log(action.payload);
      return {...state, pagesContent: action.payload, loading: false};
    case PAGES_FAIL:
      return {...state, message: action.payload, loading: false};
    case GET_HOMESCREEN:
      return {...state, loading: true};
    case GET_HOMESCREEN_SUCCESS:
      // console.log(action.payload);
      return {
        ...state,
        slider: action.payload.slider,
        brands: action.payload.brands,
        offeredCats: action.payload.offered_cats,
        updateAssc: false,
        loading: false,
      };
    case GET_HOMESCREEN_FAIL:
      // console.log(action.payload);
      return {...state, message: action.payload, loading: false};
    case GET_NOTIFICATIONS:
      return {...state, loading: true};
    case GET_NOTIFICATIONS_SUCCESS:
      // console.log(action.payload);
      return {
        ...state,
        notifications: action.payload,
        loading: false,
      };
    case GET_NOTIFICATIONS_FAIL:
      return {...state, message: action.payload, loading: false};
    case GET_SETTINGS:
      return {...state, loading: true, refresh: true};
    case GET_SETTINGS_SUCCESS:
      // console.log('111', action.payload);
        AsyncStorage.setItem('showFacebookSocialLogin', action.payload.showFacebookSocialLogin? '1' : '0');
        AsyncStorage.setItem('showInstaSocialLogin', action.payload.showInstaSocialLogin? '1' : '0');
      return {
        ...state,
        loading: false,
        settings: action.payload,
        delivery: action.payload.shipping,
        adminPhone: action.payload.phone1,
        vat: action.payload.tax,
        specialOrderPhoto: action.payload.special_orders,
        disProductPhoto: action.payload.discounted_products,
        bank_transfer: action.payload.bank_transfer,
        admin_email: action.payload.admin_email,
        address_ar: action.payload.address_ar,
        settings: action.payload,
        bannerApp: action.payload.banner_app,
        cartMessage: action.payload.cart_phrase,
        refresh: false
      };
    case GET_SETTINGS_FAIL:
      // console.log(action.payload);
      return {...state, loading: false};
    case GET_ADDRESSES:
      return {...state, loading: true, refresh: true};
    case GET_ADDRESSES_SUCCESS:
      // console.log('111', action.payload);
      return {
        ...state,
        loading: false,
        addresses: action.payload,
        refresh: false,
      };
    case GET_ADDRESSES_FAIL:
      // console.log(action.payload);
      return {...state, loading: false};
    case DELETE_ADDRESSES:
      return {...state, loading: true, refresh: true};
    case DELETE_ADDRESSES_SUCCESS:
      //   console.log('111', action.payload);
      return {
        ...state,
        loading: false,
        addresses: action.payload,
        refresh: false,
      };
    case DELETE_ADDRESSES_FAIL:
      // console.log(action.payload);
      return {...state, loading: false};
    case ADD_ADDRESS:
      return {...state, loading: true, refresh: true};
    case ADD_ADDRESS_SUCCESS:
      // console.log('111', action.payload);
      return {
        ...state,
        loading: false,
        addresses: action.payload,
        refresh: false,
      };
    case ADD_ADDRESS_FAIL:
      // console.log(action.payload);
      return {...state, loading: false, message: action.payload};
    case SEARCH:
      return {...state, loading: true, refresh: true, searchResult: []};
    case SEARCH_SUCCESS:
      // console.log('111', action.payload);
      return {
        ...state,
        loading: false,
        searchResult: action.payload,
        refresh: false,
      };
    case SEARCH_FAIL:
      // console.log(action.payload);
      return {...state, loading: false};
    case FILTER:
      return {...state, loading: true, refresh: true, searchResult: []};
    case FILTER_SUCCESS:
      // console.log('111', action.payload);
      return {
        ...state,
        loading: false,
        searchResult: action.payload,
        refresh: false,
      };
    case FILTER_FAIL:
      // console.log(action.payload);
      return {...state, loading: false};
    case GET_FILTER_OPTIONS:
      return {...state, loading: true, refresh: true};
    case GET_FILTER_OPTIONS_SUCCESS:
      // console.log('111', action.payload);
      return {
        ...state,
        loading: false,
        brands: action.payload.brands,
        durations: action.payload.durations,
        colors: action.payload.colors,
        refresh: false,
      };
    case GET_FILTER_OPTIONS_FAIL:
      // console.log(action.payload);
      return {...state, loading: false};
    case ADD_COMMENT:
      return {...state, loading: true, refresh: true};
    case ADD_COMMENT_SUCCESS:
      // console.log('111', action.payload);
      return {
        ...state,
        loading: false,
        message: action.payload.message,
        refresh: false,
      };
    case ADD_COMMENT_FAIL:
      // console.log(action.payload);
      return {...state, message: action.payload, loading: false};
    case GET_COMMENTS:
      return {...state, loading: true};
    case GET_COMMENTS_SUCCESS:
      return {...state, comments: action.payload, loading: false};
    case GET_COMMENTS_FAIL:
      return {...state, loading: false};
      case GET_CURRENCIRES: {
          return {...state, loading: false};
      }
      case GET_CURRENCIRES_SUCCESS: {
          return {...state,currencies: action.payload, loading: false};
      }
      case GET_CURRENCIRES_FAIL:
          return {...state, loading: false};
      case GET_VIDEOS: {
          return {...state,videos: [], loading: false};
      }
      case GET_VIDEOS_SUCCESS: {
          return {...state,videos: action.payload, loading: false};
      }
      case GET_VIDEOS_FAIL:
          return {...state, loading: false};
    default:
      return state;
  }
};
