import {
    CHANGE_MESSAGE,
    CLEAR_MESSAGE,
    GET_MAIN_CAT,
    GET_MAIN_CAT_FAIL,
    GET_MAIN_CAT_SUCCESS,
    GET_CATS_BY_PARENT,
    GET_CATS_BY_PARENT_FAIL,
    GET_CATS_BY_PARENT_SUCCESS,
    GET_PRODUCTS_BY_CATID,
    GET_PRODUCTS_BY_CATID_FAIL,
    GET_PRODUCTS_BY_CATID_SUCCESS,
    GET_PRODUCTS_BY_BRANDID,
    GET_PRODUCTS_BY_BRANDID_FAIL,
    GET_PRODUCTS_BY_BRANDID_SUCCESS,
    GET_PRODUCT,
    GET_PRODUCT_FAIL,
    GET_PRODUCT_SUCCESS,
    COUNT_CHANGED,
    ADD_TO_WISHLIST,
    ADD_TO_WISHLIST_FAIL,
    ADD_TO_WISHLIST_SUCCESS,
    DELETE_WISHLIST,
    DELETE_WISHLIST_FAIL,
    DELETE_WISHLIST_SUCCESS,
    GET_WISHLIST,
    GET_WISHLIST_FAIL,
    GET_WISHLIST_SUCCESS,
    GET_ORDERS,
    GET_ORDERS_FAIL,
    GET_ORDERS_SUCCESS,
    GET_DIS_PRODUCTS,
    GET_DIS_PRODUCTS_FAIL,
    GET_DIS_PRODUCTS_SUCCESS,
    GET_BRAND_PRODUCTS,
    GET_BRAND_PRODUCTS_SUCCESS,
    GET_BRAND_PRODUCTS_FAIL,
    GET_SLIDERS_BY_BRAND,
    GET_SLIDERS_BY_BRAND_SUCCESS,
    GET_SLIDERS_BY_BRAND_FAIL,
    GET_SLIDERS_BY_CATEGORY, GET_SLIDERS_BY_CATEGORY_SUCCESS, GET_SLIDERS_BY_CATEGORY_FAIL,
    GET_WHEEL_ITEMS_FORTUNE, GET_WHEEL_ITEMS_FORTUNE_FAIL, GET_WHEEL_ITEMS_FORTUNE_SUCCESS,
    GIFT_TO_USER, GIFT_TO_USER_FAIL, GIFT_TO_USER_SUCCESS,
} from '../actions/types';

const INITAL_STATE = {
  loading: false,
  loadingImage: false,
  result: null,
  message: '',
  mainCat: [],
  catsByParent: [],
  catProducts: [],
  brandProducts: [],
  product: '',
  count: 0,
  wishlistArr: [],
  disProducts: [],
  brandCatProducts: [],
  wheelItems: [],
  error: null
};

export default (state = INITAL_STATE, action) => {
  switch (action.type) {
    case COUNT_CHANGED:
      return {...state, count: action.payload};
    case CHANGE_MESSAGE:
      return {...state, message: action.payload};
    case CLEAR_MESSAGE:
      return {...state, message: ''};
    case GET_MAIN_CAT:
      return {...state, loading: true};
    case GET_MAIN_CAT_SUCCESS:
      // console.log(action.payload);
      return {
        ...state,
        mainCat: action.payload,
        updateAssc: false,
        loading: false,
      };
    case GET_MAIN_CAT_FAIL:
      // console.log(action.payload);
      return {...state, loading: false};
    case GET_CATS_BY_PARENT:
      return {...state, catsByParent: [], loading: true};
    case GET_CATS_BY_PARENT_SUCCESS:
      // console.log(action.payload);
      return {
        ...state,
        catsByParent: action.payload,
        updateAssc: false,
        loading: false,
      };
    case GET_CATS_BY_PARENT_FAIL:
      // console.log(action.payload);
      return {...state, loading: false};
    case GET_PRODUCTS_BY_CATID:
      return {...state, catProducts: [], brandProducts: [], loading: true};
    case GET_PRODUCTS_BY_CATID_SUCCESS:
      // console.log(action.payload);
      return {
        ...state,
        catProducts: action.payload,
        updateAssc: false,
        loading: false,
      };
    case GET_PRODUCTS_BY_CATID_FAIL:
      // console.log(action.payload);
      return {...state, loading: false};
    case GET_PRODUCTS_BY_BRANDID:
      return {...state, brandProducts: [], catProducts: [], loading: true};
    case GET_PRODUCTS_BY_BRANDID_SUCCESS:
      // console.log(action.payload);
      return {
        ...state,
        brandProducts: action.payload,
        updateAssc: false,
        loading: false,
      };
    case GET_PRODUCTS_BY_BRANDID_FAIL:
      // console.log(action.payload);
      return {...state, loading: false};
    case GET_PRODUCT:
      return {...state, product: null, loading: true};
    case GET_PRODUCT_SUCCESS:
      // console.log(action.payload);
      return {
        ...state,
        product: action.payload,
        updateAssc: false,
        loading: false,
      };
    case GET_PRODUCT_FAIL:
      // console.log(action.payload);
      return {...state, loading: false};
    case ADD_TO_WISHLIST:
      return {...state, loading: true, refresh: true};
    case ADD_TO_WISHLIST_SUCCESS:
      return {
        ...state,
        message: action.payload.message,
        loading: false,
        refresh: false,
      };
    case ADD_TO_WISHLIST_FAIL:
      return {...state, loading: false};
    case DELETE_WISHLIST:
      return {...state, loading: true};
    case DELETE_WISHLIST_SUCCESS:
      return {...state, wishlistArr: action.payload, loading: false};
    case DELETE_WISHLIST_FAIL:
      return {...state, loading: false};
    case GET_WISHLIST:
      return {...state, loading: true};
    case GET_WISHLIST_SUCCESS:
      return {...state, loading: false, wishlistArr: action.payload};
    case GET_WISHLIST_FAIL:
      return {...state, loading: false};
    case GET_DIS_PRODUCTS:
      return {...state, loading: true};
    case GET_DIS_PRODUCTS_SUCCESS:
      return {...state, loading: false, disProducts: action.payload};
    case GET_DIS_PRODUCTS_FAIL:
      return {...state, loading: false};
    case GET_BRAND_PRODUCTS:
      return {...state, loading: true};
    case GET_BRAND_PRODUCTS_SUCCESS:
      return {...state, loading: false, brandCatProducts: action.payload};
    case GET_BRAND_PRODUCTS_FAIL:
      return {...state, loading: false};
      case GET_SLIDERS_BY_BRAND:
          return {...state, brandSlider: [], loading: true};
      case GET_SLIDERS_BY_BRAND_SUCCESS:
          // console.log(action.payload);
          return {
              ...state,
              brandSlider: action.payload,
              loading: false,
          };
      case GET_SLIDERS_BY_BRAND_FAIL:
          // console.log(action.payload);
          return {...state, loading: false};
      case GET_SLIDERS_BY_CATEGORY:
          return {...state, categorySlider: [], loading: true};
      case GET_SLIDERS_BY_CATEGORY_SUCCESS:
          // console.log(action.payload);
          return {
              ...state,
              categorySlider: action.payload,
              loading: false,
          };
      case GET_SLIDERS_BY_CATEGORY_FAIL:
          // console.log(action.payload);
          return {...state, loading: false};
      case GET_WHEEL_ITEMS_FORTUNE:
          return {...state, wheelItems: null, loading: true};
      case GET_WHEEL_ITEMS_FORTUNE_SUCCESS:
            // console.log(action.payload);
          return {
              ...state,
              wheelItems: action.payload,
              loading: false,
            };
      case GET_WHEEL_ITEMS_FORTUNE_FAIL:
            // console.log(action.payload);
          return {...state, loading: false};
      case GIFT_TO_USER:
          return {...state, loading: false, refresh: true};
      case GIFT_TO_USER_SUCCESS:
          return {
              ...state,
              loading: false,
              refresh: false,
            };
      case GIFT_TO_USER_FAIL:
          return {...state, loading: false , error: action.payload};
    default:
      return state;
  }
};
