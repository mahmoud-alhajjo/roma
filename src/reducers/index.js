import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import OthersReducer from './OthersReducer';
import ProductsReducer from './ProductsReducer';
import OrdersReducer from './OrdersReducer';

export default combineReducers({
    auth: AuthReducer,
    others: OthersReducer,
    productsR: ProductsReducer,
    ordersR: OrdersReducer,
});
